package com.example.hitman.revolucianapp.activity.utils;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;

import android.provider.UserDictionary;
import android.support.annotation.Nullable;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.hitman.revolucianapp.R;

/**
 * Created by hitman on 13/7/16.
 */
public class SearchSuggestionProvider extends ContentProvider {

    List<String> items;

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        if (items == null || items.isEmpty()){
//            OkHttpClient client = new OkHttpClient();
//            Request request = new Request.Builder()
//                    .url("https://dl.dropboxusercontent.com/u/6802536/cidades.json")
//                    .build();

//            try {
//                Response response = client.newCall(request).execute();
//                String jsonString = response.body().string();
//                JSONArray jsonArray = new JSONArray(jsonString);
//
//                items = new ArrayList<>();
//
//                int lenght = jsonArray.length();
//                for (int i = 0; i < lenght; i++) {
//                    String item = jsonArray.getString(i);
//                    items.add(item);
//                }
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            items = Arrays.asList(getContext().getResources().getStringArray(R.array.items_list));

        }
        items = Arrays.asList(getContext().getResources().getStringArray(R.array.items_list));

        MatrixCursor cursor = new MatrixCursor(
                new String[] {
                        BaseColumns._ID,
                        SearchManager.SUGGEST_COLUMN_TEXT_1,
                        SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID
                }
        );
        if (items != null) {
            String query = uri.getLastPathSegment().toUpperCase();
            int limit = Integer.parseInt(uri.getQueryParameter(SearchManager.SUGGEST_PARAMETER_LIMIT));

            int lenght = items.size();
            for (int i = 0; i < lenght && cursor.getCount() < limit; i++) {
                String city = items.get(i);
                if (city.toUpperCase().contains(query)){
                    cursor.addRow(new Object[]{ i, city, i });
                }
            }
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        return super.bulkInsert(uri, values);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }
}
