package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;

import java.util.List;

/**
 * Created by hitman on 28/6/16.
 */
public class FollowShopAdapter extends InterceptorView.Adapter<RecyclerView.ViewHolder> {

    private final int MALL = 0;
    private final int FOOD = 1;
    private final int MOVIE = 2;

    private List<Object> items;
    private Context context;

    public FollowShopAdapter(Context context, List<Object> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof MallModel) {
            return MALL;
        } else if (items.get(position) instanceof FoodShopModel) {
            return FOOD;
        } else
            return MOVIE;
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.anticipateovershoot_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //More to come
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case MALL:
                View v1 = inflater.inflate(R.layout.mall_list_item, viewGroup, false);
                viewHolder = new MallViewHolder(v1);
                break;
            case FOOD:
                View v2 = inflater.inflate(R.layout.food_list_item, viewGroup, false);
                viewHolder = new FoodViewHolder(v2);
                break;
            case MOVIE:
                View v3 = inflater.inflate(R.layout.movies_list_item, viewGroup, false);
                viewHolder = new MallViewHolder(v3);
                break;
            default:
                View v4 = inflater.inflate(R.layout.mall_list_item, viewGroup, false);
                viewHolder = new MallViewHolder(v4);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        //More to come
        switch (viewHolder.getItemViewType()) {
            case MALL:
                MallViewHolder v1 = (MallViewHolder) viewHolder;
                configureMallViewHolder(v1, position);
                animate(v1);
                break;
            case FOOD:
                FoodViewHolder v2 = (FoodViewHolder) viewHolder;
                configureFoodViewHolder(v2, position);
                animate(v2);
                break;
            case MOVIE:
                break;
            default:
                MallViewHolder v4 = (MallViewHolder) viewHolder;
                configureMallViewHolder(v4, position);
                animate(v4);
        }
    }

    private void configureMallViewHolder(MallViewHolder v, int position) {
        MallModel mallModel = (MallModel) items.get(position);
        if(mallModel != null) {
            v.cardShopName.setText(mallModel.getShopName());
            v.cardShopLocation.setText(mallModel.getArea());
            v.cardShopCategory.setText(mallModel.getShopTimings());
            v.cardShopDistance.setText(mallModel.getDistance()+"km");
            v.cardShopRating.setText(mallModel.getRating()+"");
            v.linearLayoutShop.setBackgroundResource(mallModel.getImage());
        }
    }

    private void configureFoodViewHolder(FoodViewHolder v, int position) {
        FoodShopModel foodShopModel = (FoodShopModel) items.get(position);
        if(foodShopModel != null) {
            v.cardFoodShopName.setText(foodShopModel.getShopName());
            v.cardFoodShopLocation.setText(foodShopModel.getShopLocation());
            v.cardFoodShopDistance.setText(foodShopModel.getShopDistance()+"km");
            v.cardFoodShopRatings.setText(foodShopModel.getShopRating()+"");
            v.cardFoodShopImage.setImageResource(foodShopModel.getImage());
        }
    }

    private class MallViewHolder extends RecyclerView.ViewHolder {

        public TextView cardShopName, cardShopLocation, cardShopDistance, cardShopCategory, cardShopRating;
        private LinearLayout linearLayoutShop;

        public MallViewHolder(View v) {
            super(v);
            cardShopName = (TextView) v.findViewById(R.id.mall_list_item_shop_name);
            cardShopLocation = (TextView) v.findViewById(R.id.mall_list_item_shop_area);
            cardShopCategory = (TextView) v.findViewById(R.id.mall_list_item_category);
            cardShopDistance = (TextView) v.findViewById(R.id.mall_list_item_distance);
            cardShopRating = (TextView) v.findViewById(R.id.mall_list_item_rating);
            linearLayoutShop = (LinearLayout) v.findViewById(R.id.mall_list_item_image);
        }
    }

    private class FoodViewHolder extends RecyclerView.ViewHolder {

        public TextView cardFoodShopName, cardFoodShopLocation, cardFoodShopDistance, cardFoodShopRatings;
        public ImageView cardFoodShopImage;

        public FoodViewHolder(View v) {
            super(v);
            cardFoodShopName = (TextView) v.findViewById(R.id.food_list_item_shopname);
            cardFoodShopDistance = (TextView) v.findViewById(R.id.food_list_item_distance);
            cardFoodShopLocation = (TextView) v.findViewById(R.id.food_list_item_location);
            cardFoodShopRatings= (TextView) v.findViewById(R.id.food_list_item_rating);
            cardFoodShopImage = (ImageView) v.findViewById(R.id.food_list_item_shopimage);
        }
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private FollowShopAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FollowShopAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
