package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.SearchItemModel;

import java.util.List;

/**
 * Created by hitman on 13/7/16.
 */
public class SearchItemAdapter extends InterceptorView.Adapter<SearchItemAdapter.MyViewHolder> {

    private List<SearchItemModel> items;
    public Context mContext;
    private ServerDB serverDB;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView shopPageInventoryItemImage;
        public TextView shopPageInventoryItemName;
        public TextView shopPageInventoryItemBrandName;
        public TextView shopPageInventoryItemPrice;
        public TextView shopPageInventoryItemShopName;

        public MyViewHolder(View view) {
            super(view);
            shopPageInventoryItemImage = (ImageView) view.findViewById(R.id.search_item_image);
            shopPageInventoryItemName = (TextView) view.findViewById(R.id.search_item_name);
            shopPageInventoryItemBrandName = (TextView) view.findViewById(R.id.search_item_brand_name);
            shopPageInventoryItemPrice = (TextView) view.findViewById(R.id.search_item_price);
            shopPageInventoryItemShopName = (TextView) view.findViewById(R.id.search_item_shop_name);
        }
    }

    public SearchItemAdapter(Context context, List<SearchItemModel> items) {
        mContext = context;
        this.items = items;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final SearchItemModel item = items.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.shopPageInventoryItemImage.setBackgroundResource(item.getImage());
        holder.shopPageInventoryItemName.setText(item.getName());
        holder.shopPageInventoryItemBrandName.setText(item.getCategory());
        holder.shopPageInventoryItemPrice.setText("Rs"+item.getPrice());
        holder.shopPageInventoryItemShopName.setText(item.getShopName());

//        holder.shopPageInventoryItemWishlist1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shopPageActivity.addShopItemWishList(item.getId1(), );
//            }
//        });
//
//        holder.shopPageInventoryItemWishlist2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shopPageActivity.addShopItemWishList(item.getId2());
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private SearchItemAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final SearchItemAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}


