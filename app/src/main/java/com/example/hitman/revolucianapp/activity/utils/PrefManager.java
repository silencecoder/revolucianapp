package com.example.hitman.revolucianapp.activity.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.hitman.revolucianapp.R;

/**
 * Created by hitman on 10/6/16.
 */
public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "androidhive-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String USER_LOGIN = "user_logged_in";
    private static final String USER_SIGNIN_TYPE = "user_signin";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_ID = "user_id";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setUserLoggedIn(boolean loggedIn) {
        editor.putBoolean(USER_LOGIN, loggedIn);
        editor.commit();
    }

    public boolean isUserLoggedIn() {
        return pref.getBoolean(USER_LOGIN, false);
    }


    public void setUserSigninType(String type) {
        editor.putString(USER_SIGNIN_TYPE, type);
    }

    public String getUserSigninType() {
        return pref.getString(USER_SIGNIN_TYPE, ""+R.string.signin_revolucian);
    }

    public void setUserEmail(String email) {
        editor.putString(USER_EMAIL, email);
    }

    public String getUserEmail() {
        return  pref.getString(USER_EMAIL, null);
    }

    public void setUserId(int id) {
        editor.putInt(USER_ID, id);
    }

    public int getUserId() {
        return pref.getInt(USER_ID, 0);
    }
}
