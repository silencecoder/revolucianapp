package com.example.hitman.revolucianapp.activity.interfaces;

/**
 * Created by hitman on 12/7/16.
 */
public interface ItemTouchHelperAdapter {
    void onItemMove(int fromPosition, int toPosition);
    void onItemDismiss(int position);
}
