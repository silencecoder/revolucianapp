package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 24/6/16.
 */
public class ReviewModel {
    int id;
    String user;
    String review;
    double rating;

    public ReviewModel(int id, String user, String review, double rating) {
        this.user = user;
        this.review = review;
        this.rating = rating;
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
