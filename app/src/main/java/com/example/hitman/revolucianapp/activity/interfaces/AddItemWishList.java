package com.example.hitman.revolucianapp.activity.interfaces;

/**
 * Created by hitman on 8/7/16.
 */
public interface AddItemWishList {

    public void addShopItemWishList(int itemID);
}
