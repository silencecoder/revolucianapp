package com.example.hitman.revolucianapp.activity.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager;
import com.example.hitman.revolucianapp.activity.adapter.ReviewAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.ReviewModel;

import java.util.ArrayList;

public class ReviewActivity extends AppCompatActivity {

    private int shopID;
    private String category;

    private Toolbar toolbar;
    private RecyclerView reviewsRecyclerView;
    private View mProgressView;
    private LinearLayout reviewPage;

    private ServerDB serverDB;
    private ReviewAdapter reviewAdapter;
    private ArrayList<ReviewModel> reviewModels;
    private ReviewTask reviewTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        category = getIntent().getStringExtra("category");
        shopID = getIntent().getIntExtra("shopID", 0);

        mProgressView = findViewById(R.id.review_progress);
        reviewsRecyclerView = (RecyclerView) findViewById(R.id.review_recycler_view);
        reviewPage = (LinearLayout) findViewById(R.id.review_page);

        reviewModels = new ArrayList<>();
        reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);

        reviewsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        reviewsRecyclerView.setNestedScrollingEnabled(false);
        reviewsRecyclerView.setHasFixedSize(false);
        reviewsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(13));
        reviewsRecyclerView.setAdapter(reviewAdapter);

        showProgress(true);
        reviewTask = new ReviewTask(shopID, category);
        reviewTask.execute((Void) null);

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            reviewsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            reviewPage.setVisibility(show ? View.GONE : View.VISIBLE);
            reviewPage.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    reviewPage.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            reviewsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            reviewPage.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private class ReviewTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;
        String category;

        ReviewTask(int shopID, String category) {
            this.shopID = shopID;
            this.category = category;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }

            if(category.equals("shop"))
                reviewModels = serverDB.getShopReviews(shopID, false);
            else if(category.equals("food"))
                reviewModels = serverDB.getFoodShopReviews(shopID, false);
                return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);
            if(success) {
                reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);
                reviewsRecyclerView.setAdapter(reviewAdapter);
                reviewsRecyclerView.setVisibility(View.VISIBLE);
            } else {

            }
        }
    }
}
