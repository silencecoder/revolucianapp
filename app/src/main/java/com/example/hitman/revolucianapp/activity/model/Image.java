package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 24/5/16.
 */
public class Image  {

    private int name;

    private String offerShopName;
    private String offerDesc;
    private String offerExpiryDate;

    public Image(int name, String offerShopName, String offerDesc, String offerExpiryDate) {
        this.name = name;
        this.offerShopName = offerDesc;
        this.offerExpiryDate = offerExpiryDate;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getName() {
        return name;
    }

    public String getOfferDesc() {
        return offerDesc;
    }

    public void setOfferDesc(String offerDesc) {
        this.offerDesc = offerDesc;
    }

    public String getOfferShopName() {
        return offerShopName;
    }

    public void setOfferShopName(String offerShopName) {
        this.offerShopName = offerShopName;
    }

    public String getOfferExpiryDate() {
        return offerExpiryDate;
    }

    public void setOfferExpiryDate(String offerExpiryDate) {
        this.offerExpiryDate = offerExpiryDate;
    }

}
