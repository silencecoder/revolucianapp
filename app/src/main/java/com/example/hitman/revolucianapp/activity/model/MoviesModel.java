package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 31/5/16.
 */
public class MoviesModel {
    int id;
    int image;
    String movieName;
    double movieRating;
    String movieAction;

    public MoviesModel(int image, String movieName, double movieRating, String movieAction) {
        this.image = image;
        this.movieName = movieName;
        this.movieRating = movieRating;
        this.movieAction = movieAction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public double getMovieRating() {
        return movieRating;
    }

    public void setMovieRating(double movieRating) {
        this.movieRating = movieRating;
    }

    public String getMovieAction() {
        return movieAction;
    }

    public void setMovieAction(String movieAction) {
        this.movieAction = movieAction;
    }
}
