package com.example.hitman.revolucianapp.activity.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.adapter.ViewPagerAdapter;
import com.example.hitman.revolucianapp.activity.fragment.FoodFragment;
import com.example.hitman.revolucianapp.activity.fragment.MallFragment;
import com.example.hitman.revolucianapp.activity.fragment.MoviesFragment;
import com.example.hitman.revolucianapp.activity.fragment.OffersFragment;
import com.example.hitman.revolucianapp.activity.fragment.WishListItemFragment;
import com.example.hitman.revolucianapp.activity.fragment.WishListShopFragment;

public class WishListActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new WishListShopFragment(), "Shops");
        viewPagerAdapter.addFragment(new WishListItemFragment(), "Items");
        viewPager.setAdapter(viewPagerAdapter);
    }
}
