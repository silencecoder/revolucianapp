package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.interfaces.ItemTouchHelperAdapter;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.holder.FoodViewHolder;
import com.example.hitman.revolucianapp.activity.holder.MallViewHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by hitman on 28/6/16.
 */
public class DeckCardAdapter extends InterceptorView.Adapter<RecyclerView.ViewHolder> implements ItemTouchHelperAdapter {

    private int deckID;

    private final int MALL = 0;
    private final int FOOD = 1;
    private final int MOVIE = 2;

    private List<Object> items;
    private Context context;
    DatabaseAdapter databaseAdapter;

    public DeckCardAdapter(Context context, List<Object> items, int deckID) {
        this.context = context;
        this.items = items;
        this.deckID = deckID;
        databaseAdapter = new DatabaseAdapter(context);
    }

    @Override
    public void onItemDismiss(int position) {
        items.remove(position);
        if(items.get(position) instanceof  MallModel) {
            databaseAdapter.deleteMallCard( deckID, ((MallModel) items.get(position)).getId());
        } else if(items.get(position) instanceof FoodShopModel) {
            databaseAdapter.deleteFoodCard(deckID, ((FoodShopModel) items.get(position)).getId());
        }
        notifyItemRemoved(position);
        notifyDataSetChanged();

    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(items, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(items, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof MallModel) {
            return MALL;
        } else if (items.get(position) instanceof FoodShopModel) {
            return FOOD;
        } else
            return MOVIE;
    }

    public void animate(RecyclerView.ViewHolder viewHolder) {
        final Animation animAnticipateOvershoot = AnimationUtils.loadAnimation(context, R.anim.anticipateovershoot_interpolator);
        viewHolder.itemView.setAnimation(animAnticipateOvershoot);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        //More to come
        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (viewType) {
            case MALL:
                View v1 = inflater.inflate(R.layout.deck_card_mall_item, viewGroup, false);
                viewHolder = new MallViewHolder(v1);
                break;
            case FOOD:
                View v2 = inflater.inflate(R.layout.deck_card_food_item, viewGroup, false);
                viewHolder = new FoodViewHolder(v2);
                break;
            case MOVIE:
                View v3 = inflater.inflate(R.layout.deck_card_mall_item, viewGroup, false);
                viewHolder = new MallViewHolder(v3);
                break;
            default:
                View v4 = inflater.inflate(R.layout.deck_card_mall_item, viewGroup, false);
                viewHolder = new MallViewHolder(v4);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        //More to come
        switch (viewHolder.getItemViewType()) {
            case MALL:
                MallViewHolder v1 = (MallViewHolder) viewHolder;
                configureMallViewHolder(v1, position);
                animate(v1);
                break;
            case FOOD:
                FoodViewHolder v2 = (FoodViewHolder) viewHolder;
                configureFoodViewHolder(v2, position);
                animate(v2);
                break;
            case MOVIE:
                break;
            default:
                MallViewHolder v4 = (MallViewHolder) viewHolder;
                configureMallViewHolder(v4, position);
                animate(v4);
        }
    }

    private void configureMallViewHolder(MallViewHolder v, int position) {
        MallModel mallModel = (MallModel) items.get(position);
        if(mallModel != null) {
            v.cardShopName.setText(mallModel.getShopName());
            v.cardShopLocation.setText(mallModel.getArea());
            v.cardShopTimings.setText(mallModel.getShopTimings());
        }
    }

    private void configureFoodViewHolder(FoodViewHolder v, int position) {
        FoodShopModel foodShopModel = (FoodShopModel) items.get(position);
        if(foodShopModel != null) {
            v.cardFoodShopName.setText(foodShopModel.getShopName());
            v.cardFoodShopLocation.setText(foodShopModel.getShopLocation());
            v.cardFoodShopTimings.setText(foodShopModel.getShopTimings());
            v.cardFoodShopCost.setText(foodShopModel.getShopCost());
        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private DeckCardAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final DeckCardAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
