package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 3/6/16.
 */
public class OfferModel {
    int offerId;
    int image;
    int fabImage;
    int shopID;
    String  offerShopName;
    String  offerShopLocation;
    double offerShopLatitude;
    double offerShopLongitude;
    double offerShopDistance;
    String offerDescription;

    public OfferModel(int image, int offerID, int shopID, String offerShopName, String offerShopLocation, double offerShopLatitude, double offerShopLongitude,
                      String offerDescription) {
        this.offerId = offerID;
        this.shopID = shopID;
        this.image = image;
        this.offerShopName = offerShopName;
        this.offerShopLocation = offerShopLocation;
        this.offerShopLatitude = offerShopLatitude;
        this.offerShopLongitude = offerShopLongitude;
        this.offerDescription = offerDescription;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getFabImage() {
        return fabImage;
    }

    public void setFabImage(int fabImage) {
        this.fabImage = fabImage;
    }

    public String getOfferShopName() {
        return offerShopName;
    }

    public void setOfferShopName(String offerShopName) {
        this.offerShopName = offerShopName;
    }

    public String getOfferShopLocation() {
        return offerShopLocation;
    }

    public void setOfferShopLocation(String offerShopLocation) {
        this.offerShopLocation = offerShopLocation;
    }

    public double getOfferShopDistance() {
        return offerShopDistance;
    }

    public double getOfferShopLatitude() {
        return offerShopLatitude;
    }

    public void setOfferShopLatitude(double offerShopLatitude) {
        this.offerShopLatitude = offerShopLatitude;
    }

    public double getOfferShopLongitude() {
        return offerShopLongitude;
    }

    public void setOfferShopLongitude(double offerShopLongitude) {
        this.offerShopLongitude = offerShopLongitude;
    }

    public void setOfferShopDistance(double offerShopDistance) {
        this.offerShopDistance = offerShopDistance;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }
}
