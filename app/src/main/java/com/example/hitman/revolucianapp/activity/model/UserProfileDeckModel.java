package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 8/6/16.
 */
public class UserProfileDeckModel {
    int id;
    int image;
    String title;
    String user;//Username who created the deck;
    int numberOfCards;
    String description;

    public UserProfileDeckModel(int id, int image, String title, String user, String description, int numberOfCards) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.user = user;
        this.description = description;
        this.numberOfCards = numberOfCards;
    }

    public UserProfileDeckModel(int id, int image, String title, String user, String description) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.user = user;
        this.description = description;
        this.numberOfCards = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }

    public void setNumberOfCards(int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
