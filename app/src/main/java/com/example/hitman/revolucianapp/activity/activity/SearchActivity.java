package com.example.hitman.revolucianapp.activity.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.adapter.MallAdapter;
import com.example.hitman.revolucianapp.activity.adapter.SearchItemAdapter;
import com.example.hitman.revolucianapp.activity.adapter.ViewPagerAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.FilterItemModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.model.SearchItemModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SearchActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
//    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private TabLayout tabLayout;
    public EditText editSearch;
    private ListView listView;
    private TextView reset, apply;
    private TextView filter1, filter2, filter3, filter4;
    private LinearLayout searchFilter, searchSort;
    private AlertDialog alertDialog;
    private RecyclerView searchRecyclerView;
    private static RecyclerView shopRecyclerView;
    private static RecyclerView itemRecyclerView;
    private static View mProgressView;
    private LinearLayout searchLLFilter;

    private static ArrayList<SearchItemModel> itemModels;
    private static ArrayList<MallModel> mallModels;
    private static MallAdapter mallAdapter;
    private static SearchItemAdapter itemAdapter;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<FilterItemModel> arrayList;
    private ArrayList<String> categories;
    private ArrayList<String> foodCategories;
    private ArrayList<String> mallCategories;
    private ArrayList<String> entertainmentCategories;
    private ArrayList<String> offersCategories;
    private Intent intent;
    private FilterAdapter filterAdapter;
    private int filterCategory;
    private ServerDB serverDB;

    private List<String> itemsList;

    private List<String> followedMallArray;
    private ArrayList<FilterItemModel> followedMallList;

    private List<String> priceMallArray;
    private ArrayList<FilterItemModel> priceMallList;

    private List<String> discountMallArray;
    private ArrayList<FilterItemModel> discountMallList;

    private List<String> categoryMallArray;
    private ArrayList<FilterItemModel> categoryMallList;

    private List<String> followedFoodArray;
    private ArrayList<FilterItemModel> followedFoodList;

    private List<String> priceFoodArray;
    private ArrayList<FilterItemModel> priceFoodList;

    private List<String> discontFoodArray;
    private ArrayList<FilterItemModel> discountFoodList;

    private List<String> categoryFoodArray;
    private ArrayList<FilterItemModel> categoryFoodList;

    private List<String> followedEntertainmentArray;
    private ArrayList<FilterItemModel> followedEntertainmentList;

    private List<String> priceEntertainmentArray;
    private ArrayList<FilterItemModel> priceEntertainmentList;

    private List<String> discountEntertainmentArray;
    private ArrayList<FilterItemModel> discountEntertainmentList;

    private List<String> categoryEntertainmentArray;
    private ArrayList<FilterItemModel> categoryEntertainmentList;

    private HashMap<String, ArrayList<String> > filterCategories;
    private HashMap<String, ArrayList<FilterItemModel> > mallCategoryDescription;
    private HashMap<String, ArrayList<FilterItemModel> > foodCategoryDescription;
    private HashMap<String, ArrayList<FilterItemModel> > entertainmentCategoryDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        editSearch = (EditText) findViewById(R.id.search_search);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
//        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.container);
        searchFilter = (LinearLayout) findViewById(R.id.search_filter);
        searchSort = (LinearLayout) findViewById(R.id.search_sort);
        searchRecyclerView = (RecyclerView) findViewById(R.id.search_recycler_view);
        mProgressView = findViewById(R.id.search_progress);
        searchLLFilter = (LinearLayout) findViewById(R.id.search_ll_filter);

//        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);

        mallModels = new ArrayList<>();
        mallAdapter = new MallAdapter(getApplicationContext(), mallModels);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        searchRecyclerView.setLayoutManager(mLayoutManager);
        searchRecyclerView.setItemAnimator(new DefaultItemAnimator());
        searchRecyclerView.setNestedScrollingEnabled(false);
        searchRecyclerView.setHasFixedSize(false);
        searchRecyclerView.addItemDecoration(new DividerItemDecoration(8));
        searchRecyclerView.setAdapter(mallAdapter);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                filterCategory = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        mViewPager.setAdapter(mSectionsPagerAdapter);

//
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        searchFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpFoodFilterOption();
            }
        });
        searchSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupFoodSortOption();
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
//        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPagerAdapter.addFragment(new SearchMallFragment(), "ShoppingMall");
//        viewPagerAdapter.addFragment(new SearchFoodFragment(), "Food");
//        viewPagerAdapter.addFragment(new SearchMoviesFragment(), "Entertainment");
//        viewPagerAdapter.addFragment(new SearchOffersFragment(), "Offers");
//        viewPagerAdapter.addFragment(new SearchTrendingFragment(), "Trending");
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search1, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button
        Log.d("Query submit", query);
        if(query.length() == 0)
            return false;
        search(query);
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text
//        Log.d("Query change", newText);
        return false;
    }

    private void search(String input) {
        itemsList = Arrays.asList(getResources().getStringArray(R.array.items_list));
        String inputArr[] = input.split("[ ]");
        ArrayList<String> list = new ArrayList<>();
        String result = "";
        for (String s: inputArr) {
            list.addAll(searchQuery(s));
        }
        if(list.size() > 0) {
            for (int i = 0; i < list.size()-1; i++)
                result += list.get(i)+"|";
            result += list.get(list.size()-1);
            getSearchItems(result, true);
        } else {
            for(int i = 0; i < inputArr.length-1; i++)
                result += inputArr[i]+"|";
            result += inputArr[inputArr.length - 1];
            getSearchItems(result, false);
        }

    }

    private ArrayList<String> searchQuery(String input) {
        ArrayList<String> list = new ArrayList<>();
        for (String s : itemsList) {
            int x = minDistance(input, s);
//            Log.d("input", s+" "+x);
            if(x <= 2)
                list.add(s);
        }
        return list;
    }

    public static int minDistance(String word1, String word2) {
        int len1 = word1.length();
        int len2 = word2.length();

        // len1+1, len2+1, because finally return dp[len1][len2]
        int[][] dp = new int[len1 + 1][len2 + 1];

        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }

        for (int j = 0; j <= len2; j++) {
            dp[0][j] = j;
        }

        //iterate though, and check last char
        for (int i = 0; i < len1; i++) {
            char c1 = word1.charAt(i);
            for (int j = 0; j < len2; j++) {
                char c2 = word2.charAt(j);

                //if last two chars equal
                if (c1 == c2) {
                    //update dp value for +1 length
                    dp[i + 1][j + 1] = dp[i][j];
                } else {
                    int replace = dp[i][j] + 1;
                    int insert = dp[i][j + 1] + 1;
                    int delete = dp[i + 1][j] + 1;

                    int min = replace > insert ? insert : replace;
                    min = delete > min ? min : delete;
                    dp[i + 1][j + 1] = min;
                }
            }
        }

        return dp[len1][len2];
    }

    private void getSearchItems(String result, boolean flag) {

        viewPager.setVisibility(View.GONE);
        tabLayout.setVisibility(View.GONE);
        searchRecyclerView.setVisibility(View.GONE);

        if(flag) {
            ItemFragment itemFragment = new ItemFragment();
            ShopFragment shopFragment = new ShopFragment();

            viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
            viewPagerAdapter.addFragment(itemFragment, "Item");
            viewPagerAdapter.addFragment(shopFragment, "Shop");
            viewPager.setAdapter(viewPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);
            showProgress(true);

            new SearchItem(result).execute((Void) null);
        } else {
            showProgress(true);
            new SearchShop(result).execute((Void) null);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    // Add an option of sort. When user click on sort button a dialog box will appear which contain list
    private void setupFoodSortOption() {
        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
//        builderSingle.setIcon(R.drawable.ic_launcher);
        builderSingle.setTitle("Sort");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Sort from low to high");
        arrayAdapter.add("Sort from high to low");
        arrayAdapter.add("Popularity from low to high");
        arrayAdapter.add("Popularity from high to low");

        builderSingle.setNegativeButton(
                "cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        builderSingle.setAdapter(
                arrayAdapter,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String strName = arrayAdapter.getItem(which);
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                SearchActivity.this);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Item is");
                        builderInner.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builderInner.show();
                    }
                });
        searchSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builderSingle.show();
            }
        });
    }

    private void setUpFoodFilterOption() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_filter_dialog, null);
        filter1 = (TextView) v.findViewById(R.id.search_filter1);
        filter2 = (TextView) v.findViewById(R.id.search_filter2);
        filter3 = (TextView) v.findViewById(R.id.search_filter3);
        filter4 = (TextView) v.findViewById(R.id.search_filter4);
        listView = (ListView) v.findViewById(R.id.search_filter_list);
        reset = (TextView) v.findViewById(R.id.search_filter_reset);
        apply = (TextView) v.findViewById(R.id.search_filter_set);

        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        addCategories();
        addDescription();
        Log.d("Index", ""+filterCategory);
        filter1.setBackgroundResource(R.color.white);
        filter1.setText("Followed");
        filter2.setText("Price");
        filter3.setText("Discount");
        filter4.setText("Category");

        arrayList = new ArrayList<>();
        if(filterCategory == 0) {
            arrayList = mallCategoryDescription.get("Followed");
        } else if(filterCategory == 1) {
            arrayList = foodCategoryDescription.get("Followed");
        } else if(filterCategory == 2) {
            arrayList = entertainmentCategoryDescription.get("Followed");
        } else if(filterCategory == 3) {

        } else if(filterCategory == 4) {

        }

        filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 1);
//        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, arrayList);
//        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setDividerHeight(0);
        listView.setAdapter(filterAdapter);
        addActionListener();

        builder.setView(v);
        alertDialog = builder.create();
        alertDialog.show();
    }

    private void addActionListener() {
        filter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.white);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Followed");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Followed");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Followed");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 1);
                listView.setAdapter(filterAdapter);
            }
        });

        filter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.white);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Price");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Price");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Price");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 2);
                listView.setAdapter(filterAdapter);
            }
        });

        filter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.white);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Discount");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Discount");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Discount");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 3);
                listView.setAdapter(filterAdapter);
            }
        });

        filter4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.white);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Category");
                } else if(filterCategory == 0) {
                    arrayList = foodCategoryDescription.get("Category");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Category");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 4);
                listView.setAdapter(filterAdapter);
            }
        });
    }

    private void addCategories() {
        offersCategories = new ArrayList<>();

        addSearchCategories();
        addFoodCategories();
        addMallCategories();
        addEntertainmentCategories();
        addFilterCategories();
    }

    private void addSearchCategories() {
        categories = new ArrayList<>();
        categories.add("mall");
        categories.add("food");
        categories.add("entertainment");
        categories.add("offers");
        categories.add("trending");
    }

    private void addFoodCategories() {
        foodCategories = new ArrayList<>();
        foodCategories.add("Followed");
        foodCategories.add("Price");
        foodCategories.add("Discount");
        foodCategories.add("Category");
    }

    private void addMallCategories() {
        mallCategories = new ArrayList<>();
        mallCategories.add("Followed");
        mallCategories.add("Price");
        mallCategories.add("Discount");
        mallCategories.add("Category");
    }

    private void addEntertainmentCategories() {
        entertainmentCategories = new ArrayList<>();
        entertainmentCategories.add("Followed");
        entertainmentCategories.add("Price");
        entertainmentCategories.add("Discount");
        entertainmentCategories.add("Category");
    }

    private void addFilterCategories() {
        filterCategories = new HashMap<String, ArrayList<String> >();
        filterCategories.put(categories.get(0), mallCategories);
        filterCategories.put(categories.get(1), foodCategories);
        filterCategories.put(categories.get(2), entertainmentCategories);
    }

    private void addDescription() {
        mallCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();
        foodCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();
        entertainmentCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();

        addMallDescription();
        addFoodDescription();
        addEntertainmentDescription();
        addCategoryDescription();
    }

    private void addMallDescription() {

        followedMallArray = Arrays.asList(getResources().getStringArray(R.array.followedMallArray));
        followedMallList = new ArrayList<>();
        for(String s: followedMallArray)
            followedMallList.add(new FilterItemModel(s));

        priceMallArray = Arrays.asList(getResources().getStringArray(R.array.priceMallArray));
        priceMallList = new ArrayList<>();
        for (String s: priceMallArray)
            priceMallList.add(new FilterItemModel(s));

        discountMallArray = Arrays.asList(getResources().getStringArray(R.array.discountMallArray));
        discountMallList = new ArrayList<>();
        for (String s: discountMallArray)
            discountMallList.add(new FilterItemModel(s));

        categoryMallArray = Arrays.asList(getResources().getStringArray(R.array.categoryMallArray));
        categoryMallList = new ArrayList<>();
        for (String s: categoryMallArray)
            categoryMallList.add(new FilterItemModel(s));
    }

    private void addFoodDescription() {
        followedFoodArray = Arrays.asList(getResources().getStringArray(R.array.followedFoodArray));
        followedFoodList = new ArrayList<>();
        for (String s: followedFoodArray)
            followedFoodList.add(new FilterItemModel(s));

        priceFoodArray = Arrays.asList(getResources().getStringArray(R.array.priceFoodArray));
        priceFoodList = new ArrayList<>();
        for (String s: priceFoodArray)
            priceFoodList.add(new FilterItemModel(s));

        discontFoodArray = Arrays.asList(getResources().getStringArray(R.array.discountFoodArray));
        discountFoodList = new ArrayList<>();
        for(String s: discontFoodArray)
            discountFoodList.add(new FilterItemModel(s));

        categoryFoodArray = Arrays.asList(getResources().getStringArray(R.array.categoryFoodArray));
        categoryFoodList = new ArrayList<>();
        for(String s: categoryFoodArray)
            categoryFoodList.add(new FilterItemModel(s));
    }

    private void addEntertainmentDescription() {
        followedEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.followedEntertainmentArray));
        followedEntertainmentList = new ArrayList<>();
        for (String s: followedEntertainmentArray)
            followedEntertainmentList.add(new FilterItemModel(s));

        priceEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.priceEntertainmentArray));
        priceEntertainmentList = new ArrayList<>();
        for (String s: priceEntertainmentArray)
            priceEntertainmentList.add(new FilterItemModel(s));

        discountEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.discountEntertainmentArray));
        discountEntertainmentList = new ArrayList<>();
        for (String s: discountEntertainmentArray)
            discountEntertainmentList.add(new FilterItemModel(s));

        categoryEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.categoryEntertainmentArray));
        categoryEntertainmentList = new ArrayList<>();
        for (String s: categoryEntertainmentArray)
            categoryEntertainmentList.add(new FilterItemModel(s));
    }

    private void addCategoryDescription() {
        mallCategoryDescription.put("Followed", followedMallList);
        mallCategoryDescription.put("Price", priceMallList);
        mallCategoryDescription.put("Discount", discountMallList);
        mallCategoryDescription.put("Category", categoryMallList);

        foodCategoryDescription.put("Followed", followedFoodList);
        foodCategoryDescription.put("Price", priceFoodList);
        foodCategoryDescription.put("Discount", discountFoodList);
        foodCategoryDescription.put("Discount", categoryFoodList);

        entertainmentCategoryDescription.put("Followed", followedEntertainmentList);
        entertainmentCategoryDescription.put("Price", priceEntertainmentList);
        entertainmentCategoryDescription.put("Discount", discountEntertainmentList);
        entertainmentCategoryDescription.put("Category", categoryEntertainmentList);
    }

    public class FilterAdapter extends BaseAdapter {

        Context context;
        private ArrayList<FilterItemModel> filterItemModels;
        private int filterNumber;
        private int filterCategory;

        public FilterAdapter(Context c, ArrayList<FilterItemModel> filterItemModels, int filterCategory, int filterNumber) {
            // TODO Auto-generated constructor stub
            this.context = c;
            this.filterItemModels = filterItemModels;
            this.filterNumber = filterNumber;
            this.filterCategory = filterCategory;
            Log.d("Category", filterCategory+"");
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return filterItemModels.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return filterItemModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View rootView = convertView;

            if(rootView == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rootView = inflater.inflate(R.layout.search_filter_list_item, parent, false);
            }
            final ImageView img = (ImageView) rootView.findViewById(R.id.search_filter_check);
            TextView title = (TextView) rootView.findViewById(R.id.search_filter_item);

            final FilterItemModel row = filterItemModels.get(position);
            Log.d("Error", "Message: "+position+" title = "+row.getTitle());
            title.setText(row.getTitle());
            final int pos = position;

            if(row.isFlag())
                img.setImageResource(R.drawable.btn_check_on_holo_light);
            else
                img.setImageResource(R.drawable.btn_check_off_holo_light);

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(row.isFlag()) {
                        img.setImageResource(R.drawable.btn_check_off_holo_light);
                        row.setFlag(false);
                    } else {
                        img.setImageResource(R.drawable.btn_check_on_holo_light);
                        row.setFlag(true);
                    }
                    changeData(pos, filterCategory, filterNumber, row.isFlag());
                }
            });
            return rootView;
        }

        private void changeData(int position, int filterCategory, int filterNumber, boolean flag) {
            if(filterNumber == 1) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedFoodList.get(position).setFlag(flag);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber == 2) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceFoodList.get(position).setFlag(false);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber == 3) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountFoodList.get(position).setFlag(flag);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber == 4) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Category").get(position).setFlag(flag);
                    categoryMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {

                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Category").get(position).setFlag(flag);
                    categoryEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            }
        }
    }

    public static class ItemFragment extends Fragment {

        private PrefManager prefManager;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View v = inflater.inflate(R.layout.fragment_search_item, container, false);
            itemRecyclerView = (RecyclerView) v.findViewById(R.id.search_item_recycler_view);

            itemModels = new ArrayList<>();
            itemAdapter = new SearchItemAdapter(getContext(), itemModels);
            prefManager = new PrefManager(getContext());

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
            itemRecyclerView.setLayoutManager(mLayoutManager);
            itemRecyclerView.setItemAnimator(new DefaultItemAnimator());
            itemRecyclerView.setNestedScrollingEnabled(false);
            itemRecyclerView.setHasFixedSize(false);
            itemRecyclerView.addItemDecoration(new DividerItemDecoration(8));
            itemRecyclerView.setAdapter(itemAdapter);

//            new WishlistItem().execute((Void) null);

            return v;

        }

    }

    public static class ShopFragment extends Fragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_search_shop, container, false);
            shopRecyclerView = (RecyclerView) v.findViewById(R.id.search_shop_recycler_view);

            mallModels = new ArrayList<>();
            mallAdapter = new MallAdapter(getContext(), mallModels);

            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
            shopRecyclerView.setLayoutManager(mLayoutManager);
            shopRecyclerView.setItemAnimator(new DefaultItemAnimator());
            shopRecyclerView.setNestedScrollingEnabled(false);
            shopRecyclerView.setHasFixedSize(false);
            shopRecyclerView.addItemDecoration(new DividerItemDecoration(8));
            shopRecyclerView.setAdapter(mallAdapter);

            return v;
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    private class SearchItem extends AsyncTask<Void, Void, Boolean> {

        String query;

        SearchItem(String query) {
            this.query = query;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            itemModels = serverDB.getSearchItems(query);
            mallModels = serverDB.getSearchShops(query);

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            showProgress(false);
            if(success) {
                itemAdapter = new SearchItemAdapter(getApplicationContext(), itemModels);
                itemRecyclerView.setAdapter(itemAdapter);

                mallAdapter = new MallAdapter(getApplicationContext(), mallModels);
                shopRecyclerView.setAdapter(mallAdapter);

                tabLayout.setVisibility(View.VISIBLE);
                viewPager.setVisibility(View.VISIBLE);
            }
            super.onPostExecute(success);
        }
    }

    private class SearchShop extends AsyncTask<Void, Void, Boolean> {

        String query;

        SearchShop(String query) {
            this.query = query;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            mallModels = serverDB.getSearchShops1(query);
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            showProgress(false);
            if(success) {
                mallAdapter = new MallAdapter(getApplicationContext(), mallModels);
                searchRecyclerView.setAdapter(mallAdapter);
                searchRecyclerView.setVisibility(View.VISIBLE);
                tabLayout.setVisibility(View.GONE);
                viewPager.setVisibility(View.GONE);
            } else {

            }
            super.onPostExecute(success);
        }
    }

}
