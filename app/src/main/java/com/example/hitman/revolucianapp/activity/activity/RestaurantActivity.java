package com.example.hitman.revolucianapp.activity.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager;
import com.example.hitman.revolucianapp.activity.adapter.RestaurantPageDealsAdapter;
import com.example.hitman.revolucianapp.activity.adapter.ReviewAdapter;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.model.RestaurantPageDealsModel;
import com.example.hitman.revolucianapp.activity.model.ReviewModel;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class RestaurantActivity extends AppCompatActivity implements View.OnClickListener {

    private final int REQUEST_CODE_ASK_PERMISSIONS = 0;
    private int restaurantID;
    private int reviewRating;
    private String restaurantPhoneNumber;

    private Toolbar toolbar;
    private TextView writeReview;
    private TextView restaurantPageName;
    private TextView restaurantPageTiming;
    private TextView restaurantPageCost;
    private TextView restaurantPageAddress;
    private TextView seeAllReview;
    private TextView saveShop;
    private TextView followShop;
    private View mProgressView;
    private ImageView restaurantPageCall;
    private ImageView star1, star2, star3, star4, star5;
    private ServerDB serverDB;
    private LinearLayout restaurantPage;
    private CardView restaurantPageDetails;
    private Intent callIntent;
    private AlertDialog alert;
    private Intent intent;
    private RecyclerView dealsRecyclerView;
    private RecyclerView reviewsRecyclerView;
    private ProgressDialog progressDialog;
    private EditText editReview;
    private ImageView restaurantPageSaveYes;
    private ImageView restaurantPageFollowYes;

    private boolean follow;
    private ArrayList<UserProfileDeckModel> userProfileDeckModels;
    private ReviewAdapter reviewAdapter;
    private RestaurantPageDealsAdapter restaurantPageDealsAdapter;
    private RestaurantTask restaurantTask;
    private ArrayList<ReviewModel> reviewModels;
    private OfferTask offerTask;
    private ArrayList<RestaurantPageDealsModel> deals;
    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;
    private WriteReviewTask writeReviewTask;
    private ReviewTask reviewTask;
    private FollowShopTask followShopTask;
    private UserFollowTask userFollowTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_page);
//        setupWindowAnimations();

        restaurantID = getIntent().getIntExtra("restaurantID", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        saveShop = (TextView) findViewById(R.id.restaurant_page_save);
        followShop = (TextView) findViewById(R.id.restaurant_page_follow);
        writeReview = (TextView) findViewById(R.id.restaurant_page_write_review);
        reviewsRecyclerView = (RecyclerView) findViewById(R.id.restaurant_page_reviews_recycler_view);
        dealsRecyclerView = (RecyclerView) findViewById(R.id.restaurant_page_deals_recycler_view);
        restaurantPageName = (TextView) findViewById(R.id.restaurant_page_name);
        restaurantPageTiming = (TextView) findViewById(R.id.restaurant_page_timing);
        restaurantPageCost = (TextView) findViewById(R.id.restaurant_page_cost);
        restaurantPageAddress = (TextView) findViewById(R.id.restaurant_page_address);
        restaurantPage = (LinearLayout) findViewById(R.id.restaurant_page);
        mProgressView = findViewById(R.id.food_page_progress);
        restaurantPageDetails = (CardView) findViewById(R.id.restaurant_page_details);
        restaurantPageCall = (ImageView) findViewById(R.id.restaurant_page_call);
        callIntent = new Intent(Intent.ACTION_CALL);
        seeAllReview = (TextView) findViewById(R.id.restaurant_page_all_reviews);
        restaurantPageFollowYes = (ImageView) findViewById(R.id.restaurant_page_follow_yes);
        restaurantPageSaveYes = (ImageView) findViewById(R.id.restaurant_page_save_yes);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userProfileDeckModels = new ArrayList<>();
        reviewModels = new ArrayList<>();
        deals = new ArrayList<>();
        addDeals();
        prefManager = new PrefManager(getApplicationContext());
        databaseAdapter = new DatabaseAdapter(getApplicationContext());
        reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);
        restaurantPageDealsAdapter = new RestaurantPageDealsAdapter(getApplicationContext(), deals);

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getApplicationContext());
        dealsRecyclerView.setLayoutManager(mLayoutManager);
        dealsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        dealsRecyclerView.setHasFixedSize(false);
        dealsRecyclerView.setNestedScrollingEnabled(false);
        dealsRecyclerView.addItemDecoration(new DividerItemDecoration(15));
        dealsRecyclerView.setAdapter(restaurantPageDealsAdapter);

        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        reviewsRecyclerView.setLayoutManager(mLayoutManager2);
        reviewsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reviewsRecyclerView.setHasFixedSize(true);
        reviewsRecyclerView.setNestedScrollingEnabled(false);
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(6));
        reviewsRecyclerView.setAdapter(reviewAdapter);

        writeReview.setOnClickListener(this);
        seeAllReview.setOnClickListener(this);
        restaurantPageCall.setOnClickListener(this);
        saveShop.setOnClickListener(this);
        followShop.setOnClickListener(this);

        restaurantTask = new RestaurantTask(restaurantID);
        offerTask = new OfferTask(restaurantID);
        reviewTask = new ReviewTask(restaurantID);
        userFollowTask = new UserFollowTask();

        showProgress(true);
        userFollowTask.execute((Void) null);
        restaurantTask.execute((Void) null);
        offerTask.execute((Void) null);
        reviewTask.execute((Void) null);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    startActivity(callIntent);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.restaurant_page_write_review:
                writeReview();
                break;
            case R.id.restaurant_page_all_reviews:
                intent = new Intent(RestaurantActivity.this, ReviewActivity.class);
                intent.putExtra("category", "food");
                intent.putExtra("shopID", restaurantID);
                startActivity(intent);
                break;
            case R.id.restaurant_page_call:
                Log.d("Request", "call");
                if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasWriteContactsPermission == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[] {Manifest.permission.CALL_PHONE}, REQUEST_CODE_ASK_PERMISSIONS);
                    } else
                        startActivity(callIntent);
                } else
                    startActivity(callIntent);
                break;
            case R.id.restaurant_page_save:
                restaurantPageSaveYes.setVisibility(View.VISIBLE);
                saveShopInDeck();
                break;
            case R.id.restaurant_page_follow:
                followShopTask = new FollowShopTask(prefManager.getUserId(), restaurantID);
                followShopTask.execute((Void) null);
                break;
        }
    }

    @TargetApi(21)
    private void setupWindowAnimations() {
        Fade fade = new Fade();
        fade.setDuration(1000);
        getWindow().setEnterTransition(fade);

        Slide slide = new Slide();
        fade.setDuration(1000);
        getWindow().setReturnTransition(slide);
    }

    private void addDeals() {
//        deals.add(new RestaurantPageDealsModel("30% cashback on bill of Rs 1000 above"));
//        deals.add(new RestaurantPageDealsModel("Buy 2 get 1 free"));
//        deals.add(new RestaurantPageDealsModel("50% cashback on bill of Rs 5000 above"));
    }

    private void writeReview() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(RestaurantActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_dialog_review, null);
        star1 =  (ImageView) v.findViewById(R.id.review_star1);
        star2 =  (ImageView) v.findViewById(R.id.review_star2);
        star3 =  (ImageView) v.findViewById(R.id.review_star3);
        star4 =  (ImageView) v.findViewById(R.id.review_star4);
        star5 =  (ImageView) v.findViewById(R.id.review_star5);
        editReview = (EditText) v.findViewById(R.id.edit_text_review);
        addOnClick();

        builder.setTitle("Review for Shop"+restaurantPageName.getText().toString())
                .setView(v)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ..
                        writeReviewTask = new WriteReviewTask();
                        writeReviewTask.execute((Void) null);
                        Toast.makeText(getApplicationContext(), "Yes working", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void addOnClick() {
        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 1;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10_light);
                star3.setImageResource(R.drawable.ic_action_star_10_light);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 2;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10_light);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 3;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 4;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 5;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10);
                star5.setImageResource(R.drawable.ic_action_star_10);
            }
        });
    }

    // Save shop details in deck when user clicks the save button
    private void saveShopInDeck() {
        userProfileDeckModels = databaseAdapter.getAllDecks();
        String[] items = new String[userProfileDeckModels.size()];
        for(int i = 0; i < userProfileDeckModels.size(); i++)
            items[i] = userProfileDeckModels.get(i).getTitle();
        final AlertDialog.Builder builder = new AlertDialog.Builder(RestaurantActivity.this);
        LayoutInflater inflater = getLayoutInflater();
//        View v = inflater.inflate(R.layout.custom_dialog_list, null);
//        listView = (ListView) v.findViewById(R.id.list_view);
//        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
//        listView.setAdapter(arrayAdapter);
        builder.setTitle("Select a deck");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                saveCardInDeck(which);
                alert.dismiss();
                intent = new Intent(RestaurantActivity.this, CardActivity.class);
                intent.putExtra("deckID", userProfileDeckModels.get(which).getId());
                startActivity(intent);
            }
        });
        alert = builder.create();
        alert.show();

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                saveCardInDeck(position);
//                alert.dismiss();
//                intent = new Intent(ShopPageActivity.this, CardActivity.class);
//                intent.putExtra("deckID", userProfileDeckModels.get(position).getId());
//                startActivity(intent);
//            }
//        });
    }

    private void saveCardInDeck(int position) {
        long id = databaseAdapter.insertCardInDeck(userProfileDeckModels.get(position).getId());
        Log.e("card id", id+"");
        FoodShopModel foodShopModel = new FoodShopModel(restaurantID, restaurantPageName.getText().toString(), restaurantPageAddress.getText().toString(),
                restaurantPageTiming.getText().toString(), restaurantPageCost.getText().toString(), restaurantPhoneNumber);
        long a = databaseAdapter.insertFoodCard(foodShopModel, (int) id);
        long b = databaseAdapter.setCardCategory("food", (int) id);
        Log.d("Insert Id", a+" "+b);
    }

    private void startDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging In. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

   /* @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            restaurantPageDetails.setVisibility(show? View.GONE:View.VISIBLE);
            dealsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            reviewsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            restaurantPage.setVisibility(show ? View.GONE : View.VISIBLE);
            restaurantPage.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    restaurantPage.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            restaurantPageDetails.setVisibility(show? View.GONE:View.VISIBLE);
            dealsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            reviewsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            restaurantPage.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private class RestaurantTask extends AsyncTask<Void, Void, Boolean> {

        int restaurantID;
        ArrayList<Object> restaurantDetails;

        RestaurantTask(int restaurantID) {
            this.restaurantID = restaurantID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            restaurantDetails = serverDB.getFoodShopDetails(restaurantID);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            restaurantTask = null;
            if(success) {
                Log.d("Shop Size", restaurantDetails.size()+"");
                restaurantPageDetails.setVisibility(View.VISIBLE);
                restaurantPageName.setText((String) restaurantDetails.get(0));
                restaurantPageAddress.setText((String) restaurantDetails.get(1));
                restaurantPageCost.setText((String) restaurantDetails.get(2));
                restaurantPageTiming.setText((String) restaurantDetails.get(3));
                restaurantPhoneNumber = (String) restaurantDetails.get(4);
                callIntent.setData(Uri.parse("tel:" + restaurantPhoneNumber));
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class OfferTask extends AsyncTask<Void, Void, Boolean> {

        int restaurantID;

        OfferTask(int restaurantID) {
            this.restaurantID = restaurantID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            deals = serverDB.getRestaurantOffers(restaurantID);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            offerTask = null;
            showProgress(false);
            if(success) {
                Log.d("offers", deals.size()+"");
                restaurantPageDealsAdapter= new RestaurantPageDealsAdapter(getApplicationContext(), deals);
                dealsRecyclerView.setAdapter(restaurantPageDealsAdapter);
                dealsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class ReviewTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;

        ReviewTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            reviewModels = serverDB.getFoodShopReviews(shopID, true);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            Log.d("Reviews Size: ", reviewModels.size()+"");
            if(success) {
                reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);
                reviewsRecyclerView.setAdapter(reviewAdapter);
                reviewsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class WriteReviewTask extends AsyncTask<Void, Void, Boolean> {

        String username;
        String review;

        @Override
        protected void onPreExecute() {
            startDialog();
            username = databaseAdapter.getUserName(prefManager.getUserEmail());
            review = editReview.getText().toString();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            serverDB.writeFoodShopReview(restaurantID, username, review, reviewRating);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            progressDialog.dismiss();
            if(success) {

            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class FollowShopTask extends AsyncTask<Void, Void, Boolean> {

        int userID;
        int shopID;

        FollowShopTask(int userID, int shopID) {
            this.userID = userID;
            this.shopID = shopID;
        }

        @Override
        protected void onPreExecute() {
            startDialog();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            if(userID <= 0)
                userID= serverDB.getUserID(prefManager.getUserEmail());
            if(!follow) {
                serverDB.setUserFollowFoodShop(userID, shopID);
                follow = true;
            } else {
                serverDB.setUseUnrFollowFoodShop(userID, shopID);
                follow = false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            progressDialog.dismiss();
            if(success) {
                if(follow) {
                    restaurantPageFollowYes.setVisibility(View.VISIBLE);
                    followShop.setText("UNFOLLOW");
                } else {
                    restaurantPageFollowYes.setVisibility(View.GONE);
                    followShop.setText("FOLLOW");
                }
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    // Check user follow the shop or not
    private class UserFollowTask extends AsyncTask<Void, Void, Boolean> {

        int userId;

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            userId = prefManager.getUserId();
            if(userId <= 0) {
                userId = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userId);
            }
            follow =  serverDB.isUserFollowRestaurant(prefManager.getUserId(), restaurantID);
            return follow;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if(success) {
                Log.d("User Followed", "Yes");
                restaurantPageFollowYes.setVisibility(View.VISIBLE);
                followShop.setText("UNFOLLOW");
            } else {
                Log.d("User Followed", "No");
                restaurantPageFollowYes.setVisibility(View.INVISIBLE);
                followShop.setText("FOLLOW");
            }
        }
    }
}