package com.example.hitman.revolucianapp.activity.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager;
import com.example.hitman.revolucianapp.activity.adapter.DeckCardAdapter;
import com.example.hitman.revolucianapp.activity.callbacks.SimpleItemTouchHelperCallback;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.interfaces.ItemTouchHelperAdapter;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;

import java.util.ArrayList;

public class CardActivity extends AppCompatActivity {

    private int deckID;

    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private TextView deckTitle, deckDesc, deckUser, deckNoOfCards;
    private ImageView deckUserImage;
    private ItemTouchHelper.Callback callback;
    private ItemTouchHelper touchHelper;
    private Intent intent;

    private ArrayList<Object> cards;
    private DatabaseAdapter databaseAdapter;
    private DeckCardAdapter deckCardAdapter;

    private float historicX = Float.NaN, historicY = Float.NaN;
    static final int DELTA = 50;
    enum Direction {LEFT, RIGHT;}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        deckID = getIntent().getIntExtra("deckID", 0);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        deckTitle = (TextView) findViewById(R.id.deck_card_title);
        deckDesc = (TextView) findViewById(R.id.deck_card_desc);
        deckUser = (TextView) findViewById(R.id.deck_card_user);
        deckNoOfCards = (TextView) findViewById(R.id.deck_card_no_cards);
        deckUserImage = (ImageView) findViewById(R.id.deck_card_image);
        recyclerView = (RecyclerView) findViewById(R.id.deck_card_recycler_view);

        cards = new ArrayList<>();
        deckCardAdapter = new DeckCardAdapter(getApplicationContext(), cards, deckID);
        databaseAdapter = new DatabaseAdapter(getApplicationContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(10));
        recyclerView.setAdapter(deckCardAdapter);

        callback = new SimpleItemTouchHelperCallback(deckCardAdapter);
        touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(recyclerView);

        setDeckDetails();
        getCards();

//        recyclerView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                switch (event.getAction()) {
//                    case MotionEvent.ACTION_DOWN:
//                        historicX = event.getX();
//                        historicY = event.getY();
//                        Toast.makeText(getApplicationContext(), "Item " + v.getId() + " down", Toast.LENGTH_SHORT).show();
//                        break;
//                    case MotionEvent.ACTION_UP:
//                        if (event.getX() - historicX < -DELTA) {
////                            FunctionDeleteRowWhenSlidingLeft();
//                            Toast.makeText(getApplicationContext(), "Item " + v.getId() + " left clicked", Toast.LENGTH_SHORT).show();
//                            return true;
//                        }
//                        else if (event.getX() - historicX > DELTA)
//                        {
////                            FunctionDeleteRowWhenSlidingRight();
//                            Toast.makeText(getApplicationContext(), "Item " + v.getId() + " right clicked", Toast.LENGTH_SHORT).show();
//                            return true;
//                        }
//                        break;
//                    default:
//                        return false;
//                }
//
//                return false;
//            }
//        });

        recyclerView.addOnItemTouchListener(new DeckCardAdapter.RecyclerTouchListener(getApplicationContext(), recyclerView, new DeckCardAdapter.ClickListener(){
            @Override
            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideShowDialogFragment newFragment = SlideShowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");

//                Intent intent = new Intent(getActivity(), ShopPageActivity.class);
//                intent.putExtra("shopID", malls.get(position).getId());
                if(cards.get(position) instanceof MallModel) {
                    intent = new Intent(CardActivity.this, ShopPageActivity.class);
                    intent.putExtra("shopID", ((MallModel) cards.get(position)).getId());
                }else if(cards.get(position) instanceof FoodShopModel) {
                    intent = new Intent(CardActivity.this, RestaurantActivity.class);
                    intent.putExtra("restaurantID", ((FoodShopModel) cards.get(position)).getId());
                }
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        recyclerView.setOnTouchListener(new OnSwipeTouchListener(getApplicationContext()){

            public void onSwipeLeft() {
                Toast.makeText(getApplicationContext(), "Swipe left", Toast.LENGTH_LONG).show();
            }

            public void onSwipeRight() {
                Toast.makeText(getApplicationContext(), "Swipe right", Toast.LENGTH_LONG).show();
            }

        });
    }

    private void setDeckDetails() {
        ArrayList<String> deckDetails = databaseAdapter.getDeckDetails(deckID);
        deckTitle.setText(deckDetails.get(0));
        deckDesc.setText(deckDetails.get(1));
        deckUser.setText(deckDetails.get(2));
        deckNoOfCards.setText(deckDetails.get(3)+" cards");
    }

    private void getCards() {
        cards = databaseAdapter.getCards(deckID);
        for (Object object : cards)
            printCard(object);
        deckCardAdapter = new DeckCardAdapter(getApplicationContext(), cards, deckID);
        recyclerView.setAdapter(deckCardAdapter);
    }

    private void printCard(Object object) {
        if(object instanceof MallModel) {
            Log.e("card", ((MallModel) object).getShopName()+((MallModel) object).getShopTimings()+((MallModel) object).getShopNumber());
        } else if(object instanceof FoodShopModel) {
            Log.e("card1", ((FoodShopModel) object).getShopCost()+ ((FoodShopModel) object).getShopNumber()+((FoodShopModel) object).getShopName());
        }
    }

    class OnSwipeTouchListener implements View.OnTouchListener {

        private final GestureDetector gestureDetector;

        public OnSwipeTouchListener (Context ctx){
            gestureDetector = new GestureDetector(ctx, new GestureListener());
        }

        private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

            private static final int SWIPE_THRESHOLD = 100;
            private static final int SWIPE_VELOCITY_THRESHOLD = 100;

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                boolean result = false;
                try {
                    float diffY = e2.getY() - e1.getY();
                    float diffX = e2.getX() - e1.getX();
                    if (Math.abs(diffX) > Math.abs(diffY)) {
                        if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                            if (diffX > 0) {
                                onSwipeRight();
                            } else {
                                onSwipeLeft();
                            }
                        }
                        result = true;
                    }
                    else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffY > 0) {
                            onSwipeBottom();
                        } else {
                            onSwipeTop();
                        }
                    }
                    result = true;

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                return result;
            }
        }

        public void onSwipeRight(){
        }

        public void onSwipeLeft(){
        }

        public void onSwipeTop(){
        }

        public void onSwipeBottom(){
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            // TODO Auto-generated method stub
            return gestureDetector.onTouchEvent(event);
        }
    }

}
