package com.example.hitman.revolucianapp.activity.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.activity.FilterActivity;
import com.example.hitman.revolucianapp.activity.adapter.MoviesAdapter;
import com.example.hitman.revolucianapp.activity.model.MoviesModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchMoviesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchMoviesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchMoviesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    private ArrayList<MoviesModel> movies;
    private MoviesAdapter moviesAdapter;
    private RecyclerView recyclerView;
    private EditText edit_search;
//    private LinearLayout movieSearchSort;
//    private LinearLayout movieSearchFilter;
//    private Intent intent;


    public SearchMoviesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchMoviesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchMoviesFragment newInstance(String param1, String param2) {
        SearchMoviesFragment fragment = new SearchMoviesFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_movies, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.search_movies_recycler_view);
//        movieSearchSort = (LinearLayout) v.findViewById(R.id.search_movies_sort);
//        movieSearchFilter = (LinearLayout) v.findViewById(R.id.search_movies_filter);

        movies = new ArrayList<>();

        addMovies();
        moviesAdapter = new MoviesAdapter(getContext(), movies);
        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(8));
        recyclerView.setAdapter(moviesAdapter);

//        setUpMovieFilterOption();
//        setupMovieSortOption();

        return v;
    }

    private void addMovies() {
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
    }

    // Add an option of sort. When user click on sort button a dialog box will appear which contain list
//    private void setupMovieSortOption() {
//        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(getActivity());
////        builderSingle.setIcon(R.drawable.ic_launcher);
//        builderSingle.setTitle("Sort");
//
//        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
//                getActivity(),
//                android.R.layout.select_dialog_singlechoice);
//        arrayAdapter.add("Sort from low to high");
//        arrayAdapter.add("Sort from high to low");
//        arrayAdapter.add("Popularity from low to high");
//        arrayAdapter.add("Popularity from high to low");
//
//        builderSingle.setNegativeButton(
//                "cancel",
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//
//        builderSingle.setAdapter(
//                arrayAdapter,
//                new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        String strName = arrayAdapter.getItem(which);
//                        AlertDialog.Builder builderInner = new AlertDialog.Builder(
//                                getActivity());
//                        builderInner.setMessage(strName);
//                        builderInner.setTitle("Your Selected Item is");
//                        builderInner.setPositiveButton(
//                                "Ok",
//                                new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(
//                                            DialogInterface dialog,
//                                            int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                        builderInner.show();
//                    }
//                });
//        movieSearchSort.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                builderSingle.show();
//            }
//        });
//    }
//
//    private void setUpMovieFilterOption() {
//        movieSearchFilter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                intent = new Intent(getActivity(), FilterActivity.class);
//                intent.putExtra(""+R.string.search_category, "entertainment");
//                startActivity(intent);
//            }
//        });
//    }
//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
