package com.example.hitman.revolucianapp.activity.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.activity.NavigationDrawerActivity;
import com.example.hitman.revolucianapp.activity.activity.SearchActivity;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.adapter.MallAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.utils.GPSTracker;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MallFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MallFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MallFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MallFragment.
     */

    private RecyclerView recyclerView;
    private LinearLayout ll;
//    private EditText edit_search;
    private View mProgressView;
    private LinearLayout mall_ll_store, mall_ll_mall, mall_ll_market;

    private ServerDB serverDB;
    private MallStoreTask mAuthTask;
    private ArrayList<MallModel> malls;
    private MallAdapter mallAdapter;
    private GPSTracker gpsTracker;

    // TODO: Rename and change types and number of parameters
    public static MallFragment newInstance(String param1, String param2) {
        MallFragment fragment = new MallFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public MallFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_mall, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.mall_ll_recycler_view);
//        edit_search = (EditText) v.findViewById(R.id.mall_edit_text_search);

//        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        malls = new ArrayList<>();
        //addMalls();
        mallAdapter = new MallAdapter(getActivity().getBaseContext(), malls);

        ll = (LinearLayout) v.findViewById(R.id.ll);
        mall_ll_store = (LinearLayout) v.findViewById(R.id.mall_ll_store);
        mall_ll_mall = (LinearLayout) v.findViewById(R.id.mall_ll_mall);
        mall_ll_market = (LinearLayout) v.findViewById(R.id.mall_ll_market);
        mProgressView = v.findViewById(R.id.mall_progress);
        //fadeOutAndHideImage(ll);

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(8));
        recyclerView.setAdapter(mallAdapter);
        gpsTracker = new GPSTracker(getContext(), getActivity());

        recyclerView.addOnItemTouchListener(new MallAdapter.RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new MallAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideShowDialogFragment newFragment = SlideShowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");

                Intent intent = new Intent(getActivity(), ShopPageActivity.class);
                intent.putExtra("shopID", malls.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        mall_ll_store.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new MallStoreTask(1);
                mAuthTask.execute((Void) null);
            }
        });

        mall_ll_mall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new MallStoreTask(2);
                mAuthTask.execute((Void) null);
            }
        });

        mall_ll_market.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new MallStoreTask(3);
                mAuthTask.execute((Void) null);
            }
        });

//        edit_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SearchActivity.class));
//            }
//        });

        return v;
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    private void toolbarSetElevation(float elevation) {
//        // setElevation() only works on Lollipop
//        toolbar.setElevation(elevation);
//    }
//
//    private void toolbarAnimateShow(final int verticalOffset) {
//        toolbar.animate()
//                .translationY(0)
//                .setInterpolator(new LinearInterpolator())
//                .setDuration(180)
//                .setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationStart(Animator animation) {
//                        toolbarSetElevation(verticalOffset == 0 ? 0 : TOOLBAR_ELEVATION);
//                    }
//                });
//    }
//
//    private void toolbarAnimateHide() {
//        toolbar.animate()
//                .translationY(-toolbar.getHeight())
//                .setInterpolator(new LinearInterpolator())
//                .setDuration(180)
//                .setListener(new AnimatorListenerAdapter() {
//                    @Override
//                    public void onAnimationEnd(Animator animation) {
//                        toolbarSetElevation(0);
//                    }
//                });
//    }
//
//    public RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
//        boolean hideToolBar = false;
//        @Override
//        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//            super.onScrollStateChanged(recyclerView, newState);
//            if (hideToolBar) {
//                getActivity().getActionBar().hide();
//            } else {
//                getActivity().getActionBar().show();
//            }
//        }
//
//        @Override
//        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//            super.onScrolled(recyclerView, dx, dy);
//            if (dy > 20) {
//                hideToolBar = true;
//            } else if (dy < -5) {
//                hideToolBar = false;
//            }
//        }
//    };

//    private void addMalls() {
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//        malls.add(new MallModel(R.drawable.image1, "Preetam Jweleries", "Hafeezpet", 1.9, "Jwellery", 8.3));
//    }

    private void fadeOutAndHideImage(final LinearLayout ll) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setDuration(1000);

        fadeOut.setAnimationListener(new Animation.AnimationListener()
        {
            public void onAnimationEnd(Animation animation)
            {
                ll.setVisibility(View.GONE);
            }
            public void onAnimationRepeat(Animation animation) {}
            public void onAnimationStart(Animation animation) {}
        });

        ll.startAnimation(fadeOut);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            recyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class MallStoreTask extends AsyncTask<Void, Void, Boolean> {

        int flag;

        MallStoreTask(int flag) {
            this.flag = flag;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext(), getActivity());
            } catch (InterruptedException e) {
                return false;
            }
            if(flag == 1) {
                malls = serverDB.getStoresInMalls();
            } else if(flag == 2) {
                malls = serverDB.getMalls("madhapur");
            } else if(flag == 3) {
                malls = serverDB.getMarketsInMalls();
            }

            return true;
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mPhone)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            findDistance();
            Log.d("Size", ""+malls.size());
            if (success) {
                mallAdapter = new MallAdapter(getActivity().getBaseContext(), malls);
                recyclerView.setAdapter(mallAdapter);
            } else {
               recyclerView.setVisibility(View.GONE);
                Snackbar.make(getView(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        private void findDistance() {
            if(gpsTracker.canGetLocation()) {
                for (MallModel mall: malls) {
                    Location location = new Location(mall.getShopName());
                    location.setLatitude(mall.getLatitude());
                    location.setLongitude(mall.getLongitude());
                    mall.setDistance(gpsTracker.getDistance(location));
                }
            } else {
                gpsTracker.showSettingsAlert();
            }
        }
    }
}
