package com.example.hitman.revolucianapp.activity.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.activity.FilterActivity;
import com.example.hitman.revolucianapp.activity.adapter.FoodAdapter;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFoodFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFoodFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFoodFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;
    private ArrayList<FoodShopModel> foodShops;
    private FoodAdapter foodAdapter;
    private RecyclerView recyclerView;

    public SearchFoodFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFoodFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFoodFragment newInstance(String param1, String param2) {
        SearchFoodFragment fragment = new SearchFoodFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_food, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.search_food_recycler_view);

        foodShops = new ArrayList<>();
        addFoodShops();
        foodAdapter = new FoodAdapter(getContext(), foodShops);

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(3));
        recyclerView.setAdapter(foodAdapter);
        return v;
    }

    private void addFoodShops() {
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
    }

//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
