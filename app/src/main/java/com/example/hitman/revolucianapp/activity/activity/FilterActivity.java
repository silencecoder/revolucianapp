package com.example.hitman.revolucianapp.activity.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.model.FilterItemModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class FilterActivity extends AppCompatActivity {

    private ListView listView;
    private TextView reset, apply;
    private TextView filter1, filter2, filter3, filter4;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<FilterItemModel> arrayList;
    private ArrayList<String> categories;
    private ArrayList<String> foodCategories;
    private ArrayList<String> mallCategories;
    private ArrayList<String> entertainmentCategories;
    private ArrayList<String> offersCategories;
    private Intent intent;
    private FilterAdapter filterAdapter;
    private int filterCategory;

    private List<String> followedMallArray;
    private ArrayList<FilterItemModel> followedMallList;

    private List<String> priceMallArray;
    private ArrayList<FilterItemModel> priceMallList;

    private List<String> discountMallArray;
    private ArrayList<FilterItemModel> discountMallList;

    private List<String> categoryMallArray;
    private ArrayList<FilterItemModel> categoryMallList;

    private List<String> followedFoodArray;
    private ArrayList<FilterItemModel> followedFoodList;

    private List<String> priceFoodArray;
    private ArrayList<FilterItemModel> priceFoodList;

    private List<String> discontFoodArray;
    private ArrayList<FilterItemModel> discountFoodList;

    private List<String> categoryFoodArray;
    private ArrayList<FilterItemModel> categoryFoodList;

    private List<String> followedEntertainmentArray;
    private ArrayList<FilterItemModel> followedEntertainmentList;

    private List<String> priceEntertainmentArray;
    private ArrayList<FilterItemModel> priceEntertainmentList;

    private List<String> discountEntertainmentArray;
    private ArrayList<FilterItemModel> discountEntertainmentList;

    private List<String> categoryEntertainmentArray;
    private ArrayList<FilterItemModel> categoryEntertainmentList;

    private HashMap<String, ArrayList<String> > filterCategories;
    private HashMap<String, ArrayList<FilterItemModel> > mallCategoryDescription;
    private HashMap<String, ArrayList<FilterItemModel> > foodCategoryDescription;
    private HashMap<String, ArrayList<FilterItemModel> > entertainmentCategoryDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listView = (ListView) findViewById(R.id.search_filter_list);
        reset = (TextView) findViewById(R.id.search_filter_reset);
        apply = (TextView) findViewById(R.id.search_filter_set);
        filter1 = (TextView) findViewById(R.id.search_filter1);
        filter2 = (TextView) findViewById(R.id.search_filter2);
        filter3 = (TextView) findViewById(R.id.search_filter3);
        filter4 = (TextView) findViewById(R.id.search_filter4);
        intent = getIntent();

        addCategories();
        addDescription();
        filterCategory = categories.indexOf((String) intent.getStringExtra(R.string.search_category+""));
        Log.d("Index", ""+filterCategory);
        filter1.setBackgroundResource(R.color.white);
        filter1.setText("Followed");
        filter2.setText("Price");
        filter3.setText("Discount");
        filter4.setText("Category");

        arrayList = new ArrayList<>();
        if(filterCategory == 0) {
            arrayList = mallCategoryDescription.get("Followed");
        } else if(filterCategory == 1) {
            arrayList = foodCategoryDescription.get("Followed");
        } else if(filterCategory == 2) {
            arrayList = entertainmentCategoryDescription.get("Followed");
        } else if(filterCategory == 3) {

        } else if(filterCategory == 4) {

        }

        filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 1);
//        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_multiple_choice, arrayList);
//        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setDividerHeight(0);
        listView.setAdapter(filterAdapter);
        addActionListener();
    }

    private void addActionListener() {
        filter1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.white);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Followed");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Followed");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Followed");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 1);
                listView.setAdapter(filterAdapter);
            }
        });

        filter2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.white);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Price");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Price");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Price");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 2);
                listView.setAdapter(filterAdapter);
            }
        });

        filter3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.white);
                filter4.setBackgroundResource(R.color.dark_grey);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Discount");
                } else if(filterCategory == 1) {
                    arrayList = foodCategoryDescription.get("Discount");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Discount");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 3);
                listView.setAdapter(filterAdapter);
            }
        });

        filter4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter1.setBackgroundResource(R.color.dark_grey);
                filter2.setBackgroundResource(R.color.dark_grey);
                filter3.setBackgroundResource(R.color.dark_grey);
                filter4.setBackgroundResource(R.color.white);
//                arrayAdapter.clear();
                if(filterCategory == 0) {
                    arrayList = mallCategoryDescription.get("Category");
                } else if(filterCategory == 0) {
                    arrayList = foodCategoryDescription.get("Category");
                } else if(filterCategory == 2) {
                    arrayList = entertainmentCategoryDescription.get("Category");
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
                filterAdapter = new FilterAdapter(getApplicationContext(), arrayList, filterCategory, 4);
                listView.setAdapter(filterAdapter);
            }
        });
    }

    private void addCategories() {
        offersCategories = new ArrayList<>();

        addSearchCategories();
        addFoodCategories();
        addMallCategories();
        addEntertainmentCategories();
        addFilterCategories();
    }

    private void addSearchCategories() {
        categories = new ArrayList<>();
        categories.add("mall");
        categories.add("food");
        categories.add("entertainment");
        categories.add("offers");
        categories.add("trending");
    }

    private void addFoodCategories() {
        foodCategories = new ArrayList<>();
        foodCategories.add("Followed");
        foodCategories.add("Price");
        foodCategories.add("Discount");
        foodCategories.add("Category");
    }

    private void addMallCategories() {
        mallCategories = new ArrayList<>();
        mallCategories.add("Followed");
        mallCategories.add("Price");
        mallCategories.add("Discount");
        mallCategories.add("Category");
    }

    private void addEntertainmentCategories() {
        entertainmentCategories = new ArrayList<>();
        entertainmentCategories.add("Followed");
        entertainmentCategories.add("Price");
        entertainmentCategories.add("Discount");
        entertainmentCategories.add("Category");
    }

    private void addFilterCategories() {
        filterCategories = new HashMap<String, ArrayList<String> >();
        filterCategories.put(categories.get(0), mallCategories);
        filterCategories.put(categories.get(1), foodCategories);
        filterCategories.put(categories.get(2), entertainmentCategories);
    }

    private void addDescription() {
        mallCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();
        foodCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();
        entertainmentCategoryDescription = new HashMap<String, ArrayList<FilterItemModel>>();

        addMallDescription();
        addFoodDescription();
        addEntertainmentDescription();
        addCategoryDescription();
    }

    private void addMallDescription() {

        followedMallArray = Arrays.asList(getResources().getStringArray(R.array.followedMallArray));
        followedMallList = new ArrayList<>();
        for(String s: followedMallArray)
            followedMallList.add(new FilterItemModel(s));

        priceMallArray = Arrays.asList(getResources().getStringArray(R.array.priceMallArray));
        priceMallList = new ArrayList<>();
        for (String s: priceMallArray)
            priceMallList.add(new FilterItemModel(s));

        discountMallArray = Arrays.asList(getResources().getStringArray(R.array.discountMallArray));
        discountMallList = new ArrayList<>();
        for (String s: discountMallArray)
            discountMallList.add(new FilterItemModel(s));

        categoryMallArray = Arrays.asList(getResources().getStringArray(R.array.categoryMallArray));
        categoryMallList = new ArrayList<>();
        for (String s: categoryMallArray)
            categoryMallList.add(new FilterItemModel(s));
    }

    private void addFoodDescription() {
        followedFoodArray = Arrays.asList(getResources().getStringArray(R.array.followedFoodArray));
        followedFoodList = new ArrayList<>();
        for (String s: followedFoodArray)
            followedFoodList.add(new FilterItemModel(s));

        priceFoodArray = Arrays.asList(getResources().getStringArray(R.array.priceFoodArray));
        priceFoodList = new ArrayList<>();
        for (String s: priceFoodArray)
            priceFoodList.add(new FilterItemModel(s));

        discontFoodArray = Arrays.asList(getResources().getStringArray(R.array.discountFoodArray));
        discountFoodList = new ArrayList<>();
        for(String s: discontFoodArray)
            discountFoodList.add(new FilterItemModel(s));

        categoryFoodArray = Arrays.asList(getResources().getStringArray(R.array.categoryFoodArray));
        categoryFoodList = new ArrayList<>();
        for(String s: categoryFoodArray)
            categoryFoodList.add(new FilterItemModel(s));
    }

    private void addEntertainmentDescription() {
        followedEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.followedEntertainmentArray));
        followedEntertainmentList = new ArrayList<>();
        for (String s: followedEntertainmentArray)
            followedEntertainmentList.add(new FilterItemModel(s));

        priceEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.priceEntertainmentArray));
        priceEntertainmentList = new ArrayList<>();
            for (String s: priceEntertainmentArray)
        priceEntertainmentList.add(new FilterItemModel(s));

        discountEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.discountEntertainmentArray));
        discountEntertainmentList = new ArrayList<>();
        for (String s: discountEntertainmentArray)
            discountEntertainmentList.add(new FilterItemModel(s));

        categoryEntertainmentArray = Arrays.asList(getResources().getStringArray(R.array.categoryEntertainmentArray));
        categoryEntertainmentList = new ArrayList<>();
        for (String s: categoryEntertainmentArray)
            categoryEntertainmentList.add(new FilterItemModel(s));
    }

    private void addCategoryDescription() {
        mallCategoryDescription.put("Followed", followedMallList);
        mallCategoryDescription.put("Price", priceMallList);
        mallCategoryDescription.put("Discount", discountMallList);
        mallCategoryDescription.put("Category", categoryMallList);

        foodCategoryDescription.put("Followed", followedFoodList);
        foodCategoryDescription.put("Price", priceFoodList);
        foodCategoryDescription.put("Discount", discountFoodList);
        foodCategoryDescription.put("Discount", categoryFoodList);

        entertainmentCategoryDescription.put("Followed", followedEntertainmentList);
        entertainmentCategoryDescription.put("Price", priceEntertainmentList);
        entertainmentCategoryDescription.put("Discount", discountEntertainmentList);
        entertainmentCategoryDescription.put("Category", categoryEntertainmentList);
    }

    public class FilterAdapter extends BaseAdapter {

        Context context;
        private ArrayList<FilterItemModel> filterItemModels;
        private int filterNumber;
        private int filterCategory;

        public FilterAdapter(Context c, ArrayList<FilterItemModel> filterItemModels, int filterCategory, int filterNumber) {
            // TODO Auto-generated constructor stub
            this.context = c;
            this.filterItemModels = filterItemModels;
            this.filterNumber = filterNumber;
            this.filterCategory = filterCategory;
            Log.d("Category", filterCategory+"");
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return filterItemModels.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return filterItemModels.get(position);
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View rootView = convertView;

            if(rootView == null){
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                rootView = inflater.inflate(R.layout.search_filter_list_item, parent, false);
            }
            final ImageView img = (ImageView) rootView.findViewById(R.id.search_filter_check);
            TextView title = (TextView) rootView.findViewById(R.id.search_filter_item);

            final FilterItemModel row = filterItemModels.get(position);
            Log.d("Error", "Message: "+position+" title = "+row.getTitle());
            title.setText(row.getTitle());
            final int pos = position;

            if(row.isFlag())
                img.setImageResource(R.drawable.btn_check_on_holo_light);
            else
                img.setImageResource(R.drawable.btn_check_off_holo_light);

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(row.isFlag()) {
                        img.setImageResource(R.drawable.btn_check_off_holo_light);
                        row.setFlag(false);
                    } else {
                        img.setImageResource(R.drawable.btn_check_on_holo_light);
                        row.setFlag(true);
                    }
                    changeData(pos, filterCategory, filterNumber, row.isFlag());
                }
            });
            return rootView;
        }

        private void changeData(int position, int filterCategory, int filterNumber, boolean flag) {
            if(filterNumber == 1) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedFoodList.get(position).setFlag(flag);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Followed").get(position).setFlag(flag);
                    followedEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber == 2) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceFoodList.get(position).setFlag(false);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Price").get(position).setFlag(flag);
                    priceEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber== 3) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {
                    foodCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountFoodList.get(position).setFlag(flag);
                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Discount").get(position).setFlag(flag);
                    discountEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            } else if(filterNumber == 4) {
                if(filterCategory == 0) {
                    mallCategoryDescription.get("Category").get(position).setFlag(flag);
                    categoryMallList.get(position).setFlag(flag);
                } else if(filterCategory == 1) {

                } else if(filterCategory == 2) {
                    entertainmentCategoryDescription.get("Category").get(position).setFlag(flag);
                    categoryEntertainmentList.get(position).setFlag(flag);
                } else if(filterCategory == 3) {

                } else if(filterCategory == 4) {

                }
            }
        }
    }
}
