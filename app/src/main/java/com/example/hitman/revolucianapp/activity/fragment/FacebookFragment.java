package com.example.hitman.revolucianapp.activity.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.activity.NavigationDrawerActivity;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.UserModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FacebookFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FacebookFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FacebookFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;

    private CallbackManager mCallbackManager;
    private Profile profile;
    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;
    private ServerDB serverDB;
    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;
    private UserExist userExist;
    private UserLoginTask mAuthTask;
    private View mProgressView;
    private ProgressDialog progressDialog;
    private boolean permission_success;
    private UserModel permissionUserModel;

    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    public FacebookFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FacebookFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FacebookFragment newInstance(String param1, String param2) {
        FacebookFragment fragment = new FacebookFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }

        mCallbackManager = CallbackManager.Factory.create();
        tokenTracker=new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker=new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                saveProfile(profile1);
            }
        };



        tokenTracker.startTracking();
        profileTracker.startTracking();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LoginButton loginButton = (LoginButton) view.findViewById(R.id.login_button);

        int location[] = new int[2];
        loginButton.getLocationOnScreen(location);
        //Initialize the Point with x, and y positions
//        p = new Point();
//        p.x = location[0];
//        p.y = location[1];
//
//        pref = getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
//        edit = pref.edit();

        loginButton.setCompoundDrawables(null, null, null, null);
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        loginButton.setFragment(this);
        loginButton.registerCallback(mCallbackManager, mFacebookCallback);
    }

    private FacebookCallback<LoginResult> mFacebookCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            saveProfile(Profile.getCurrentProfile());
            databaseAdapter= new DatabaseAdapter(getContext());
//            userExist = new UserExist(Profile.getCurrentProfile().getName(), "123456789", "gargh96@gmail.com", "qwerty123");
//            startDialog();
//
//            userExist.execute((Void) null);
//            Log.d("Hello", "Hello1");
//            GraphRequest request = GraphRequest.newMeRequest(
//                    loginResult.getAccessToken(),
//                    new GraphRequest.GraphJSONObjectCallback() {
//                        @Override
//                        public void onCompleted(JSONObject object, GraphResponse response) {
//                            Log.v("LoginActivity", response.toString());
//
//                            // Application code
//                            try {
//                                 String email = object.getString("email");
////                                String birthday = object.getString("birthday"); // 01/31/1980 format
//
//                                userExist = new UserExist(Profile.getCurrentProfile().getName(), "123456789", email, "qwerty123");
//                                startDialog();
//                                userExist.execute((Void) null);
//                                Log.d("Hello", "Hello1");
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    });

//            userExist = new UserExist(Profile.getCurrentProfile().getName(), "123456789", "gargh96@gmail.com", "qwerty123");
//            startDialog();
//            userExist.execute((Void) null);
//            Log.d("Hello", "Hello1");
//            Bundle parameters = new Bundle();
//            parameters.putString("fields", "id,name,email,gender,birthday");
//            request.setParameters(parameters);
//            request.executeAsync();

//            userExist = new UserExist(profile.getName(), "123456789", )
//            if(getActivity().getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE).getString(UserPhone, "").length() > 0) {
//                Log.d("Login", "Login 1");
//                new Login().execute();
//            } else {
//                Log.d("Register", "Register 1");
//                showPopup(getActivity());
//                new Register().execute();
//            }
        }

        @Override
        public void onCancel() {
            Toast.makeText(getActivity().getApplicationContext(),"Cancel", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onError(FacebookException e) {
            Toast.makeText(getActivity().getApplicationContext(),"Error", Toast.LENGTH_LONG).show();
        }
    };

    private void startDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Logging In. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        profile = Profile.getCurrentProfile();
        saveProfile(profile);
    }

    @Override
    public void onStop() {
        super.onStop();
        profileTracker.stopTracking();
        tokenTracker.stopTracking();
    }

    public void saveProfile(Profile profile1) {
        profile = profile1;

        if(profile != null) {
            userExist = new UserExist(Profile.getCurrentProfile().getName(), "123456789", "gargh96@gmail.com", "qwerty123");

            userExist.execute((Void) null);
            Log.d("Hello", "Hello1");
            Toast.makeText(getActivity().getApplicationContext(), "User Logged in: "+profile1.getName(), Toast.LENGTH_LONG).show();
//            edit.putString(UserName, profile1.getName());
//            edit.putBoolean(UserLogin, true);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_facebook, container, false);
        mProgressView = v.findViewById(R.id.login_facebook_progress);
        databaseAdapter = new DatabaseAdapter(getContext());
        prefManager = new PrefManager(getContext());
        mCallbackManager = CallbackManager.Factory.create();
        tokenTracker=new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken accessToken, AccessToken accessToken1) {

            }
        };
        profileTracker=new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile profile, Profile profile1) {
                saveProfile(profile1);
            }
        };

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    if(permission_success) {
                        databaseAdapter.loginUser(permissionUserModel.getEmail());
                        prefManager.setUserLoggedIn(true);
                        prefManager.setUserSigninType(getString(R.string.signin_google));
                        startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
                    } else {
                        mAuthTask = new UserLoginTask(permissionUserModel.getUserName(), permissionUserModel.getPhone(),
                                permissionUserModel.getPhone(), permissionUserModel.getPassword());
                        mAuthTask.execute((Void) null);
                    }
                } else {
                    // Permission Denied
                    Toast.makeText(getActivity().getApplicationContext(), "GET_ACCOUNTS Denied", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private class UserExist extends AsyncTask<Void, Void, Boolean> {

        private String mName;
        private String mPhone;
        private String mEmail;
        private String mPassword;

        UserExist(String userName, String userPhone, String userEmail, String userPassword) {
            mName =  userName;
            mPhone = userPhone;
            mEmail = userEmail;
            mPassword = userPassword;
        }

        @Override
        protected void onPreExecute() {
            mProgressView.setVisibility(View.VISIBLE);
            Log.d("UserExist", "Yes");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(1500);
                serverDB =  ServerDB.getInstance(getContext());
                return serverDB.isUserExist(mEmail);
            } catch (InterruptedException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {

            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permission_success = success;
                permissionUserModel = new UserModel(mName, mEmail, mPhone, mPassword);
                int hasWriteContactsPermission = ((Activity) getContext()).checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
                if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[] {Manifest.permission.GET_ACCOUNTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_COARSE_LOCATION},
                            REQUEST_CODE_ASK_PERMISSIONS);
                    return;
                }
            } else {
                if(success) {
                    databaseAdapter.loginUser(mEmail);
                    prefManager.setUserLoggedIn(true);
                    prefManager.setUserSigninType(getString(R.string.signin_google));
                    startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
                } else {
                    mAuthTask = new UserLoginTask(mName, mPhone, mEmail, mPassword);
                    mAuthTask.execute((Void) null);
                }
            }
        }
    }

    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private UserModel userModel;
        private int id;

        UserLoginTask(String userName, String userPhone, String userEmail, String userPassword) {
            userModel = new UserModel(userName, userEmail, userPhone, userPassword);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext());
            } catch (InterruptedException e) {
                return false;
            }
            serverDB.signUpUser(userModel);
            id = serverDB.getUserID(userModel.getEmail());
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            mProgressView.setVisibility(View.GONE);
            progressDialog.dismiss();
            if (success) {
                getActivity().finish();
                databaseAdapter.signupUser(userModel);
                prefManager.setUserLoggedIn(true);
                prefManager.setUserEmail(userModel.getEmail());
                prefManager.setUserSigninType(getString(R.string.signin_facebook));
                prefManager.setUserId(id);
                startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
            } else {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPhoneView.setError(getString(R.string.error_invalid_phone));
//                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            //showProgress(false);
        }
    }
}
