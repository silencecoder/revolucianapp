package com.example.hitman.revolucianapp.activity.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.RestaurantActivity;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.adapter.FollowShopAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.ItemModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WishListItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WishListItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WishListItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    public WishListItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WishListItemFragment.
     */

    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private View mProgressView;

    private ArrayList<ItemModel> itemModels;
    private ItemAdapter itemAdapter;
    private ServerDB serverDB;
    private PrefManager prefManager;


    // TODO: Rename and change types and number of parameters
    public static WishListItemFragment newInstance(String param1, String param2) {
        WishListItemFragment fragment = new WishListItemFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_wish_list_item, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.wishlist_item_recyclerview);
        mProgressView = v.findViewById(R.id.wishlist_progress);

        itemModels = new ArrayList<>();
        itemAdapter = new ItemAdapter(getContext(), itemModels);
        prefManager = new PrefManager(getContext());

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(8));
        recyclerView.setAdapter(itemAdapter);

        new WishlistItem().execute((Void) null);

        return v;

    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            recyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class ItemAdapter extends InterceptorView.Adapter<ItemAdapter.MyViewHolder> {

        private List<ItemModel> items;
        public Context mContext;
        private ServerDB serverDB;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView shopPageInventoryItemImage;
            public TextView shopPageInventoryItemName;
            public TextView shopPageInventoryItemBrandName;
            public TextView shopPageInventoryItemPrice;
            public ImageView shopPageInventoryItemWishlist;

            public MyViewHolder(View view) {
                super(view);
                shopPageInventoryItemImage = (ImageView) view.findViewById(R.id.wishlist_item_image);
                shopPageInventoryItemName = (TextView) view.findViewById(R.id.wishlist_item_name);
                shopPageInventoryItemBrandName = (TextView) view.findViewById(R.id.wishlist_item_brand_name);
                shopPageInventoryItemPrice = (TextView) view.findViewById(R.id.wishlist_item_price);
                shopPageInventoryItemWishlist = (ImageView) view.findViewById(R.id.wishlist_item_wishlist);
            }
        }

        public ItemAdapter(Context context, List<ItemModel> items) {
            mContext = context;
            this.items = items;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.wishlist_item_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            final ItemModel item = items.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);

            holder.shopPageInventoryItemImage.setBackgroundResource(item.getImage());
            holder.shopPageInventoryItemName.setText(item.getName());
            holder.shopPageInventoryItemBrandName.setText(item.getCategory());
            holder.shopPageInventoryItemPrice.setText("Rs"+item.getPrice());
            holder.shopPageInventoryItemWishlist.setImageResource(R.drawable.ic_action_minus);

            holder.shopPageInventoryItemWishlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    addShopItemWishList(item.getId());
                    new RemoveWishlistItem(item.getId()).execute((Void) null);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

            private GestureDetector gestureDetector;
            private ClickListener clickListener;

            public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
                this.clickListener = clickListener;
                gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                        return true;
                    }

                    @Override
                    public void onLongPress(MotionEvent e) {
                        View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                        if (child != null && clickListener != null) {
                            clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                        }
                    }
                });
            }

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                    clickListener.onClick(child, rv.getChildPosition(child));
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        }
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    private class WishlistItem extends AsyncTask<Void, Void, Boolean> {

        int userID;

        @Override
        protected void onPreExecute() {
//            progressDialog = new ProgressDialog(getActivity().getApplicationContext());
//            progressDialog.setTitle("Loading...");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext(), getActivity());
            } catch (InterruptedException e) {
                return false;
            }
            userID = prefManager.getUserId();
            if(userID <= 0) {
                userID = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userID);
            }
            itemModels = serverDB.getWishlistItems(userID);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
//            progressDialog.hide();
            showProgress(false);
            Log.d("Size", itemModels.size()+"");
            if (success) {
                itemAdapter =  new ItemAdapter(getContext(), itemModels);
                recyclerView.setAdapter(itemAdapter);
            } else {
                itemAdapter =  new ItemAdapter(getContext(), itemModels);
                recyclerView.setAdapter(itemAdapter);
            }
        }
    }

    private class RemoveWishlistItem extends AsyncTask<Void, Void, Boolean> {

        int userID;
        int itemID;
        int position;

        public RemoveWishlistItem(int itemID) {
            this.itemID = itemID;
        }

        @Override
        protected void onPreExecute() {
//            progressDialog = new ProgressDialog(getActivity().getApplicationContext());
//            progressDialog.setTitle("Loading...");
//            progressDialog.setCancelable(false);
//            progressDialog.show();
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext(), getActivity());
            } catch (InterruptedException e) {
                return false;
            }
            userID = prefManager.getUserId();
            if(userID <= 0) {
                userID = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userID);
            }
            serverDB.removeItemFromWishlist(userID, itemID);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
//            progressDialog.hide();
            showProgress(false);
            Log.d("Size", itemModels.size()+"");
            if (success) {
                position = getItemPosition(itemID);
                itemModels.remove(position);
                itemAdapter =  new ItemAdapter(getContext(), itemModels);
                recyclerView.setAdapter(itemAdapter);
            } else {
                itemAdapter =  new ItemAdapter(getContext(), itemModels);
                recyclerView.setAdapter(itemAdapter);
            }
        }
    }

    private int getItemPosition(int itemID) {
        int id = 0;
        for (int i = 0; i < itemModels.size(); i++)
            if(itemModels.get(i).getId() == itemID)
                return i;
        return id;
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
