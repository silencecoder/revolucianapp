package com.example.hitman.revolucianapp.activity.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager;
import com.example.hitman.revolucianapp.activity.adapter.ReviewAdapter;
import com.example.hitman.revolucianapp.activity.adapter.ShopPageDealsAdapter;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.model.ReviewModel;
import com.example.hitman.revolucianapp.activity.model.ShopPageDealsModel;
import com.example.hitman.revolucianapp.activity.model.ShopPageInventoryItemModel;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;
import java.util.List;

public class ShopPageActivity extends AppCompatActivity implements View.OnClickListener{

    private final int REQUEST_CODE_ASK_PERMISSIONS = 0;
    private int shopID;
    private int reviewRating = 0;
    private String reviewContent;
    private String shopPhoneNumber;

    private Toolbar toolbar;
    private TextView writeReview;
    private TextView seeAllAreview;
    private TextView saveShop;
    private TextView followShop;
    private RecyclerView dealsRecyclerView;
    private RecyclerView inventoryRecyclerView;
    private RecyclerView reviewsRecyclerView;
    private View mProgressView;
    private ImageView shopPageCall;
    private ImageView star1, star2, star3, star4, star5;
    private CardView shopPageShop;
    private LinearLayout shopPage;
    private TextView shopPageName, shopPageTimings, shopPageAddress;
    private EditText editReview;
    private ListView listView;
    private ImageView followShopYes;
    private ImageView saveShopYes;
    private ProgressDialog pDialog;

    private boolean follow;
    private ArrayAdapter<String> arrayAdapter;
    private ShopPageDealsAdapter shopPageDealsAdapter;
    private ShopPageInventoryAdapter shopPageInventoryAdapter;
    private ReviewAdapter reviewAdapter;
    private ArrayList<ShopPageDealsModel> deals;
    private ArrayList<ShopPageInventoryItemModel> inventorys;
    private ArrayList<ReviewModel> reviewModels;
    private ArrayList<UserProfileDeckModel> userProfileDeckModels;
    private ServerDB serverDB;
    private ShopTask shopTask;
    private ItemTask itemTask;
    private ReviewTask reviewTask;
    private OfferTask offerTask;
    private Intent callIntent;
    private AlertDialog alert;
    private Intent intent;
    private ProgressDialog progressDialog;
    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;
    private WriteReviewTask writeReviewTask;
    private FollowShopTask followShopTask;
    private UserFollowTask userFollowTask;

    private static final String[] CALL_PERMS={
            Manifest.permission.CALL_PHONE
    };
    private static final int INITIAL_REQUEST=1337;

    @TargetApi(23)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_page);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        shopID = getIntent().getIntExtra("shopID", 0);

        writeReview = (TextView) findViewById(R.id.shop_page_write_review);
        seeAllAreview = (TextView) findViewById(R.id.shop_page_all_reviews);
        dealsRecyclerView = (RecyclerView) findViewById(R.id.shop_page_deals_recycler_view);
        inventoryRecyclerView = (RecyclerView) findViewById(R.id.shop_page_inventory_recycler_view);
        reviewsRecyclerView = (RecyclerView) findViewById(R.id.shop_page_reviews_recycler_view);
        mProgressView = findViewById(R.id.shop_page_progress);
        shopPageShop = (CardView) findViewById(R.id.shop_page_shop);
        shopPage = (LinearLayout) findViewById(R.id.shop_page);
        shopPageName = (TextView) findViewById(R.id.shop_page_name);
        shopPageTimings = (TextView) findViewById(R.id.shop_page_timings);
        shopPageAddress = (TextView) findViewById(R.id.shop_page_address);
        shopPageCall = (ImageView) findViewById(R.id.shop_page_call);
        saveShop = (TextView) findViewById(R.id.shop_page_save);
        followShop = (TextView) findViewById(R.id.shop_page_follow);
        followShopYes = (ImageView) findViewById(R.id.shop_page_follow_yes);
        saveShopYes = (ImageView) findViewById(R.id.shop_page_save_yes);
        callIntent = new Intent(Intent.ACTION_CALL);

        userProfileDeckModels = new ArrayList<>();
        deals = new ArrayList<>();
        inventorys = new ArrayList<>();
        reviewModels = new ArrayList<>();
        serverDB = ServerDB.getInstance(getApplicationContext());
        shopPageDealsAdapter = new ShopPageDealsAdapter(getApplicationContext(), deals);
        prefManager = new PrefManager(getApplicationContext());
        shopPageInventoryAdapter = new ShopPageInventoryAdapter(getBaseContext(), inventorys);
        reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);
        databaseAdapter = new DatabaseAdapter(getApplicationContext());
        addDeals();
        addInventory();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        dealsRecyclerView.setLayoutManager(mLayoutManager);
        dealsRecyclerView.setHasFixedSize(false);
        dealsRecyclerView.setNestedScrollingEnabled(false);
        dealsRecyclerView.addItemDecoration(new DividerItemDecoration(10));
        dealsRecyclerView.setAdapter(shopPageDealsAdapter);

        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getApplicationContext());
        inventoryRecyclerView.setLayoutManager(mLayoutManager1);
        inventoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        inventoryRecyclerView.setHasFixedSize(true);
        inventoryRecyclerView.setNestedScrollingEnabled(false);
        inventoryRecyclerView.addItemDecoration(new DividerItemDecoration(6));
        inventoryRecyclerView.setAdapter(shopPageInventoryAdapter);

        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        reviewsRecyclerView.setLayoutManager(mLayoutManager2);
        reviewsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reviewsRecyclerView.setHasFixedSize(true);
        reviewsRecyclerView.setNestedScrollingEnabled(false);
        reviewsRecyclerView.addItemDecoration(new DividerItemDecoration(6));
        reviewsRecyclerView.setAdapter(reviewAdapter);

        writeReview.setOnClickListener(this);
        seeAllAreview.setOnClickListener(this);
        shopPageCall.setOnClickListener(this);
        saveShop.setOnClickListener(this);
        followShop.setOnClickListener(this);

        shopTask = new ShopTask(shopID);
        userFollowTask = new UserFollowTask();
        offerTask = new OfferTask(shopID);
        itemTask = new ItemTask(shopID);
        reviewTask = new ReviewTask(shopID);

        showProgress(true);
        userFollowTask.execute((Void) null);
        shopTask.execute( (Void) null);
        offerTask.execute((Void) null);
        reviewTask.execute((Void) null);
        itemTask.execute((Void) null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shop_page_write_review:
                writeReview();
                break;
            case R.id.shop_page_all_reviews:
                intent = new Intent(ShopPageActivity.this, ReviewActivity.class);
                intent.putExtra("category", "shop");
                intent.putExtra("shopID", shopID);
                startActivity(intent);
                break;
            case R.id.shop_page_call:
                Log.d("Request", "call");
                if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.CALL_PHONE);
                    if (hasWriteContactsPermission == PackageManager.PERMISSION_DENIED) {
                        requestPermissions(new String[] {Manifest.permission.CALL_PHONE}, REQUEST_CODE_ASK_PERMISSIONS);
                    } else
                        startActivity(callIntent);
                } else
                    startActivity(callIntent);
                break;
            case R.id.shop_page_save:
                saveShopYes.setVisibility(View.VISIBLE);
                saveShopInDeck();
                break;
            case R.id.shop_page_follow:
                followShopTask = new FollowShopTask(prefManager.getUserId(), shopID);
                followShopTask.execute((Void) null);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    startActivity(callIntent);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            shopPageShop.setVisibility(show? View.GONE:View.VISIBLE);
            dealsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            inventoryRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            shopPage.setVisibility(show ? View.GONE : View.VISIBLE);
            shopPage.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                   shopPage.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            shopPageShop.setVisibility(show? View.GONE:View.VISIBLE);
            dealsRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            inventoryRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            shopPage.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public void addShopItemWishList(int itemId, boolean flag) {
        new WishList(itemId, flag).execute((Void) null);
    }

    // Save shop details in deck when user clicks the save button
    private void saveShopInDeck() {
        userProfileDeckModels = databaseAdapter.getAllDecks();
        String[] items = new String[userProfileDeckModels.size()];
        for(int i = 0; i < userProfileDeckModels.size(); i++)
            items[i] = userProfileDeckModels.get(i).getTitle();
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShopPageActivity.this);
        LayoutInflater inflater = getLayoutInflater();
//        View v = inflater.inflate(R.layout.custom_dialog_list, null);
//        listView = (ListView) v.findViewById(R.id.list_view);
//        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
//        listView.setAdapter(arrayAdapter);
        builder.setTitle("Select a deck");
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                saveCardInDeck(which);
                alert.dismiss();
                intent = new Intent(ShopPageActivity.this, CardActivity.class);
                intent.putExtra("deckID", userProfileDeckModels.get(which).getId());
                startActivity(intent);
            }
        });
        alert = builder.create();
        alert.show();

//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                saveCardInDeck(position);
//                alert.dismiss();
//                intent = new Intent(ShopPageActivity.this, CardActivity.class);
//                intent.putExtra("deckID", userProfileDeckModels.get(position).getId());
//                startActivity(intent);
//            }
//        });
    }

    private void saveCardInDeck(int position) {
        long id = databaseAdapter.insertCardInDeck(userProfileDeckModels.get(position).getId());
        MallModel mallModel = new MallModel(shopID, shopPageName.getText().toString(), shopPageAddress.getText().toString(),
                shopPageTimings.getText().toString(), shopPhoneNumber);
        long a = databaseAdapter.insertMallCard(mallModel, (int) id);
        long b = databaseAdapter.setCardCategory("mall", (int) id);
        Log.d("Insert ID", a+" "+b);
    }

    private void addDeals() {
//        deals.add(new ShopPageDealsModel("30% cashback on bill of Rs 1000 above"));
//        deals.add(new ShopPageDealsModel("Buy 2 get 1 free"));
//        deals.add(new ShopPageDealsModel("50% cashback on bill of Rs 5000 above"));
    }

    private void addInventory() {
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));
//        inventorys.add(new ShopPageInventoryItemModel(R.drawable.shop, R.drawable.shop, "Pan", "Pan","Philips",
//                "Philips", 500, 500));

    }

    private void writeReview() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShopPageActivity.this);
        // Get the layout inflater
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_dialog_review, null);
        star1 =  (ImageView) v.findViewById(R.id.review_star1);
        star2 =  (ImageView) v.findViewById(R.id.review_star2);
        star3 =  (ImageView) v.findViewById(R.id.review_star3);
        star4 =  (ImageView) v.findViewById(R.id.review_star4);
        star5 =  (ImageView) v.findViewById(R.id.review_star5);
        editReview = (EditText) v.findViewById(R.id.edit_text_review);
        addOnClick();

        builder.setTitle("Review for Shop"+shopPageName.getText().toString())
                .setView(v)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ..
                        writeReviewTask = new WriteReviewTask();
                        writeReviewTask.execute((Void) null);
                        Toast.makeText(getApplicationContext(), "Yes working", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        alert.dismiss();
                    }
                });
        alert = builder.create();
        alert.show();
    }

    private void addOnClick() {
        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 1;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10_light);
                star3.setImageResource(R.drawable.ic_action_star_10_light);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 2;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10_light);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 3;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10_light);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 4;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10);
                star5.setImageResource(R.drawable.ic_action_star_10_light);
            }
        });

        star1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating = 5;
                star1.setImageResource(R.drawable.ic_action_star_10);
                star2.setImageResource(R.drawable.ic_action_star_10);
                star3.setImageResource(R.drawable.ic_action_star_10);
                star4.setImageResource(R.drawable.ic_action_star_10);
                star5.setImageResource(R.drawable.ic_action_star_10);
            }
        });
    }

    private void startDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging In. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private class ShopTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;
        ArrayList<Object> shopDetails;

        ShopTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            shopDetails = serverDB.getStoreDetails(shopID);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            shopTask = null;
            if(success) {
                Log.d("Shop Size", shopDetails.size()+"");
                shopPageShop.setVisibility(View.VISIBLE);
                shopPageName.setText((String) shopDetails.get(2));
                shopPageTimings.setText((String) shopDetails.get(0));
                shopPageAddress.setText((String) shopDetails.get(1));
                shopPhoneNumber = (String) shopDetails.get(3);
                callIntent.setData(Uri.parse("tel:" + shopPhoneNumber));
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        }
    }

    private class OfferTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;

        OfferTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            deals = serverDB.getShopOffers(shopID);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            offerTask = null;
            if(success) {
                Log.d("offers", deals.size()+"");
                shopPageDealsAdapter = new ShopPageDealsAdapter(getApplicationContext(), deals);
                dealsRecyclerView.setAdapter(shopPageDealsAdapter);
//                shopPageInventoryAdapter.notifyDataSetChanged();
                dealsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class ReviewTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;

        ReviewTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            reviewModels = serverDB.getShopReviews(shopID, true);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            Log.d("Reviews Size: ", reviewModels.size()+"");
            if(success) {
                reviewAdapter = new ReviewAdapter(getApplicationContext(), reviewModels);
                reviewsRecyclerView.setAdapter(reviewAdapter);
                reviewsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class ItemTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;
        int userID;

        ItemTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            userID = prefManager.getUserId();
            if(userID <= 0)
                userID = serverDB.getUserID(prefManager.getUserEmail());
            inventorys = serverDB.getShopItems(userID, shopID);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            itemTask = null;
            showProgress(false);
            if(success) {
                Log.d("Items size: ", inventorys.size()+"");
                shopPageInventoryAdapter = new ShopPageInventoryAdapter(getBaseContext(), inventorys);
                inventoryRecyclerView.setAdapter(shopPageInventoryAdapter);
//                shopPageInventoryAdapter.notifyDataSetChanged();
                inventoryRecyclerView.setVisibility(View.VISIBLE);
                shopPage.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class WriteReviewTask extends AsyncTask<Void, Void, Boolean> {

        String username;
        String review;

        @Override
        protected void onPreExecute() {
            startDialog();
            username = databaseAdapter.getUserName(prefManager.getUserEmail());
            review = editReview.getText().toString();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            serverDB.writeShopReview(shopID, username, review, reviewRating);
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            progressDialog.dismiss();
            if(success) {

            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    private class FollowShopTask extends AsyncTask<Void, Void, Boolean> {

        int userID;
        int shopID;

        FollowShopTask(int userID, int shopID) {
            this.userID = userID;
            this.shopID = shopID;
        }

        @Override
        protected void onPreExecute() {
            startDialog();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            Log.d("Use Shop Id", userID + " " + shopID);
            if(userID <= 0) {
                userID = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userID);
            }
            Log.d("Use Shop Id", userID + " Hello " + shopID);
            if(!follow) {
                serverDB.setUserFollowShop(userID, shopID);
                follow = true;
            }else {
                serverDB.setUserUnFollowShop(userID, shopID);
                follow = false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            progressDialog.dismiss();
            if(success) {
                if(follow) {
                    followShopYes.setVisibility(View.VISIBLE);
                    followShop.setText("UNFOLLOW");
                } else {
                    followShopYes.setVisibility(View.GONE);
                    followShop.setText("FOLLOW");
                }
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }

    // Check user follow the shop or not
    private class UserFollowTask extends AsyncTask<Void, Void, Boolean> {

        int userId;

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            userId = prefManager.getUserId();
            if(userId <= 0) {
                userId = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userId);
            }
            follow =  serverDB.isUserFollow(prefManager.getUserId(), shopID);
            return follow;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            if(success) {
                Log.d("User Followed", "Yes");
                followShopYes.setVisibility(View.VISIBLE);
                followShop.setText("UNFOLLOW");
            } else {
                Log.d("User Followed", "No");
                followShopYes.setVisibility(View.INVISIBLE);
                followShop.setText("FOLLOW");
            }
        }
    }

    private class WishList extends AsyncTask<Void, Void, Boolean> {

        int itemID;
        int userID;
        boolean flag;

        WishList(int itemID, boolean flag) {
            this.itemID = itemID;
            this.flag = flag;
        }

        @Override
        protected void onPreExecute() {
//            pDialog = new ProgressDialog(ShopPageActivity.this);
//            pDialog.setMessage("Loading...");
//            pDialog.setCancelable(false);
//            showProgressDialog();
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            userID = prefManager.getUserId();
            if(userID <= 0) {
                userID = serverDB.getUserID(prefManager.getUserEmail());
                prefManager.setUserId(userID);
            }
            Log.d("User Id", userID+"");
            Log.d("ShopID", shopID+"");
            Log.d("Item ID", itemID+"");
            if(flag)
                serverDB.addItemInWishList(userID, shopID, itemID);
            else
                serverDB.removeItemFromWishlist(userID, shopID, itemID);
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
//            hideProgressDialog();
            super.onPostExecute(aBoolean);
        }
    }

    public class ShopPageInventoryAdapter extends InterceptorView.Adapter<ShopPageInventoryAdapter.MyViewHolder> {

        private List<ShopPageInventoryItemModel> items;
        public Context mContext;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public ImageView shopPageInventoryItemImage1, shopPageInventoryItemImage2;
            public TextView shopPageInventoryItemName1, shopPageInventoryItemName2;
            public TextView shopPageInventoryItemBrandName1, shopPageInventoryItemBrandName2;
            public TextView shopPageInventoryItemPrice1, shopPageInventoryItemPrice2;
            public ImageView shopPageInventoryItemWishlist1, shopPageInventoryItemWishlist2;

            public MyViewHolder(View view) {
                super(view);
                shopPageInventoryItemImage1 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_image1);
                shopPageInventoryItemName1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_name1);
                shopPageInventoryItemBrandName1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_brand_name1);
                shopPageInventoryItemPrice1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_price1);
                shopPageInventoryItemWishlist1 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_wishlist1);

                shopPageInventoryItemImage2 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_image2);
                shopPageInventoryItemName2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_name2);
                shopPageInventoryItemBrandName2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_brand_name2);
                shopPageInventoryItemPrice2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_price2);
                shopPageInventoryItemWishlist2 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_wishlist2);
            }
        }

        public ShopPageInventoryAdapter(Context context, List<ShopPageInventoryItemModel> items) {
            mContext = context;
            this.items = items;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shop_page_inventory_list_item, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {
            final ShopPageInventoryItemModel item = items.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
            holder.shopPageInventoryItemImage1.setBackgroundResource(item.getImage1());
            holder.shopPageInventoryItemName1.setText(item.getItemName1());
            holder.shopPageInventoryItemBrandName1.setText(item.getBrandName1());
            holder.shopPageInventoryItemPrice1.setText("Rs"+item.getPrice1());

            holder.shopPageInventoryItemImage2.setBackgroundResource(item.getImage2());
            holder.shopPageInventoryItemName2.setText(item.getItemName2());
            holder.shopPageInventoryItemBrandName2.setText(item.getBrandName2());
            holder.shopPageInventoryItemPrice2.setText("Rs"+item.getPrice2());

            if(item.isWishlist1())
                holder.shopPageInventoryItemWishlist1.setImageResource(R.drawable.ic_action_add);
            else
                holder.shopPageInventoryItemWishlist1.setImageResource(R.drawable.ic_action_minus);
            if(item.isWishlist2())
                holder.shopPageInventoryItemWishlist2.setImageResource(R.drawable.ic_action_add);
            else
                holder.shopPageInventoryItemWishlist2.setImageResource(R.drawable.ic_action_minus);

            holder.shopPageInventoryItemWishlist1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addShopItemWishList(item.getId1(), !item.isWishlist1());
                    if(item.isWishlist1())
                        holder.shopPageInventoryItemWishlist1.setImageResource(R.drawable.ic_action_minus);
                    else
                        holder.shopPageInventoryItemWishlist1.setImageResource(R.drawable.ic_action_add);
                }
            });

            holder.shopPageInventoryItemWishlist2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addShopItemWishList(item.getId2(), !item.isWishlist2());
                    if(item.isWishlist2())
                        holder.shopPageInventoryItemWishlist2.setImageResource(R.drawable.ic_action_minus);
                    else
                        holder.shopPageInventoryItemWishlist2.setImageResource(R.drawable.ic_action_add);
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

            private GestureDetector gestureDetector;
            private ClickListener clickListener;

            public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
                this.clickListener = clickListener;
                gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                        return true;
                    }

                    @Override
                    public void onLongPress(MotionEvent e) {
                        View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                        if (child != null && clickListener != null) {
                            clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                        }
                    }
                });
            }

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                    clickListener.onClick(child, rv.getChildPosition(child));
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        }
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }
}
