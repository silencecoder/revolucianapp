package com.example.hitman.revolucianapp.activity.adapter;

/**
 * Created by hitman on 24/5/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.OfferModel;

import java.util.List;


public class OfferAdapter extends InterceptorView.Adapter<OfferAdapter.MyViewHolder> {

    private List<OfferModel> offers;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView offerImage;
        public TextView offerShopName, offerShopDesc, offerShopDistance, offersShopLocation;
        public FloatingActionButton offerShopFAB;

        public MyViewHolder(View view) {
            super(view);
            offerImage = (ImageView) view.findViewById(R.id.offers_list_item_image);
            offerShopName = (TextView) view.findViewById(R.id.offer_list_item_name);
            offerShopDesc = (TextView) view.findViewById(R.id.offer_list_item_description);
            offerShopDistance = (TextView) view.findViewById(R.id.offer_list_item_distance);
            offersShopLocation = (TextView) view.findViewById(R.id.offer_list_item_location);
            //offerShopFAB = (FloatingActionButton) view.findViewById(R.id.offers_list_item_fab);
        }
    }

    public OfferAdapter(Context context, List<OfferModel> offers) {
        mContext = context;
        this.offers = offers;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.offers_page_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        OfferModel offer = offers.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.offerImage.setBackgroundResource(offer.getImage());
        holder.offerShopName.setText(offer.getOfferShopName());
        holder.offerShopDesc.setText(offer.getOfferDescription());
        holder.offerShopDistance.setText(offer.getOfferShopDistance()+"km");
        holder.offersShopLocation.setText(offer.getOfferShopLocation());
        //holder.offerShopFAB.setBackgroundResource(offer.getFabImage());
    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private OfferAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final OfferAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
