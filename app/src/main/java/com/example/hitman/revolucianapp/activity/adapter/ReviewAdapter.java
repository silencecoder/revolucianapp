package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.ReviewModel;

import java.util.List;

/**
 * Created by hitman on 24/6/16.
 */
public class ReviewAdapter extends InterceptorView.Adapter<ReviewAdapter.MyViewHolder> {

    private List<ReviewModel> reviews;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView reviewUser, reviewDesc, reviewRating;

        public MyViewHolder(View view) {
            super(view);
            reviewUser = (TextView) view.findViewById(R.id.review_user_name);
            reviewDesc = (TextView) view.findViewById(R.id.review_user_desc);
            reviewRating = (TextView) view.findViewById(R.id.review_user_rating);
        }
    }

    public ReviewAdapter(Context context, List<ReviewModel> reviews) {
        mContext = context;
        this.reviews = reviews;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ReviewModel review = reviews.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.reviewUser.setText(review.getUser());
        holder.reviewDesc.setText(review.getReview());
        holder.reviewRating.setText(review.getRating()+"");
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ReviewAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ReviewAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}

