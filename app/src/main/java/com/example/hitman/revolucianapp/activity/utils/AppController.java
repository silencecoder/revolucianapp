package com.example.hitman.revolucianapp.activity.utils;

import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.activity.NavigationDrawerActivity;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.facebook.FacebookSdk;
import com.ibm.mobilefirstplatform.clientsdk.android.core.api.BMSClient;

import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by hitman on 15/6/16.
 */
public class AppController extends Application {


    private RequestQueue mRequestQueue;
    private ServerDB serverDB;
//    private ImageLoader mImageLoader;

    private static AppController mInstance;
    private static final String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            //initialize SDK with IBM Bluemix application ID and route
            // You can find your backendRoute and backendGUID in the Mobile Options section on top of your Bluemix application dashboard
            //TODO: Please replace <APPLICATION_ROUTE> with a valid ApplicationRoute and <APPLICATION_ID> with a valid ApplicationId
//            super.onCreate();
//            mInstance = this;
            BMSClient.getInstance().initialize(this, "https://revolucian.mybluemix.net", "01e2b078-18b8-4a6d-8714-9595a66f8624");
            FacebookSdk.sdkInitialize(getApplicationContext());
            gettingHashKey();
//            GoogleAuthenticationManager.getInstance().register(getApplicationContext());

           new StartTask().execute((Void) null);

        }
        catch (MalformedURLException mue) {
//            this.setStatus("Unable to parse Application Route URL\n Please verify you have entered your Application Route and Id correctly and rebuild the app", false);

        }
    }

//    public static synchronized AppController getInstance() {
//        return mInstance;
//    }
//
//    public RequestQueue getRequestQueue() {
//        if (mRequestQueue == null) {
//            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
//        }
//
//        return mRequestQueue;
//    }
//
//    public ImageLoader getImageLoader() {
//        getRequestQueue();
//        if (mImageLoader == null) {
//            mImageLoader = new ImageLoader(this.mRequestQueue,
//                    new LruBitmapCache());
//        }
//        return this.mImageLoader;
//    }
//
//    public <T> void addToRequestQueue(Request<T> req, String tag) {
//        // set the default tag if tag is empty
//        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
//        getRequestQueue().add(req);
//    }
//
//    public <T> void addToRequestQueue(Request<T> req) {
//        req.setTag(TAG);
//        getRequestQueue().add(req);
//    }
//
//    public void cancelPendingRequests(Object tag) {
//        if (mRequestQueue != null) {
//            mRequestQueue.cancelAll(tag);
//        }
//    }

    public void gettingHashKey(){

        // Add code to print out the key hash
        try {
            //dont forget to change your package name here
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.example.hitman.revolucianapp",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                //this log statement will print out the hask key
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

    }

    public class StartTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
               e.printStackTrace();
                return false;
            }
            return true;
        }

    }

}
