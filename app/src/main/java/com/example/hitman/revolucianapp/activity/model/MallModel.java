package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 1/6/16.
 */
public class MallModel {

    private int id;
    private int image;
    private String shopName;
    private String shopTimings;
    private String shopNumber;
    private String area;
    private double distance;
    private double latitude;
    private double longitude;
    private String category;
    private double rating;

    public MallModel() {

    }

    public MallModel(int id, String shopName, String shopTimings, String area, String shopNumber) {
        this.id = id;
        this.shopName = shopName;
        this.shopTimings = shopTimings;
        this.shopNumber = shopNumber;
        this.area = area;
    }

    public MallModel(int image, String shopName, int id, String area, double latitude, double longitude, double rating) {
        this.image = image;
        this.id = id;
        this.shopName = shopName;
        this.area = area;
        this.latitude = latitude;
        this.longitude = longitude;
        this.rating = rating;
    }

    public MallModel(int image, String shopName, int id, String area, double latitude, double longitude, String category, double rating) {
        this.image = image;
        this.id = id;
        this.shopName = shopName;
        this.area = area;
        this.latitude = latitude;
        this.longitude = longitude;
        this.category = category;
        this.rating = rating;
    }

    public MallModel(int image, String shopName, String area, double latitude, double longitude, String category, double rating) {
        this.image = image;
        this.shopName = shopName;
        this.area = area;
        this.latitude = latitude;
        this.longitude = longitude;
        this.category = category;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopTimings() {
        return shopTimings;
    }

    public void setShopTimings(String shopTimings) {
        this.shopTimings = shopTimings;
    }

    public String getShopNumber() {
        return shopNumber;
    }

    public void setShopNumber(String shopNumber) {
        this.shopNumber = shopNumber;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}

