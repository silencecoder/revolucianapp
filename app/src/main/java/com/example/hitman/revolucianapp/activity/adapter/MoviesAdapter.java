package com.example.hitman.revolucianapp.activity.adapter;

/**
 * Created by hitman on 31/5/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.MoviesModel;

import java.util.List;

/**
 * Created by hitman on 31/5/16.
 */

public class MoviesAdapter extends InterceptorView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<MoviesModel> movies;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView movieItemImage;
        public TextView movieItemName, movieItemRating, movieItemAction;

        public MyViewHolder(View view) {
            super(view);
            movieItemImage = (ImageView) view.findViewById(R.id.movies_list_item_movieimage);
            movieItemName = (TextView) view.findViewById(R.id.movies_list_item_moviename);
            movieItemRating = (TextView) view.findViewById(R.id.movies_list_item_rating);
            movieItemAction = (TextView) view.findViewById(R.id.movies_list_item_action);
        }
    }

    public MoviesAdapter(Context context, List<MoviesModel> movies) {
        mContext = context;
        this.movies = movies;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movies_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MoviesModel movie = movies.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.movieItemImage.setBackgroundResource(movie.getImage());
        holder.movieItemName.setText(movie.getMovieName());
        holder.movieItemRating.setText(""+movie.getMovieRating());
        holder.movieItemAction.setText(""+movie.getMovieAction());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MoviesAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final MoviesAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);
                    

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}

