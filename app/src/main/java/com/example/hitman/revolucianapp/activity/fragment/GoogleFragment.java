package com.example.hitman.revolucianapp.activity.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.util.Util;
import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.activity.NavigationDrawerActivity;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.UserModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.ibm.mobilefirstplatform.clientsdk.android.security.internal.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GoogleFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link GoogleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class GoogleFragment extends Fragment implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, OnConnectionFailedListener{

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String UserName = "nameKey";
    public static final String UserPhone = "phoneKey";
    public static final String UserEmail = "emailKey";
    public static final String UserLogin = "login";
    public static final String UserType = "typeKey";
    private final static String TAG_URL_REGISTER = "http://192.168.1.103:8020/register/";
    private final static String TAG_URL_LOGIN = "http://192.168.1.103:8020/login/";
    private final static String TAG_SUCCESS = "success";
    private final static String TAG_MESSAGE = "message";
    /* Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;

    /* Client used to interact with Google APIs. */
    private GoogleApiClient mGoogleApiClient;

    /* A flag indicating that a PendingIntent is in progress and prevents
   * us from starting further intents.
   */
    private boolean mIntentInProgress;

    private boolean mShouldResolve;
    private Person person;

    /*finding connection result*/
    private ConnectionResult connectionResult;

    private SignInButton signInButton;
    private Button signOutButton;
    private TextView tvName, tvMail, tvNotSignedIn;
    private LinearLayout viewContainer;
    private ProgressDialog progressDialog;

    Point p;
    private View mProgressView;
    private UserLoginTask mAuthTask;
    private UserExist mUserExist;
    private ServerDB serverDB;
    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;

    // TODO: Rename and change types and number of parameters
    public static GoogleFragment newInstance(String param1, String param2) {
        GoogleFragment fragment = new GoogleFragment();

        return fragment;
    }

    public GoogleFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_google, container, false);

        prefManager = new PrefManager(getActivity());
        databaseAdapter = new DatabaseAdapter(getActivity().getApplicationContext());

        signInButton = (SignInButton) v.findViewById(R.id.sign_in_button);
        mProgressView = v.findViewById(R.id.login_google_progress);
//        signOutButton = (Button) v.findViewById(R.id.sign_out_button);

        //Builg GoogleApiClient
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        onSignOutClicked();
        signInButton.setOnClickListener(this);

        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        //connect GoogleApiClient
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        //disconnect GoogleApiClient
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /*
    When the GoogleApiClient object is unable to establish a connection onConnectionFailed() is called
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(),
                    0).show();
            return;
        }

        if (!mIntentInProgress) {

            connectionResult = result;

            if (mShouldResolve) {

                resolveSignInError();
            }
        }

    }

    /*
    onConnectionFailed() was started with startIntentSenderForResult and the code RC_SIGN_IN,
    we can capture the result inside Activity.onActivityResult.
     */
    @Override
    public void onActivityResult(int requestCode, int responseCode,
                                 Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != Activity.RESULT_OK) {
                mShouldResolve = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                    Toast.makeText(getActivity().getApplicationContext(), email, Toast.LENGTH_LONG).show();
                    mUserExist = new UserExist(person.getDisplayName(), "123456789", Plus.AccountApi.getAccountName(mGoogleApiClient), "qwerty123");
                    //startDialog();
                    mUserExist.execute((Void) null);
                    Log.d("Login", "Successfully");
                } else {
                    // Permission Denied
                    Toast.makeText(getActivity().getApplicationContext(), "GET_ACCOUNTS Denied", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /*
    on the successfull connection onConnected is called
     */
    @TargetApi(23)
    @Override
    public void onConnected(Bundle arg0) {
        mShouldResolve = false;
        stopDiaplg();
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                person = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = person.getDisplayName();
                /*String email = Plus.AccountApi.getAccountName(mGoogleApiClient);*/
//                edit.putString(UserName, person.getDisplayName().toString());
//                edit.putString(UserEmail, Plus.AccountApi.getAccountName(mGoogleApiClient));
//                edit.putBoolean(UserLogin, true);
//                edit.commit();
                Toast.makeText(getActivity().getApplicationContext(),
                        "You are Logged In " + personName, Toast.LENGTH_LONG).show();

                if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

                    int hasWriteContactsPermission = ((Activity) getContext()).checkSelfPermission(Manifest.permission.GET_ACCOUNTS);
                    if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[] {Manifest.permission.GET_ACCOUNTS, Manifest.permission.ACCESS_FINE_LOCATION,
                                        Manifest.permission.READ_CONTACTS, Manifest.permission.ACCESS_COARSE_LOCATION},
                                REQUEST_CODE_ASK_PERMISSIONS);
                        return;
                    } else {
                        mUserExist = new UserExist(person.getDisplayName(), "123456789", Plus.AccountApi.getAccountName(mGoogleApiClient), "qwerty123");
                        startDialog();
                        mUserExist.execute((Void) null);
                        Log.d("Hello", "Hello1");
                    }
                } else {
                    mUserExist = new UserExist(person.getDisplayName(), "123456789", Plus.AccountApi.getAccountName(mGoogleApiClient), "qwerty123");
                    startDialog();
                    mUserExist.execute((Void) null);
                    Log.d("Hello", "Hello1");
                }
//                new Register().execute();
            } else {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Couldnt Get the Person Info", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //signOutUI();

    }

    private void startDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Logging In. Please wait...");
        progressDialog.setIndeterminate(false);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /*
    Used for resolving errors during signIn
    */
    private void resolveSignInError() {
        if (connectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                connectionResult.startResolutionForResult(getActivity(), RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    /*
    sign out UI
     */
    private void signOutUI() {
        signInButton.setVisibility(View.GONE);
        signOutButton.setVisibility(View.VISIBLE);
    }


    private void stopDiaplg() {
        if(progressDialog != null)
            progressDialog.dismiss();
    }
    /*
    SignIn UI
     */
    private void signInUI() {
        signInButton.setVisibility(View.VISIBLE);
        startDialog();
        mProgressView.setVisibility(View.VISIBLE);
    }


    /*
    called when the connection is suspended
     */
    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
        signInUI();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in_button:
                onSignInClicked();
                break;
        }
    }

    /*
    called when signIn Button is clicked
     */
    private void onSignInClicked() {
        if (!mGoogleApiClient.isConnecting()) {
            mShouldResolve = true;
            resolveSignInError();
        }
    }

    /*
    called when sign out button is clicked
     */
    private void onSignOutClicked() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            signInUI();
        }
    }


    private class UserExist extends AsyncTask<Void, Void, Boolean> {

        private String mName;
        private String mPhone;
        private String mEmail;
        private String mPassword;

        UserExist(String userName, String userPhone, String userEmail, String userPassword) {
            mName =  userName;
            mPhone = userPhone;
            mEmail = userEmail;
            mPassword = userPassword;
        }

        @Override
        protected void onPreExecute() {
            mProgressView.setVisibility(View.VISIBLE);
            Log.d("UserExist", "Yes");
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(1500);
                serverDB =  ServerDB.getInstance(getContext());
                return serverDB.isUserExist(mEmail);
            } catch (InterruptedException e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if(success) {
                databaseAdapter.loginUser(mEmail);
                prefManager.setUserLoggedIn(true);
                prefManager.setUserSigninType(getString(R.string.signin_google));
                startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
            } else {
                mAuthTask = new UserLoginTask(mName, mPhone, mEmail, mPassword);
                mAuthTask.execute((Void) null);
            }
        }
    }

    private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private UserModel userModel;
        private int id = 0;

        UserLoginTask(String userName, String userPhone, String userEmail, String userPassword) {
            userModel = new UserModel(userName, userEmail, userPhone, userPassword);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext());
            } catch (InterruptedException e) {
                return false;
            }
            serverDB.signUpUser(userModel);
            id = serverDB.getUserID(userModel.getEmail());
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            mProgressView.setVisibility(View.GONE);
            progressDialog.dismiss();
            if (success) {
                getActivity().finish();
                databaseAdapter.signupUser(userModel);
                prefManager.setUserEmail(userModel.getEmail());
                prefManager.setUserSigninType(getString(R.string.signin_google));
                prefManager.setUserId(id);
                startActivity(new Intent(getActivity(), NavigationDrawerActivity.class));
            } else {
//                mPasswordView.setError(getString(R.string.error_incorrect_password));
//                mPhoneView.setError(getString(R.string.error_invalid_phone));
//                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            //showProgress(false);
        }
    }

//    @Override
//    public void onAttach(Activity activity) {
//        super.onAttach(activity);
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//
//    }

//    // The method that displays the popup.
//    private void showPopup() {
//        int popupWidth = 200;
//        int popupHeight = 150;
//
//        // Inflate the popup_layout.xml
//        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getBaseContext()
//                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View popupView = layoutInflater.inflate(R.layout.popup_layout, null);
//        final PopupWindow popup = new PopupWindow(
//                popupView,
//                ViewGroup.LayoutParams.WRAP_CONTENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT);
//        popup.setFocusable(true);
//        Log.d("PopUp1", "PopUp1");
//        // Some offset to align the popup a bit to the right, and a bit down, relative to button's position.
//        int OFFSET_X = 30;
//        int OFFSET_Y = 30;
//
//        // Clear the default translucent background
//        popup.setBackgroundDrawable(new BitmapDrawable());
//        int location[] = new int[2];
//       /* signInButton.getLocationOnScreen(location);
//        //Initialize the Point with x, and y positions
//        p.x = location[0];
//        p.y = location[1];*/
//
//        // Displaying the popup at the specified location, + offsets.
//        popup.showAsDropDown(signInButton, 50, -30);
//
//        // Getting a reference to Close button, and close the popup when clicked.
//        final EditText editMobile = (EditText) popupView.findViewById(R.id.pop_mobile);
//        final RadioGroup pop_grp = (RadioGroup) popupView.findViewById(R.id.pop_type);
//        final RadioButton pop_btn = (RadioButton) popupView.findViewById(pop_grp.getCheckedRadioButtonId());
//        Button send = (Button) popupView.findViewById(R.id.pop_btn);
//        Log.d("Popup2", "Popup2");
//        send.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (editMobile.getText().toString().length() == 0 || pop_btn.getText().toString().length() == 0)
//                    return;
//                edit.putString(UserPhone, editMobile.getText().toString());
//                edit.putString(UserType, pop_btn.getText().toString());
//                edit.commit();
//                popup.dismiss();
//            }
//        });
//        Log.d("Popup3", "Popup3");
//    }


}

