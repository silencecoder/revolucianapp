package com.example.hitman.revolucianapp.activity.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.adapter.DeckCardAdapter;
import com.example.hitman.revolucianapp.activity.adapter.FollowShopAdapter;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;

public class FollowShopActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private Intent intent;
    private RecyclerView followRecyclerView;
    private View mProgressView;
    private LinearLayout navHeaderLL;
    private ImageView navHeaderImage;
    private TextView navHeaderName, navHeaderDesc;

    private static final float BITMAP_SCALE = 0.2f;
    private static final float BLUR_RADIUS = 9.5f;

    private PrefManager prefManager;
    private FollowShopAdapter followShopAdapter;
    private ArrayList<Object> followShops;
    private ServerDB serverDB;
    private DatabaseAdapter databaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_shop);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toolbar.setNavigationIcon(R.drawable.ic_action_list);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        followRecyclerView = (RecyclerView) findViewById(R.id.follow_recycler_view);
        mProgressView = findViewById(R.id.follow_shops_page_progress);

        followShops = new ArrayList<>();
        followShopAdapter = new FollowShopAdapter(getApplicationContext(), followShops);
        databaseAdapter = new DatabaseAdapter(getApplicationContext());

        prefManager = new PrefManager(getApplicationContext());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        followRecyclerView.setLayoutManager(layoutManager);
        followRecyclerView.setItemAnimator(new DefaultItemAnimator());
        followRecyclerView.setNestedScrollingEnabled(false);
        followRecyclerView.setHasFixedSize(false);
        followRecyclerView.setAdapter(followShopAdapter);

        followRecyclerView.addOnItemTouchListener(new FollowShopAdapter.RecyclerTouchListener(getApplicationContext(), followRecyclerView, new FollowShopAdapter.ClickListener(){
            @Override
            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideShowDialogFragment newFragment = SlideShowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");

//                Intent intent = new Intent(getActivity(), ShopPageActivity.class);
//                intent.putExtra("shopID", malls.get(position).getId());
                if(followShops.get(position) instanceof MallModel) {
                    intent = new Intent(FollowShopActivity.this, ShopPageActivity.class);
                    intent.putExtra("shopID", ((MallModel) followShops.get(position)).getId());
                }else if(followShops.get(position) instanceof FoodShopModel) {
                    intent = new Intent(FollowShopActivity.this, RestaurantActivity.class);
                    intent.putExtra("restaurantID", ((FoodShopModel) followShops.get(position)).getId());
                }
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        navigationView.setNavigationItemSelectedListener(this);

        addNavigationHeader();

        showProgress(true);
        new FollowShopsTask().execute((Void) null);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
//            fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.containerView, new TabFragment());
////            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
            intent = new Intent(FollowShopActivity.this, NavigationDrawerActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_offers) {

        } else if (id == R.id.nav_notification) {

        } else if (id == R.id.nav_follow) {

        } else if (id == R.id.nav_wishlist) {
            intent = new Intent(FollowShopActivity.this, WishListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_to_do) {
            intent = new Intent(this, UserProfileActivity.class);
            startActivity(intent);
        } else if(id == R.id.nav_logout) {
            prefManager.setUserLoggedIn(false);
            intent = new Intent(FollowShopActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            followRecyclerView.setVisibility(show? View.GONE:View.VISIBLE);
            followRecyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    followRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            followRecyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void addNavigationHeader() {
        View v = getLayoutInflater().inflate(R.layout.nav_header_navigation_drawer, null);
        navHeaderLL = (LinearLayout) v.findViewById(R.id.nav_header_ll);
        navHeaderImage = (ImageView) v.findViewById(R.id.nav_header_image);
        navHeaderName = (TextView) v.findViewById(R.id.nav_header_name);
        navHeaderDesc = (TextView) v.findViewById(R.id.nav_header_desc);

        navHeaderImage.setImageResource(R.drawable.profile);
        navHeaderDesc.setText(prefManager.getUserEmail());
        navHeaderName.setText(databaseAdapter.getUserName(prefManager.getUserEmail()));
        navHeaderLL.setBackground(new BitmapDrawable(getResources(),
                blur(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.profile))));
        navigationView.addHeaderView(v);
    }

    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }

    private class FollowShopsTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            if(prefManager.getUserId() <= 0) {
                prefManager.setUserId(serverDB.getUserID(prefManager.getUserEmail()));
            }
            followShops = serverDB.getUserFollowShops(prefManager.getUserId());
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);
            if(success) {
                followShopAdapter = new FollowShopAdapter(getApplicationContext(), followShops);
                followRecyclerView.setAdapter(followShopAdapter);
            } else {
                Snackbar.make(getCurrentFocus(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }
}
