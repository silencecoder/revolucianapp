package com.example.hitman.revolucianapp.activity.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.transition.Fade;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hitman.revolucianapp.BuildConfig;
import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.adapter.ViewPagerAdapter;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.fragment.FoodFragment;
import com.example.hitman.revolucianapp.activity.fragment.MallFragment;
import com.example.hitman.revolucianapp.activity.fragment.MoviesFragment;
import com.example.hitman.revolucianapp.activity.fragment.OffersFragment;
import com.example.hitman.revolucianapp.activity.utils.BackgroundSubscribeIntentService;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;
import com.example.hitman.revolucianapp.activity.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.MessagesOptions;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.NearbyPermissions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeOptions;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private static final int PERMISSIONS_REQUEST_CODE = 1111;

    private static final String KEY_SUBSCRIBED = "subscribed";

    /**
     * The entry point to Google Play Services.
     */
    private GoogleApiClient mGoogleApiClient;

    private boolean mSubscribed = false;

    /**
     * Adapter for working with messages from nearby beacons.
     */
    private ArrayAdapter<String> mNearbyMessagesArrayAdapter;

    /**
     * Backing data structure for {@code mNearbyMessagesArrayAdapter}.
     */
    private List<String> mNearbyMessagesList = new ArrayList<>();

    private static final String TAG = MainActivity.class.getSimpleName();

    private DrawerLayout drawerLayout;

    private Toolbar toolbar, toolbar1;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;
    private Intent intent;
    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private AppBarLayout appBarLayout;
    private CoordinatorLayout mContainer;
    private LinearLayout navHeaderLL;
    private ImageView navHeaderImage;
    private TextView navHeaderName, navHeaderDesc;

    private static final float BITMAP_SCALE = 0.2f;
    private static final float BLUR_RADIUS = 9.5f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        prefManager = new PrefManager(this);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar1 = (Toolbar) findViewById(R.id.toolbar1);
        toolbar1.setVisibility(View.GONE);
        mContainer = (CoordinatorLayout) findViewById(R.id.nav_activity_cl);

        setSupportActionBar(toolbar);
        setupWindowAnimations();

//        action_bar = (LinearLayout) findViewById(R.id.action_bar);
//        setActionBar(action_bar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toolbar.setNavigationIcon(R.drawable.ic_action_list);
        toggle.syncState();

        databaseAdapter = new DatabaseAdapter(getApplicationContext());

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        addNavigationHeader();

        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
//                if(verticalOffset == 0 || verticalOffset <= toolbar.getHeight()){
//                    mCollapsingToolbar.setTitle(mCollapsedTitle);
//                }else if(!mToolbar.getTitle().equals(mExpandedTitle)){
//                    mCollapsingToolbar.setTitle(mExpandedTitle);
//                }
//                Log.d("Vertical offset", verticalOffset+"");
//                Log.d("AppBarHeight", ""+appBarLayout.getHeight());
//                Log.d("TabBarLayout", ""+tabLayout.getHeight());
//                Log.d("ToolBar1", ""+toolbar1.getHeight());
                if(Math.abs(verticalOffset) >= appBarLayout.getHeight() - toolbar1.getHeight() - tabLayout.getHeight() - 10) {
//                    Timer t = new Timer(false);
//                    t.schedule(new TimerTask() {
//                        @Override
//                        public void run() {
//                            runOnUiThread(new Runnable() {
//                                public void run() {
//                                    toolbar1.setVisibility(View.VISIBLE);
//                                }
//                            });
//                        }
//                    }, 3000);
//                    final Animation animationFadeOut = AnimationUtils.loadAnimation(NavigationDrawerActivity.this, R.anim.fadein);
//                    //toolbar1.setAnimation(animationFadeOut);
                    TranslateAnimation animate = new TranslateAnimation(0,toolbar1.getHeight(),0,0);
                    animate.setDuration(5000);
                    animate.setFillAfter(true);
                    toolbar1.startAnimation(animate);
                    toolbar1.setVisibility(View.VISIBLE);
                    setSupportActionBar(toolbar1);
                    toggle = new ActionBarDrawerToggle(
                            NavigationDrawerActivity.this, drawerLayout, toolbar1, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                    drawerLayout.setDrawerListener(toggle);
                    toolbar1.setNavigationIcon(R.drawable.ic_action_list);
                    toggle.syncState();
//                    Log.d("Toolbar", "Toolbar1");
                } else {
                    toolbar1.setVisibility(View.GONE);
                    setSupportActionBar(toolbar);
                    toggle = new ActionBarDrawerToggle(
                            NavigationDrawerActivity.this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                    drawerLayout.setDrawerListener(toggle);
                    toolbar.setNavigationIcon(R.drawable.ic_action_list);
                    toggle.syncState();
//                    Log.d("Toolbar", "Toolbar");
                }
            }
        });


        if (savedInstanceState != null) {
            mSubscribed = savedInstanceState.getBoolean(KEY_SUBSCRIBED, false);
        }


        if (!havePermissions()) {
            Log.i(TAG, "Requesting permissions needed for this app.");
            requestPermissions();
        }

        final List<String> cachedMessages = Utils.getCachedMessages(getApplicationContext());
        if (cachedMessages != null) {
            mNearbyMessagesList.addAll(cachedMessages);
            for(int i = 0; i < mNearbyMessagesList.size(); i++)
                Log.e("Beacon", cachedMessages.get(i));
        } else
            Log.e("Beacon", "Not working");
//        fragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new MallFragment(), "Store");
        viewPagerAdapter.addFragment(new FoodFragment(), "Food");
        viewPagerAdapter.addFragment(new MoviesFragment(), "FunZone");
        viewPagerAdapter.addFragment(new OffersFragment(), "Offers");
        viewPager.setAdapter(viewPagerAdapter);
    }

    @Override
    public void onBackPressed() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @TargetApi(21)
    private void setupWindowAnimations() {
        Fade fade = new Fade();
        fade.setDuration(1000);
        getWindow().setEnterTransition(fade);

        Slide slide = new Slide();
        fade.setDuration(1000);
        getWindow().setReturnTransition(slide);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

//        noinspection SimplifiableIfStatement
        switch(id) {
            case R.id.action_search:
                intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
//            fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.containerView, new TabFragment());
////            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
        } else if (id == R.id.nav_offers) {

        } else if (id == R.id.nav_notification) {

        } else if (id == R.id.nav_follow) {
            intent = new Intent(NavigationDrawerActivity.this, FollowShopActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_wishlist) {
            intent = new Intent(NavigationDrawerActivity.this, WishListActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_to_do) {
            intent = new Intent(this, UserProfileActivity.class);
            startActivity(intent);
        } else if(id == R.id.nav_logout) {
            prefManager.setUserLoggedIn(false);
            intent = new Intent(NavigationDrawerActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);

        if (havePermissions()) {
            buildGoogleApiClient();
        }
    }

    @TargetApi(23)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != PERMISSIONS_REQUEST_CODE) {
            return;
        }
        for (int i = 0; i < permissions.length; i++) {
            String permission = permissions[i];
            if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                if (shouldShowRequestPermissionRationale(permission)) {
                    Log.i(TAG, "Permission denied without 'NEVER ASK AGAIN': " + permission);
                    showRequestPermissionsSnackbar();
                } else {
                    Log.i(TAG, "Permission denied with 'NEVER ASK AGAIN': " + permission);
                    showLinkToSettingsSnackbar();
                }
            } else {
                Log.i(TAG, "Permission granted, building GoogleApiClient");
                buildGoogleApiClient();
            }
        }
    }

    private void addNavigationHeader() {
        View v = LayoutInflater.from(this).inflate(R.layout.nav_header_navigation_drawer, null);
        navHeaderLL = (LinearLayout) v.findViewById(R.id.nav_header_ll);
        navHeaderImage = (ImageView) v.findViewById(R.id.nav_header_image);
        navHeaderName = (TextView) v.findViewById(R.id.nav_header_name);
        navHeaderDesc = (TextView) v.findViewById(R.id.nav_header_desc);

        navHeaderImage.setImageResource(R.drawable.profile);
        navHeaderDesc.setText(prefManager.getUserEmail());
        navHeaderName.setText(databaseAdapter.getUserName(prefManager.getUserEmail()));
        navHeaderLL.setBackground(new BitmapDrawable(getResources(),
                blur(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.profile))));
        navigationView.addHeaderView(v);
    }

    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }

    private synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Nearby.MESSAGES_API, new MessagesOptions.Builder()
                            .setPermissions(NearbyPermissions.BLE).build())
                    .addConnectionCallbacks(this)
                    .enableAutoManage(this, this)
                    .build();
        }
    }

    @Override
    protected void onPause() {
        getSharedPreferences(getApplicationContext().getPackageName(), Context.MODE_PRIVATE)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mContainer != null) {
            Snackbar.make(mContainer, "Exception while connecting to Google Play services: " +
                            connectionResult.getErrorMessage(),
                    Snackbar.LENGTH_INDEFINITE).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "Connection suspended. Error code: " + i);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "GoogleApiClient connected");
        subscribe();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (TextUtils.equals(key, Utils.KEY_CACHED_MESSAGES)) {
            mNearbyMessagesList.clear();
            mNearbyMessagesList.addAll(Utils.getCachedMessages(this));
            mNearbyMessagesArrayAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_SUBSCRIBED, mSubscribed);
    }

    private boolean havePermissions() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_CODE);
    }

    /**
     * Calls {@link Messages#subscribe(GoogleApiClient, MessageListener, SubscribeOptions)},
     * using a {@link Strategy} for BLE scanning. Attaches a {@link ResultCallback} to monitor
     * whether the call to {@code subscribe()} succeeded or failed.
     */
    private void subscribe() {
        // In this sample, we subscribe when the activity is launched, but not on device orientation
        // change.
        if (mSubscribed) {
            Log.i(TAG, "Already subscribed.");
            return;
        }

        SubscribeOptions options = new SubscribeOptions.Builder()
                .setStrategy(Strategy.BLE_ONLY)
                .build();

        Nearby.Messages.subscribe(mGoogleApiClient, getPendingIntent(), options)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        if (status.isSuccess()) {
                            Log.i(TAG, "Subscribed successfully.");
                            startService(getBackgroundSubscribeServiceIntent());
                        } else {
                            Log.e(TAG, "Operation failed. Error: " +
                                    NearbyMessagesStatusCodes.getStatusCodeString(
                                            status.getStatusCode()));
                        }
                    }
                });
    }

    private PendingIntent getPendingIntent() {
        return PendingIntent.getService(this, 0,
                getBackgroundSubscribeServiceIntent(), PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private Intent getBackgroundSubscribeServiceIntent() {
        return new Intent(this, BackgroundSubscribeIntentService.class);
    }

    /**
     * Displays {@link Snackbar} instructing user to visit Settings to grant permissions required by
     * this application.
     */
    private void showLinkToSettingsSnackbar() {
        if (mContainer == null) {
            return;
        }
        Snackbar.make(mContainer,
                R.string.permission_denied_explanation,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.settings, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Build intent that displays the App settings screen.
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",
                                BuildConfig.APPLICATION_ID, null);
                        intent.setData(uri);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }).show();
    }

    /**
     * Displays {@link Snackbar} with button for the user to re-initiate the permission workflow.
     */
    private void showRequestPermissionsSnackbar() {
        if (mContainer == null) {
            return;
        }
        Snackbar.make(mContainer, R.string.permission_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Request permission.
                        ActivityCompat.requestPermissions(NavigationDrawerActivity.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                PERMISSIONS_REQUEST_CODE);
                    }
                }).show();
    }
}
