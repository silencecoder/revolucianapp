package com.example.hitman.revolucianapp.activity.fragment;

/**
 * Created by hitman on 4/6/16.
 */

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.adapter.OfferAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.OfferModel;
import com.example.hitman.revolucianapp.activity.utils.GPSTracker;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoviesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MoviesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OffersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoviesFragment.
     */
    private RecyclerView recyclerView;
//    private EditText editSearch;
    private LinearLayout offer_ll_popular, offer_ll_followed, offer_ll_suggested;
    private View mProgressView;

    private ArrayList<OfferModel> offers;
    private OfferAdapter offerAdapter;
    private GPSTracker gpsTracker;
    private ServerDB serverDB;
    private OfferTask mAuthTask;

    final String[] category = {"popular", "followed", "suggested"};

    // TODO: Rename and change types and number of parameters
    public static OffersFragment newInstance(String param1, String param2) {
        OffersFragment fragment = new OffersFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public OffersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_offers, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.offers_ll_recycler_view);
//        editSearch = (EditText) v.findViewById(R.id.offers_edit_text_search);
        offer_ll_popular = (LinearLayout) v.findViewById(R.id.offers_ll_popular);
        offer_ll_followed = (LinearLayout) v.findViewById(R.id.offers_ll_followed);
        offer_ll_suggested = (LinearLayout) v.findViewById(R.id.offers_ll_suggested);
        mProgressView = v.findViewById(R.id.offers_progress);

        offers = new ArrayList<>();
        gpsTracker = new GPSTracker(getContext(), getActivity());
        addMovies();
        offerAdapter = new OfferAdapter(getContext(), offers);

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(15));
        recyclerView.setAdapter(offerAdapter);

//        editSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SearchActivity.class));
//            }
//        });

        offer_ll_popular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new OfferTask(0);
                mAuthTask.execute((Void) null);
            }
        });

        offer_ll_followed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new OfferTask(1);
                mAuthTask.execute((Void) null);
            }
        });

        offer_ll_suggested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new OfferTask(2);
                mAuthTask.execute((Void) null);
            }
        });

        return v;
    }

    private void addMovies() {
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
//        offers.add(new OfferModel(R.drawable.civilwar, "Baharaat", "Ambattur", 523.3, "Get 10% flat off on minimum bill of Rs 300"));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            recyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class OfferTask extends AsyncTask<Void, Void, Boolean> {

        int flag;

        OfferTask(int flag) {
            this.flag = flag;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext(), getActivity());
            } catch (InterruptedException e) {
                return false;
            }
            offers = serverDB.getOffers(category[flag]);

            return true;
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mPhone)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            findDistance();
            Log.d("Size", ""+offers.size());
            if (success) {
                offerAdapter = new OfferAdapter(getActivity().getBaseContext(), offers);
                recyclerView.setAdapter(offerAdapter);
            } else {
                recyclerView.setVisibility(View.GONE);
                Snackbar.make(getView(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        private void findDistance() {
            if(gpsTracker.canGetLocation()) {
                for (OfferModel offer: offers) {
                    Location location = new Location(offer.getOfferShopName());
                    location.setLatitude(offer.getOfferShopLatitude());
                    location.setLongitude(offer.getOfferShopLongitude());
                    offer.setOfferShopDistance(gpsTracker.getDistance(location));
                }
            } else {
                gpsTracker.showSettingsAlert();
            }
        }
    }
}
