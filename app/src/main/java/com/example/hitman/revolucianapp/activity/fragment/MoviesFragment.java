package com.example.hitman.revolucianapp.activity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.activity.SearchActivity;
import com.example.hitman.revolucianapp.activity.adapter.FoodAdapter;
import com.example.hitman.revolucianapp.activity.adapter.MoviesAdapter;
import com.example.hitman.revolucianapp.activity.model.MoviesModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MoviesFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MoviesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MoviesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MoviesFragment.
     */
    private ArrayList<MoviesModel> movies;
    private MoviesAdapter moviesAdapter;
    private RecyclerView recyclerView;
//    private EditText editSearch;

    HorizontalScrollView sv;

    // TODO: Rename and change types and number of parameters
    public static MoviesFragment newInstance(String param1, String param2) {
        MoviesFragment fragment = new MoviesFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public MoviesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_movies, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.movies_ll_recycler_view);
//        editSearch = (EditText) v.findViewById(R.id.movies_edit_text_search);

        movies = new ArrayList<>();

        addMovies();
        moviesAdapter = new MoviesAdapter(getContext(), movies);

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(8));
        recyclerView.setAdapter(moviesAdapter);

//        editSearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SearchActivity.class));
//            }
//        });

        return v;
    }

    private void addMovies() {
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
        movies.add(new MoviesModel(R.drawable.ic_vividseats, "X-Men Apocalypse", 8.3, "Thriller, Action"));
    }

}
