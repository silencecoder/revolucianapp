package com.example.hitman.revolucianapp.activity.LayoutManager;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by hitman on 31/5/16.
 */
public class InterceptorView extends RecyclerView {

        InterceptorView(Context context) {
            super(context);
        }

    public InterceptorView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InterceptorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return false;
    }
}
