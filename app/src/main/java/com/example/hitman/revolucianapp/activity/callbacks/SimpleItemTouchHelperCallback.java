package com.example.hitman.revolucianapp.activity.callbacks;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;

import com.example.hitman.revolucianapp.activity.holder.FoodViewHolder;
import com.example.hitman.revolucianapp.activity.holder.MallViewHolder;
import com.example.hitman.revolucianapp.activity.interfaces.ItemTouchHelperAdapter;

/**
 * Created by hitman on 12/7/16.
 */
public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ItemTouchHelperAdapter mAdapter;

    public static final float ALPHA_FULL = 1.0f;

    public SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter) {
        mAdapter = adapter;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
        int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                          RecyclerView.ViewHolder target) {
        mAdapter.onItemMove(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
//        if(direction == ItemTouchHelper.LEFT || direction == ItemTouchHelper.RIGHT)
////            mAdapter.onItemDismiss(viewHolder.getAdapterPosition());
//            Log.d("diection", "left");
//        else
//            Log.d("direction", "different");
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            // Fade out the view as it is swiped out of the parent's bounds
            final float alpha = ALPHA_FULL - Math.abs(dX) / (float) viewHolder.itemView.getWidth();
            viewHolder.itemView.setAlpha(alpha);
            viewHolder.itemView.setTranslationX(dX);
        } else {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        // We only want the active item to change
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            if (viewHolder instanceof MallViewHolder) {
                // Let the view holder know that this item is being moved or dragged
                MallViewHolder itemViewHolder = (MallViewHolder) viewHolder;
                itemViewHolder.onItemSelected();
            } else if (viewHolder instanceof FoodViewHolder) {
                // Let the view holder know that this item is being moved or dragged
                FoodViewHolder itemViewHolder = (FoodViewHolder) viewHolder;
                itemViewHolder.onItemSelected();
            }
        }

        super.onSelectedChanged(viewHolder, actionState);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);

        viewHolder.itemView.setAlpha(ALPHA_FULL);
        if (viewHolder instanceof MallViewHolder) {
            // Let the view holder know that this item is being moved or dragged
            MallViewHolder itemViewHolder = (MallViewHolder) viewHolder;
            itemViewHolder.onItemClear();
        } else if (viewHolder instanceof FoodViewHolder) {
            // Let the view holder know that this item is being moved or dragged
            FoodViewHolder itemViewHolder = (FoodViewHolder) viewHolder;
            itemViewHolder.onItemClear();
        }
    }


}

