package com.example.hitman.revolucianapp.activity.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.model.UserModel;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;

import java.util.ArrayList;

/**
 * Created by hitman on 11/6/16.
 */
public class DatabaseAdapter {
    private Context context;
    private DBHelper helper;

    public DatabaseAdapter(Context context) {
        helper = new DBHelper(context);
    }

    public boolean signupUser(UserModel userModel) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.USER_NAME, userModel.getUserName());
        contentValues.put(helper.USER_EMAIL, userModel.getEmail());
        contentValues.put(helper.USER_MOBILE, userModel.getPhone());
        contentValues.put(helper.USER_PASSWORD, userModel.getPassword());
        contentValues.put(helper.USER_LOGIN, 1);
        long i = sqLiteDatabase.insert(helper.TABLE_NAME_USER, null, contentValues);
        if(i >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isUserLoggedIn(String phone, String password) {
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] columns = {helper.USER_LOGIN};
        String[] selectionArgs = {phone, password};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_USER, columns,helper.USER_MOBILE+" =? AND "+helper.USER_PASSWORD+" =?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            if(cursor.getInt(cursor.getColumnIndex(helper.USER_LOGIN)) == 1)
                return true;
        }
        return false;
    }

    public int getUserID(String email) {
        int id = 0;
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] columns = {helper.USER_ID};
        String[] selectionColumns = {email};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_USER, columns, helper.USER_EMAIL+"=?", selectionColumns, null, null, null, null);
        while (cursor.moveToNext())
            id = cursor.getInt(cursor.getColumnIndex(columns[0]));
        return id;
    }

    public boolean loginUser(String mobile, String password) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] columns = {helper.USER_LOGIN};
        String[] selectionArgs = {mobile, password};
        contentValues.put(helper.USER_LOGIN, 1);
        long i = sqLiteDatabase.update(helper.TABLE_NAME_USER, contentValues, helper.USER_MOBILE+"=? AND "+helper.USER_PASSWORD
                +"=?", selectionArgs);
        if(i >= 0) {
            return true;
        }
        return false;
    }
    public boolean loginUser(String email) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] columns = {helper.USER_LOGIN};
        String[] selectionArgs = {email};
        contentValues.put(helper.USER_LOGIN, 1);
        long i = sqLiteDatabase.update(helper.TABLE_NAME_USER, contentValues, helper.USER_EMAIL +"=?", selectionArgs);
        if(i >= 0) {
            return true;
        }
        return false;
    }

    public String getUserName(String email) {
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.USER_NAME};
        String[] selectionArgs = {email};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_USER, columns,helper.USER_EMAIL+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            return cursor.getString(cursor.getColumnIndex(helper.USER_NAME));
        }
        return null;
    }

    public boolean insertDeck(String deckTitle, String deckDesc, String deckUser) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.DECK_TITLE, deckTitle);
        contentValues.put(helper.DECK_DESCRIPTION, deckDesc);
        contentValues.put(helper.DECK_USERNAME, deckUser);
        contentValues.put(helper.DECK_NO_CARDS, 0);
        long i = sqLiteDatabase.insert(helper.TABLE_NAME_DECK, null, contentValues);
        if(i >= 0) {
            return true;
        } else {
            return false;
        }
    }

    // Return all decks in sqlite database
    public ArrayList<UserProfileDeckModel> getAllDecks() {
        ArrayList<UserProfileDeckModel> userProfileDeckModels = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.DECK_ID, helper.DECK_TITLE, helper.DECK_USERNAME, helper.DECK_DESCRIPTION, helper.DECK_NO_CARDS};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_DECK, columns, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            userProfileDeckModels.add(new UserProfileDeckModel(cursor.getInt(cursor.getColumnIndex(columns[0])), R.drawable.ic_vividseats,
                    cursor.getString(cursor.getColumnIndex(columns[1])), cursor.getString(cursor.getColumnIndex(columns[2])),
                    cursor.getString(cursor.getColumnIndex(columns[3])), cursor.getInt(cursor.getColumnIndex(columns[4]))));
        }
        return  userProfileDeckModels;
    }

    // Update number of cards in a deck
    public long updateNumberOfCards(int deckID) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.DECK_NO_CARDS, getNumberOfCards(deckID)+1);
        return sqLiteDatabase.update(helper.TABLE_NAME_DECK, contentValues, helper.DECK_ID+"=?", new String[]{deckID+""});
    }

    // Get number of cards in a deck
    public int getNumberOfCards(int deckID) {
        int numberOfCards = 0;
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        String[] columns = {helper.DECK_NO_CARDS};
        String[] selectionArgs = {deckID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_DECK, columns, helper.DECK_ID+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            numberOfCards = cursor.getInt(cursor.getColumnIndex(columns[0]));
        }
        return numberOfCards;
    }

    // Insert card in deck
    public long insertCardInDeck(int deckID) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.DECK_ID, deckID);
        long i = sqLiteDatabase.insert(helper.TABLE_NAME_DECK_CARD, null, contentValues);
        return i;
    }

    public long insertMallCard(MallModel mallModel, int id) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        Log.e("Insert Mall", mallModel.getId()+mallModel.getShopName()+mallModel.getArea()+mallModel.getShopNumber()+mallModel.getShopTimings());
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.CARD_ID, id);
        contentValues.put(helper.CARD_MALL_ID, mallModel.getId());
        contentValues.put(helper.CARD_MALL_NAME, mallModel.getShopName());
        contentValues.put(helper.CARD_MALL_LOCATION, mallModel.getArea());
        contentValues.put(helper.CARD_MALL_TIMINGS, mallModel.getShopTimings());
        contentValues.put(helper.CARD_MALL_PHONE, mallModel.getShopNumber());
        long i = sqLiteDatabase.insert(helper.TABLE_NAME_CARD_MALL, null, contentValues);
        return i;
    }

    public void deleteMallCard(int deckId, int cardId) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        sqLiteDatabase.delete(helper.TABLE_NAME_CARD_MALL, helper.CARD_MALL_ID+"=?", new String[]{cardId+""} );
        sqLiteDatabase.delete(helper.TABLE_NAME_DECK_CARD, helper.DECK_ID+"=? AND "+helper.CARD_ID+"=?", new String[]{deckId+"", cardId+""});
        sqLiteDatabase.delete(helper.DROP_TABLE_CARD_TYPE, helper.CARD_ID+"=?", new String[]{cardId+""});
    }

    public long insertFoodCard(FoodShopModel foodShopModel, int id) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Log.e("Insert Food", foodShopModel.getId()+foodShopModel.getShopName()+foodShopModel.getShopTimings()+foodShopModel.getShopNumber()+
                foodShopModel.getShopCost());
        contentValues.put(helper.CARD_ID, id);
        contentValues.put(helper.CARD_FOOD_ID, foodShopModel.getId());
        contentValues.put(helper.CARD_FOOD_COST, foodShopModel.getShopCost());
        contentValues.put(helper.CARD_FOOD_LOCATION, foodShopModel.getShopLocation());
        contentValues.put(helper.CARD_FOOD_NAME, foodShopModel.getShopName());
        contentValues.put(helper.CARD_FOOD_TIMINGS, foodShopModel.getShopTimings());
        contentValues.put(helper.CARD_FOOD_PHONE, foodShopModel.getShopNumber());
        long i = sqLiteDatabase.insert(helper.TABLE_NAME_CARD_FOOD, null, contentValues);
        return i;
    }

    public void deleteFoodCard(int deckId, int cardId) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        sqLiteDatabase.delete(helper.TABLE_NAME_CARD_FOOD, helper.CARD_FOOD_ID+"=?", new String[]{cardId+""} );
        sqLiteDatabase.delete(helper.TABLE_NAME_DECK_CARD, helper.DECK_ID+"=? AND "+helper.CARD_ID+"=?", new String[]{deckId+"", cardId+""});
        sqLiteDatabase.delete(helper.DROP_TABLE_CARD_TYPE, helper.CARD_ID+"=?", new String[]{cardId+""});
    }

    public ArrayList<String> getDeckDetails(int deckID) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.DECK_TITLE, helper.DECK_DESCRIPTION, helper.DECK_USERNAME, helper.DECK_NO_CARDS};
        String[] selectionArgs = {deckID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_DECK, columns, helper.DECK_ID+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            arrayList.add(cursor.getString(cursor.getColumnIndex(helper.DECK_TITLE)));
            arrayList.add(cursor.getString(cursor.getColumnIndex(helper.DECK_DESCRIPTION)));
            arrayList.add(cursor.getString(cursor.getColumnIndex(helper.DECK_USERNAME)));
            arrayList.add(cursor.getString(cursor.getColumnIndex(helper.DECK_NO_CARDS)));
        }
        return  arrayList;
    }

    public ArrayList<Object> getCards(int deckID) {
        ArrayList<Object> arrayList = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.CARD_ID};
        String[] selectionArgs = {deckID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_DECK_CARD, columns, helper.DECK_ID+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            int cardID = cursor.getInt(cursor.getColumnIndex(columns[0]));
            String category = getCardCategory(cardID);
            if(category.equals("mall")) {
                arrayList.add(getMallDetails(cardID));
            } else if(category.equals("food")) {
                arrayList.add(getFoodShopDetails(cardID));
            } else if(category.equals("movie")) {

            }
        }
        return arrayList;
    }

    public String getCardCategory(int cardID) {
        String category = "";
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.CARD_TYPE_CATEGORY};
        String[] selectionArgs = {cardID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_CARD_TYPE, columns, helper.CARD_TYPE_ID+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            category = cursor.getString(cursor.getColumnIndex(columns[0]));
        }
        return category;
    }

    public long setCardCategory(String category, int cardID) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(helper.CARD_TYPE_ID, cardID);
        contentValues.put(helper.CARD_TYPE_CATEGORY, category);
        return sqLiteDatabase.insert(helper.TABLE_NAME_CARD_TYPE, null, contentValues);
    }

    public MallModel getMallDetails(int cardID) {
        MallModel mallModel = new MallModel();
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.CARD_MALL_ID, helper.CARD_MALL_NAME, helper.CARD_MALL_LOCATION, helper.CARD_MALL_TIMINGS,
                helper.CARD_MALL_PHONE};
        String[] selectionArgs = {cardID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_CARD_MALL, columns, helper.CARD_ID+"=?", selectionArgs, null, null, null, null);
        while (cursor.moveToNext()) {
            mallModel = new MallModel(cursor.getInt(cursor.getColumnIndex(columns[0])), cursor.getString(cursor.getColumnIndex(columns[1])),
                    cursor.getString(cursor.getColumnIndex(columns[3])), cursor.getString(cursor.getColumnIndex(columns[2])),
                    cursor.getString(cursor.getColumnIndex(columns[4])));
        }
        return  mallModel;
    }

    public FoodShopModel getFoodShopDetails(int cardID) {
        FoodShopModel foodShopModel = new FoodShopModel();
        SQLiteDatabase sqLiteDatabase = helper.getReadableDatabase();
        String[] columns = {helper.CARD_FOOD_ID, helper.CARD_FOOD_NAME, helper.CARD_FOOD_LOCATION, helper.CARD_FOOD_TIMINGS, helper.CARD_FOOD_COST,
                helper.CARD_FOOD_PHONE};
        String[] selectionArgs = {cardID+""};
        Cursor cursor = sqLiteDatabase.query(helper.TABLE_NAME_CARD_FOOD, columns, helper.CARD_ID+"=?", selectionArgs, null, null,
                null, null);
        while (cursor.moveToNext()) {
            foodShopModel = new FoodShopModel(cursor.getInt(cursor.getColumnIndex(columns[0])), cursor.getString(cursor.getColumnIndex(columns[1])),
                    cursor.getString(cursor.getColumnIndex(columns[2])), cursor.getString(cursor.getColumnIndex(columns[3])),
                    cursor.getString(cursor.getColumnIndex(columns[4])), cursor.getString(cursor.getColumnIndex(columns[5])));
        }
        return foodShopModel;
    }

    public boolean logoutUser(String mobile) {
        SQLiteDatabase sqLiteDatabase = helper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String[] columns = {helper.USER_LOGIN};
        String[] selectionArgs = {mobile};
        contentValues.put(helper.USER_LOGIN, 0);
        long i = sqLiteDatabase.update(helper.TABLE_NAME_USER, contentValues, helper.USER_MOBILE+"=?", selectionArgs);
        if(i >= 0) {
            return true;
        }
        return false;
    }

    static class DBHelper extends SQLiteOpenHelper {
        private static final int DATABASE_VERSION = 3;
        private static final String DATABASE_NAME = "revoluciandb";
        private Context context;

        private static final String TABLE_NAME_USER = "usertable";
        private static final String USER_ID = "user_id";
        private static final String USER_NAME = "user_name";
        private static final String USER_EMAIL = "user_email";
        private static final String USER_PASSWORD = "user_password";
        private static final String USER_MOBILE = "user_mobile";
        private static final String USER_SEX = "user_sex";
        private static final String USER_LOGIN = "user_login";
        private static final String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_NAME_USER + " (" + USER_ID + " INTEGER PRIMARY KEY" +
                " AUTOINCREMENT, " + USER_NAME + " VARCHAR(50) NOT NULL, " + USER_EMAIL + " VARCHAR(50) NOT NULL, " + USER_PASSWORD +
                " VARCHAR(50) NOT NULL, " + USER_MOBILE + " VARCHAR(50), " + USER_SEX + " INTEGER, " + USER_LOGIN + " INTEGER);";
        private final String DROP_TABLE_USER = "DROP TABLE IF EXISTS "+TABLE_NAME_USER;

        private static final String TABLE_NAME_DECK = "decktable";
        private static final String DECK_ID = "deck_id";
        private static final String DECK_TITLE = "deck_title";
        private static final String DECK_DESCRIPTION = "deck_description";
        private static final String DECK_USERNAME = "deck_username";
        private static final String DECK_NO_CARDS = "deck_no_cards";
        private static final String DECK_IMAGE = "deck_image";
        private final String CREATE_DECK_TABLE = "CREATE TABLE " + TABLE_NAME_DECK + " ( " + DECK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "  +
                 DECK_TITLE + " VARCHAR(50) NOT NULL, " + DECK_DESCRIPTION + " VARCHAR(100) NOT NULL, " + DECK_USERNAME + " VARCHAR(50) NOT NULL, " +
                DECK_NO_CARDS + " INTEGER DEFAULT 0);";
        private final String DROP_TABLE_DECK = "DROP TABLE IF EXISTS "+TABLE_NAME_DECK;

        private  static final String TABLE_NAME_DECK_CARD = "deckcardtable";
        private static final String CARD_ID = "card_id";
        private static final String CREATE_DECK_CARD_TABLE = "CREATE TABLE " + TABLE_NAME_DECK_CARD + " ( " + DECK_ID + " INTEGER, " +
                CARD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT);";
        private final String DROP_TABLE_DECK_CARD = "DROP TABLE IF EXISTS "+TABLE_NAME_DECK_CARD;

        private static final String TABLE_NAME_CARD_TYPE = "cardtypetable";
        private static final String CARD_TYPE_ID = "card_id";
        private static final String CARD_TYPE_CATEGORY = "card_category";
        private static final String CREATE_CARD_TYPE_TABLE = "CREATE TABLE " + TABLE_NAME_CARD_TYPE + " ( " + CARD_TYPE_ID + " INTEGER, " +
                CARD_TYPE_CATEGORY + " VARCHAR(100));";
        private final String DROP_TABLE_CARD_TYPE = "DROP TABLE IF EXISTS "+TABLE_NAME_CARD_TYPE;

        private static final String TABLE_NAME_CARD_MALL = "mallcardtable";
        private static final String CARD_MALL_ID = "mall_id";
        private static final String CARD_MALL_NAME = "mall_name";
        private static final String CARD_MALL_TIMINGS = "mall_timings";
        private static final String CARD_MALL_LOCATION = "mall_location";
        private static final String CARD_MALL_PHONE = "mall_phone";
        private static final String CARD_MALL_IMAGE = "mall_image";
        private static final String CARD_MALL_RATING = "mall_rating";
        private static final String CREATE_CARD_MALL_TABLE = "CREATE TABLE " + TABLE_NAME_CARD_MALL + " ( " + CARD_ID + " INTEGER " +
                "PRIMARY KEY, " + CARD_MALL_ID + " INTEGER, " + CARD_MALL_NAME + " VARCHAR(100) NOT NULL, " + CARD_MALL_TIMINGS + " VARCHAR(50) NOT NULL, " +
                CARD_MALL_LOCATION + " VARCHAR(100) NOT NULL, " + CARD_MALL_IMAGE + " VARCHAR(100), " + CARD_MALL_PHONE +
                " VARCHAR(20) NOT NULL);";
        private final String DROP_TABLE_CARD_MALL = "DROP TABLE IF EXISTS "+TABLE_NAME_CARD_MALL;

        private static final String TABLE_NAME_CARD_FOOD = "foodcardatble";
        private static final String CARD_FOOD_ID = "food_id";
        private static final String CARD_FOOD_NAME = "food_name";
        private static final String CARD_FOOD_TIMINGS = "food_timings";
        private static final String CARD_FOOD_LOCATION = "food_location";
        private static final String CARD_FOOD_PHONE = "food_phone";
        private static final String CARD_FOOD_COST = "food_cost";
        private static final String CARD_FOOD_IMAGE = "food_image";
        private static final String CARD_FOOD_RATING = "food_rating";
        private static final String CREATE_CARD_FOOD_TABLE = "CREATE TABLE " + TABLE_NAME_CARD_FOOD + " ( " + CARD_ID + " INTEGER " +
                "PRINARY KEY, " + CARD_FOOD_ID + " INTEGER, " + CARD_FOOD_NAME + " VARCHAR(100) NOT NULL, " + CARD_FOOD_TIMINGS + " VARCHAR(100) NOT NULL, " +
                CARD_FOOD_LOCATION + " VARCHAR(100) NOT NULL, " + CARD_FOOD_COST + " VARCHAR(100) NOT NULL, " + CARD_FOOD_IMAGE + " VARCHAR(100), "
                + CARD_FOOD_PHONE + " VARCHAR(20) NOT NULL);";
        private final String DROP_TABLE_CARD_FOOD = "DROP TABLE IF EXISTS "+TABLE_NAME_CARD_FOOD;

//        private static final String TABLE_NAME_CARD = "cardtable";
//        private static final String CARD_ID = "card_id";
//        private static final String CARD_NAME = "card_name";
//        private static final String CARD_DESCRIPTION = "card_description";
//        private static final String CARD_EMAIL = "user_email";
//        private static final String CARD_MOBILE = "user_mobile";
//        private static final String CARD_ADDRESS = "user_address";
//        private static final String CARD_LOCATION = "user_location";
//        private static final String CARD_AREA = "user_area";
//        private static final String CARD_PINCODE = "user_pincode";
//        private static final String USER_SAVE_ADDRESS = "user_save";
//        private final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_NAME_USER+" (" + USER_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "
//                + USER_FIRST_NAME + " VARCHAR(50), " + USER_LAST_NAME + " VARCHAR(50), "+ USER_EMAIL+" VARCHAR(50), " + USER_LOCATION +
//                "  VARCHAR(50)," + USER_AREA + " VARCHAR(50), " + USER_ADDRESS + " VARCHAR(50), " + USER_PINCODE + " VARCHAR(50), " +
//                USER_MOBILE + " VARCHAR(50), " + USER_SAVE_ADDRESS + " INTEGER DEFAULT 0);";
//        private final String DROP_TABLE_USER = "DROP TABLE IF EXISTS "+TABLE_NAME_USER;

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.context = context;
            //Toast.makeText(context, "Constructor", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_USER_TABLE);
                db.execSQL(CREATE_DECK_TABLE);
                db.execSQL(CREATE_DECK_CARD_TABLE);
                db.execSQL(CREATE_CARD_TYPE_TABLE);
                db.execSQL(CREATE_CARD_MALL_TABLE);
                db.execSQL(CREATE_CARD_FOOD_TABLE);
//                db.execSQL(CREATE_DECK_TABLE);
//                db.execSQL(CREATE_TABLE_USER);
//                Toast.makeText(context,"CREATE",Toast.LENGTH_LONG).show();
            }
            catch (SQLException e){
                Toast.makeText(context,"CREATE Exception",Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            try {
                db.execSQL(DROP_TABLE_USER);
                db.execSQL(DROP_TABLE_DECK);
                db.execSQL(DROP_TABLE_DECK_CARD);
                db.execSQL(DROP_TABLE_CARD_TYPE);
                db.execSQL(DROP_TABLE_CARD_MALL);
                db.execSQL(DROP_TABLE_CARD_FOOD);
                //db.execSQL(DROP_TABLE_USER);
                onCreate(db);
                //Toast.makeText(context,"UPGRADE",Toast.LENGTH_LONG).show();
            }
            catch (SQLException e){
                Toast.makeText(context,"UPGRADE Exception",Toast.LENGTH_LONG).show();
            }

        }
    }
}