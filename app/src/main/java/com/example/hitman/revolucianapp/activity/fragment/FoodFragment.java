package com.example.hitman.revolucianapp.activity.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.transition.Slide;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.activity.RestaurantActivity;
import com.example.hitman.revolucianapp.activity.activity.SearchActivity;
import com.example.hitman.revolucianapp.activity.adapter.FoodAdapter;
import com.example.hitman.revolucianapp.activity.adapter.MallAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.utils.GPSTracker;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FoodFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FoodFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FoodFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FoodFragment.
     */

    private View mProgressView;
    private RecyclerView recyclerView;
//    private EditText edit_search;
    private LinearLayout food_ll_restaurant, food_ll_cafe, food_ll_icecream, food_ll_nightlife;

    private ArrayList<FoodShopModel> foodShops;
    private FoodAdapter foodAdapter;
    private GPSTracker gpsTracker;
    private FoodStoreTask mAuthTask;
    private ServerDB serverDB;

    private String[] category = {"restaurant", "cafe", "icecream", "nightlife"};

    // TODO: Rename and change types and number of parameters
    public static FoodFragment newInstance(String param1, String param2) {
        FoodFragment fragment = new FoodFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public FoodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_food, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.food_ll_recycler_view);
//        edit_search = (EditText) v.findViewById(R.id.food_edit_text_search);
        food_ll_restaurant = (LinearLayout) v.findViewById(R.id.food_ll_restaurant);
        food_ll_icecream = (LinearLayout) v.findViewById(R.id.food_ll_icecream);
        food_ll_cafe = (LinearLayout) v.findViewById(R.id.food_ll_cafe);
        food_ll_nightlife = (LinearLayout) v.findViewById(R.id.food_ll_nightlife);
        mProgressView = v.findViewById(R.id.food_progress);

        foodShops = new ArrayList<>();
        addFoodShops();
        foodAdapter = new FoodAdapter(getContext(), foodShops);
        gpsTracker = new GPSTracker(getContext(), getActivity());

        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(3));
        recyclerView.setAdapter(foodAdapter);

//        edit_search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SearchActivity.class));
//            }
//        });

        recyclerView.addOnItemTouchListener(new FoodAdapter.RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new FoodAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideShowDialogFragment newFragment = SlideShowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");

                Intent intent = new Intent(getActivity(), RestaurantActivity.class);
                intent.putExtra("restaurantID", foodShops.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        food_ll_restaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new FoodStoreTask(0);
                mAuthTask.execute((Void) null);
            }
        });

        food_ll_cafe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new FoodStoreTask(1);
                mAuthTask.execute((Void) null);
            }
        });

        food_ll_icecream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new FoodStoreTask(2);
                mAuthTask.execute((Void) null);
            }
        });

        food_ll_nightlife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                mAuthTask = new FoodStoreTask(3);
                mAuthTask.execute((Void) null);
            }
        });

        return v;
    }

    private void addFoodShops() {
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
//        foodShops.add(new FoodShopModel(R.drawable.ic_vividseats, "Jubillee Hills, Road 45, Main Rd, Adjacent to Pedamma TempleRd" +
//                ", Hyderabad", "Heart Cup Coffee", 8.3, 1.9));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
            recyclerView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class FoodStoreTask extends AsyncTask<Void, Void, Boolean> {

        int flag;

        FoodStoreTask(int flag) {
            this.flag = flag;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext(), getActivity());
            } catch (InterruptedException e) {
                return false;
            }
            foodShops = serverDB.getFood(category[flag]);

            return true;
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mPhone)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);
            findDistance();
            Log.d("Size", ""+foodShops.size());
            if (success) {
                foodAdapter = new FoodAdapter(getActivity().getBaseContext(), foodShops);
                recyclerView.setAdapter(foodAdapter);
            } else {
                recyclerView.setVisibility(View.GONE);
                Snackbar.make(getView(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }

        private void findDistance() {
            if(gpsTracker.canGetLocation()) {
                for (FoodShopModel foodShop: foodShops) {
                    Location location = new Location(foodShop.getShopName());
                    location.setLatitude(foodShop.getShopLatitude());
                    location.setLongitude(foodShop.getShopLongitude());
                    foodShop.setShopDistance(gpsTracker.getDistance(location));
                }
            } else {
                gpsTracker.showSettingsAlert();
            }
        }
    }
}
