package com.example.hitman.revolucianapp.activity.holder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;

/**
 * Created by hitman on 12/7/16.
 */
public class MallViewHolder extends RecyclerView.ViewHolder {

    public TextView cardShopName, cardShopTimings, cardShopLocation;
    public ImageView cardShopCall;

    public MallViewHolder(View v) {
        super(v);
        cardShopName = (TextView) v.findViewById(R.id.card_shop_name);
        cardShopTimings = (TextView) v.findViewById(R.id.card_shop_timings);
        cardShopLocation = (TextView) v.findViewById(R.id.card_shop_address);
        cardShopCall = (ImageView) v.findViewById(R.id.card_shop_call);
    }

    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }
}

