package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;

import java.util.List;

/**
 * Created by hitman on 31/5/16.
 */

public class FoodAdapter extends InterceptorView.Adapter<FoodAdapter.MyViewHolder> {

    private List<FoodShopModel> foodShops;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView foodItemImage;
        public TextView foodItemShopName, foodItemLocation, foodItemRating, foodItemDistance;

        public MyViewHolder(View view) {
            super(view);
            foodItemImage = (ImageView) view.findViewById(R.id.food_list_item_shopimage);
            foodItemShopName = (TextView) view.findViewById(R.id.food_list_item_shopname);
            foodItemLocation = (TextView) view.findViewById(R.id.food_list_item_location);
            foodItemRating = (TextView) view.findViewById(R.id.food_list_item_rating);
            foodItemDistance = (TextView) view.findViewById(R.id.food_list_item_distance);
        }
    }

    public FoodAdapter(Context context, List<FoodShopModel> foodShops) {
        mContext = context;
        this.foodShops = foodShops;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        FoodShopModel foodShop = foodShops.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.foodItemImage.setBackgroundResource(foodShop.getImage());
        holder.foodItemShopName.setText(foodShop.getShopName());
        holder.foodItemLocation.setText(foodShop.getShopLocation());
        holder.foodItemDistance.setText(""+foodShop.getShopDistance()+"km");
        holder.foodItemRating.setText(""+foodShop.getShopRating());
    }

    @Override
    public int getItemCount() {
        return foodShops.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private FoodAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final FoodAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
