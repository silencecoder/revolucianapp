package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 30/6/16.
 */
public class FilterItemModel{

    private String title;
    private boolean flag;

    public FilterItemModel(String title) {
        // TODO Auto-generated constructor stub
        this.title = title;
        this.flag = false;
    }


    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
