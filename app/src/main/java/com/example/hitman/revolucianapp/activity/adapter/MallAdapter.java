package com.example.hitman.revolucianapp.activity.adapter;

/**
 * Created by hitman on 1/6/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.MallModel;

import java.util.List;

/**
 * Created by hitman on 31/5/16.
 */

public class MallAdapter extends InterceptorView.Adapter<MallAdapter.MyViewHolder> {

    private List<MallModel> malls;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout mallItemImage;
        public TextView mallItemShopName, mallItemRating, mallItemDistance, mallItemCategory, mallItemArea;

        public MyViewHolder(View view) {
            super(view);
            mallItemImage = (LinearLayout) view.findViewById(R.id.mall_list_item_image);
            mallItemShopName = (TextView) view.findViewById(R.id.mall_list_item_shop_name);
            mallItemRating = (TextView) view.findViewById(R.id.mall_list_item_rating);
            mallItemDistance = (TextView) view.findViewById(R.id.mall_list_item_distance);
            mallItemCategory = (TextView) view.findViewById(R.id.mall_list_item_category);
            mallItemArea = (TextView) view.findViewById(R.id.mall_list_item_shop_area);
        }
    }

    public MallAdapter(Context context, List<MallModel> malls) {
        mContext = context;
        this.malls = malls;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mall_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MallModel mall = malls.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.mallItemShopName.setText(mall.getShopName());
        holder.mallItemRating.setText(""+mall.getRating());
        holder.mallItemDistance.setText(mall.getDistance()+"km");
        holder.mallItemArea.setText(mall.getArea());
        holder.mallItemCategory.setText(mall.getCategory());
        holder.mallItemImage.setBackgroundResource(R.drawable.shop);
    }

    @Override
    public int getItemCount() {
        return malls.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private MallAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final MallAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}