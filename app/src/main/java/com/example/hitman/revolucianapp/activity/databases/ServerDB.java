package com.example.hitman.revolucianapp.activity.databases;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.model.FoodShopModel;
import com.example.hitman.revolucianapp.activity.model.ItemModel;
import com.example.hitman.revolucianapp.activity.model.MallModel;
import com.example.hitman.revolucianapp.activity.model.OfferModel;
import com.example.hitman.revolucianapp.activity.model.RestaurantPageDealsModel;
import com.example.hitman.revolucianapp.activity.model.ReviewModel;
import com.example.hitman.revolucianapp.activity.model.SearchItemModel;
import com.example.hitman.revolucianapp.activity.model.ShopPageDealsModel;
import com.example.hitman.revolucianapp.activity.model.ShopPageInventoryItemModel;
import com.example.hitman.revolucianapp.activity.model.UserModel;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by hitman on 15/6/16.
 */
public class ServerDB {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
//    static final String DB_URL = "jdbc:mysql://us-cdbr-iron-east-04.cleardb.net/ad_2d79fe4f8df71fd?user=bfae2c7043f2df&password=ff41f63f";
    static final String DB_URL = "jdbc:mysql://us-cdbr-iron-east-04.cleardb.net/ad_d50e86b971b02f2?user=bf87fb0cec2402&password=60fcb38d";
    Connection conn = null;
    Statement stmt = null;
    static ServerDB serverDB = null;
    private static Context context;
    private static Activity activity;
    // Database credentials
//    static final String USER = "bfae2c7043f2df";
//    static final String PASS = "ff41f63f";


    private ServerDB() {
        connectToDB();
    }

    public static ServerDB getInstance(Context context1) {
        context = context1;
        if(serverDB == null)
            serverDB = new ServerDB();
        return serverDB;
    }

    public static ServerDB getInstance(Context context1, Activity activity1) {
        context = context1;
        activity = activity1;
        if(serverDB == null)
            serverDB = new ServerDB();
        return serverDB;
    }

    private void connectToDB() {
        try{
            //Class.forName("com.mysql.jdbc.Driver");
            Class.forName(JDBC_DRIVER);
            Log.d("Not Connected", "Not Connected");
            conn = DriverManager.getConnection(DB_URL);
            Log.d("Connected", "Connected");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void verifyConnection() {
        if(conn == null)
            connectToDB();
    }

    /*
     * closeConnection function that will close all connections with database
     */
    void closeConnection() {
        try {
            if(conn != null)
                conn.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    /* Create a user and add user data into the global server
     */
    public void signUpUser(UserModel userModel) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        //ChatUser user = new ChatUser(user_name, password);
        String sql = "INSERT INTO `UserTable`(`userName`, `userPassword`, `userPhone`, `userEmail`, `userVerification`) "
                + "VALUES(?, ?, ?, ?, ?)";
        String sql1 = "select userID from usertable where userEmail=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, userModel.getUserName());
            stat.setString(2, userModel.getPassword());
            stat.setString(3, userModel.getPhone());
            stat.setString(4, userModel.getEmail());
            stat.setBoolean(5, true);
            stat.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    public int getUserID(String email) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        int id = -1;
        String sql = "select userID from userTable where userEmail = ?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, email);
            rs = stat.executeQuery();
            while (rs.next())
                id = rs.getInt("userID");
            Log.d("User ID Server", id+"");
            Log.d("User Email", email);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    public boolean isUserExist(String email) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select * from userTable where userEmail = ?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, email);
            rs = stat.executeQuery();
            while (rs.next())
                return true;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /* Verify user credentials when user login into app
     */
    public boolean loginUser(String phone, String password) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        //ChatUser user = new ChatUser(user_name, password);
        String sql = "select * from UserTable where userPhone=? and userPassword=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, phone);
            stat.setString(2, password);
            rs = stat.executeQuery();
            if(rs.next())
                return true;
            else
                return false;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean logoutUser(String phone) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        //ChatUser user = new ChatUser(user_name, password);
        String sql = "update UserTable set userStatus = ? where userPhone = ?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setBoolean(1, false);
            stat.setString(2, phone);
            stat.executeUpdate();
            return true;
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Get Stores in a particular mall
    public ArrayList<MallModel> getStoresInMalls() {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.latitude, loc.longitude FROM " +
                "shoptable as s inner join locationtable as loc, shoploctable as shop where s.shopID=shop.shopID and s.shopCategory != ?;";
        try {
            verifyConnection();
            stat = conn.prepareStatement(sql);
            stat.setString(1, "market");
            rs = stat.executeQuery();
            while (rs.next()) {
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }

    // Get List of malls
    public ArrayList<MallModel> getMalls(String area) {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.mallID, s.mallName, s.mallRating, s.mallArea, loc.latitude, loc.longitude FROM " +
                "malltable as s, locationtable as loc where lower(s.mallArea) = ? and loc.locID IN (SELECT loc.locID FROM " +
                "mallloctable WHERE s.mallID IN (SELECT s.mallID FROM malltable WHERE lower(s.mallArea) = ?))";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, area);
            stat.setString(2, area);
            rs = stat.executeQuery();
            while (rs.next()) {
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.mallName"), rs.getInt("s.mallID"),
                        area, rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"), rs.getDouble("s.mallRating")));
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }

    // Get Market List for HomeScreen
    public ArrayList<MallModel> getMarketsInMalls() {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.longitude, loc.latitude FROM " +
                "shoptable as s, locationtable as loc where s.shopCategory = ? and loc.locID IN (SELECT loc.locID FROM " +
                "shoploctable WHERE s.shopID IN (SELECT s.shopID FROM shoptable WHERE s.shopCategory = ?))";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, "market");
            stat.setString(2, "market");
            rs = stat.executeQuery();
            while (rs.next()) {
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
            }
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }

    // Get Food Places according to category: Restaurant, cafe, Icecream, Nightlife for homescreen
    public ArrayList<FoodShopModel> getFood(String category) {
        verifyConnection();
        ArrayList<FoodShopModel> foodShopModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.foodShopID, s.foodShopName, s.foodShopRating, s.foodShopLocation, loc.longitude, loc.latitude FROM " +
                "foodtable as s inner join locationtable as loc, foodshoploctable as foodshop where s.foodshopid=foodshop.foodShopID and " +
                "s.foodShopCategory=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, category);
            rs = stat.executeQuery();
            while (rs.next()) {
                foodShopModels.add(new FoodShopModel(R.drawable.ic_vividseats, rs.getInt("s.foodShopID"),
                        rs.getString("s.foodShopName"), rs.getString("s.foodShopLocation"), rs.getDouble("loc.latitude"),
                        rs.getDouble("loc.longitude"), rs.getDouble("s.foodShopRating")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foodShopModels;
    }

    // Get Offers list for Home page
    public ArrayList<OfferModel> getOffers(String category) {
        verifyConnection();
        ArrayList<OfferModel> offerModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        Log.d("Category", category);
        String sql = "SELECT offer.offerID, offer.offerDesc, offer.offerUsers, offer.offerLocation, offer.offerName, loc.longitude, " +
                "loc.latitude, shop1.shopID FROM offertable as offer, locationtable as loc, shopoffertable as shop, shoploctable AS" +
                " shop1 WHERE offer.offerCategory=? AND loc.locID IN (SELECT locID FROM shoploctable WHERE shopID IN (SELECT shopID " +
                "FROM shopoffertable where offerID IN (SELECT offerID from offertable WHERE offerCategory = ?))) " + "GROUP BY(offer.offerID)";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, category);
            stat.setString(2, category);
            rs = stat.executeQuery();
            while (rs.next()) {
                offerModels.add(new OfferModel(R.drawable.avengers, rs.getInt("offer.offerID"), rs.getInt("shop1.shopID"),
                        rs.getString("offer.offerName"), rs.getString("offer.offerLocation"), rs.getDouble("loc.latitude"),
                        rs.getDouble("loc.longitude"), rs.getString("offer.offerDesc")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return offerModels;
    }

    // Return details of store according to shopID
    public ArrayList<Object> getStoreDetails(int shopID) {
        verifyConnection();
        ArrayList<Object> details = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select shopTimings, shopArea, shopName, shopPhoneNumber, shopRating from shoptable where shopID = ?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, shopID);
            rs = stat.executeQuery();
            while (rs.next()){
                details.add(rs.getString("shopTimings"));
                details.add(rs.getString("shopArea"));
                details.add(rs.getString("shopName"));
                details.add(rs.getString("shopPhoneNumber"));
                details.add(rs.getDouble("shopRating"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return details;
    }

    // Return offers for a shop according to shopID
    public ArrayList<ShopPageDealsModel> getShopOffers(int shopID) {
        verifyConnection();
        ArrayList<ShopPageDealsModel> offers = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select offerID, offerDesc from offerTable where offerID in (select offerID from shopoffertable where shopID = ?)";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, shopID);
            rs = stat.executeQuery();
            while (rs.next()) {
                offers.add(new ShopPageDealsModel(rs.getInt("offerID"), rs.getString("offerDesc")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return offers;
    }

    public void writeShopReview(int shopID, String user, String review, double rating) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "insert into reviewTable(reviewUser, reviewContent, reviewRating) values(?, ?, ?)";
        int reviewID;
        String sql1 = "insert into shopreview(reviewID, shopID) values(?, ?);";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, user);
            stat.setString(2, review);
            stat.setDouble(3, rating);
            reviewID = stat.executeUpdate();

            stat = conn.prepareStatement(sql1);
            stat.setInt(1, reviewID);
            stat.setInt(2, shopID);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setUserFollowShop(int userID, int shopId) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "insert into usershopfollowtable(userID, shopID) values(?,?);";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopId);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setUserUnFollowShop(int userID, int shopId) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "delete usershopfollowtable wher userID=? and shopID=?;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopId);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setUserFollowFoodShop(int userID, int shopId) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "insert into userfoodshopfollowtable(userID, foodShopID) values(?,?);";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopId);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setUseUnrFollowFoodShop(int userID, int shopId) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "delete userfoodshopfollowtable where userID=? and foodShopID=?;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopId);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean isUserFollow(int userID, int shopID) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select * from usershopfollowtable where userID = ? and shopID = ?;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopID);
            rs = stat.executeQuery();
            while (rs.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isUserFollowRestaurant(int userID, int shopID) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select * from userfoodshopfollowtable where userID = ? and shopID = ?;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopID);
            rs = stat.executeQuery();
            while (rs.next())
                return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public ArrayList<Object> getUserFollowShops(int userID) {
        verifyConnection();
        ArrayList<Object> followShops = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.latitude, loc.longitude FROM "+
                "shoptable as s inner join locationtable as loc, shoploctable as shop, usershopfollowtable as follow where s.shopID=shop.shopID and " +
                "s.shopID=follow.shopID and follow.userID=? group by s.shopID;";
        String sql1 = "SELECT s.foodShopID, s.foodShopName, s.foodShopRating, s.foodShopLocation, loc.longitude, loc.latitude FROM " +
                "foodtable as s inner join locationtable as loc, foodshoploctable as foodshop, userfoodshopfollowtable as follow " +
                "where s.foodshopid=foodshop.foodShopID and s.foodShopID=follow.foodShopID group by s.foodShopID;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            rs = stat.executeQuery();
            while (rs.next()) {
                followShops.add(new MallModel(R.drawable.shop, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
            }
            stat = conn.prepareStatement(sql1);
            rs = stat.executeQuery();
            while (rs.next())
                followShops.add(new FoodShopModel(R.drawable.ic_vividseats, rs.getInt("s.foodShopID"),
                        rs.getString("s.foodShopName"), rs.getString("s.foodShopLocation"), rs.getDouble("loc.latitude"),
                        rs.getDouble("loc.longitude"), rs.getDouble("s.foodShopRating")));
        } catch(SQLException e) {
            e.printStackTrace();
        }

        return followShops;
    }

    public ArrayList<ReviewModel> getShopReviews(int shopID, boolean limit) {
        verifyConnection();
        ArrayList<ReviewModel> reviewModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql;
        if(limit)
           sql = "select * from reviewTable where reviewID in (select reviewID from shopreview where shopID = ?) order by " +
                   "reviewRating desc limit 3;";
        else
            sql = "select * from reviewTable where reviewID in (select reviewID from shopreview where shopID = ?) order by reviewrating desc;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, shopID);
            rs = stat.executeQuery();
            while (rs.next()) {
                reviewModels.add(new ReviewModel(rs.getInt("reviewID"), rs.getString("reviewUser"), rs.getString("reviewContent"),
                        rs.getDouble("reviewRating")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reviewModels;
    }

    // Return item details of a shop according to shopID
    public ArrayList<ShopPageInventoryItemModel> getShopItems(int userID, int shopID) {
        verifyConnection();
        ArrayList<ShopPageInventoryItemModel> itemModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        HashSet<Integer> hashSet = new HashSet<>();
        String sql = "select * from itemtable where itemQuantity > ? and itemID in (select itemID from shopitemtabke where shopID = ?);";
        String sql1 = "select itemID from usershopwishlisttable where userID=? and shopID=?";
        try {
            stat = conn.prepareStatement(sql1);
            stat.setInt(1, userID);
            stat.setInt(2, shopID);
            rs = stat.executeQuery();
            while (rs.next())
                hashSet.add(rs.getInt("itemID"));

            stat = conn.prepareStatement(sql);
            stat.setInt(1, 0);
            stat.setInt(2, shopID);
            rs = stat.executeQuery();
            while (rs.next()) {
                ItemModel item1, item2;
                item1 = new ItemModel(rs.getInt("itemID"), R.drawable.avengers, rs.getString("itemName"), rs.getString("itemCategory"),
                        rs.getInt("itemQuantity"), rs.getDouble("itemPrice"));
                if(rs.next()) {
                    item2 = new ItemModel(rs.getInt("itemID"), R.drawable.avengers, rs.getString("itemName"),
                            rs.getString("itemCategory"), rs.getInt("itemQuantity"), rs.getDouble("itemPrice"));
                    boolean flag1 = hashSet.contains((Integer) item1.getId()), flag2 = hashSet.contains((Integer) item2.getId());
                    itemModels.add(new ShopPageInventoryItemModel(item1.getId(), item2.getId(), R.drawable.shop,
                            R.drawable.shop, item1.getName(), item2.getName(), item1.getCategory(),
                            item2.getCategory(), item1.getPrice(), item2.getPrice(), flag1, flag2));
                } else {
                    boolean flag1 = hashSet.contains((Integer) item1.getId()), flag2 = hashSet.contains((Integer) item1.getId());
                    itemModels.add(new ShopPageInventoryItemModel(item1.getId(), item1.getId(), R.drawable.shop,
                            R.drawable.shop, item1.getName(), item1.getName(), item1.getCategory(),
                            item1.getCategory(), item1.getPrice(),item1.getPrice(), flag1, flag2));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemModels;
    }

    public ArrayList<ItemModel> getWishlistItems(int userID) {
        verifyConnection();
        ArrayList<ItemModel> itemModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select item.itemImage, item.itemID, item.itemQuantity, item.itemName, item.itemPrice, item.itemCategory from " +
                "itemtable as item inner join usershopwishlisttable as shop where item.itemID=shop.itemID and userID=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            rs = stat.executeQuery();
            while (rs.next()) {
                itemModels.add(new ItemModel(rs.getInt("item.itemID"), R.drawable.shop, rs.getString("item.itemName"),
                        rs.getString("item.itemCategory"), rs.getInt("item.itemQuantity"), rs.getDouble("item.itemPrice")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return itemModels;
    }

    public void removeItemFromWishlist(int userID, int shopID, int itemID) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "delete from usershopwishlisttable where userID=? and shopID=? and itemID=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopID);
            stat.setInt(3, itemID);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeItemFromWishlist(int userID, int itemID) {
        verifyConnection();
        PreparedStatement stat = null;
        String sql = "delete from usershopwishlisttable where userID=? and itemID=?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, itemID);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MallModel> getWishListStores(int userID) {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.latitude, " +
                "loc.longitude FROM shoptable as s inner join locationtable as loc, shoploctable as shop, " +
                "usershopwishlisttable as user where s.shopID=shop.shopID and user.shopID=s.shopID and userID=? group by user.shopID;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            rs = stat.executeQuery();
            while (rs.next())
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }

    public ArrayList<Object> getFoodShopDetails(int restaurauntID) {
        verifyConnection();
        ArrayList<Object> details = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select foodShopImage, foodShopLocation, foodShopName, foodShopTimings, foodShopRating, foodShopCost, " +
                "foodShopPhoneNumber from foodtable where foodShopID = ?";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, restaurauntID);
            rs = stat.executeQuery();
            while (rs.next()){
                details.add(rs.getString("foodShopName"));
                details.add(rs.getString("foodShopLocation"));
                details.add(rs.getString("foodShopCost"));
                details.add(rs.getString("foodShopTimings"));
                details.add(rs.getString("foodShopPhoneNumber"));
                details.add(rs.getDouble("foodShopRating"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return details;
    }

    // Return offers for a shop according to shopID
    public ArrayList<RestaurantPageDealsModel> getRestaurantOffers(int restaurantID) {
        verifyConnection();
        ArrayList<RestaurantPageDealsModel> offers = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select offerID, offerDesc from offerTable where offerID in (select offerID from foodoffertable where foodShopID = ?)";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, restaurantID);
            rs = stat.executeQuery();
            while (rs.next()) {
                offers.add(new RestaurantPageDealsModel(rs.getInt("offerID"), rs.getString("offerDesc")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return offers;
    }

    public void writeFoodShopReview(int shopID, String user, String review, double rating) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "insert into reviewTable(reviewUser, reviewContent, reviewRating) values(?, ?, ?)";
        int reviewID;
        String sql1 = "insert into foodshopreviewtable(reviewID, shopID) values(?, ?);";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, user);
            stat.setString(2, review);
            stat.setDouble(3, rating);
            reviewID = stat.executeUpdate();

            stat = conn.prepareStatement(sql1);
            stat.setInt(1, reviewID);
            stat.setInt(2, shopID);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<ReviewModel> getFoodShopReviews(int foodShopID, boolean limit) {
        verifyConnection();
        ArrayList<ReviewModel> reviewModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql;
        if (limit)
            sql = "select * from reviewTable where reviewID in (select reviewID from foodshopreviewTable where foodShopID = ?) order by" +
                    " reviewRating desc limit 3;";
        else
            sql = "select * from reviewTable where reviewID in (select reviewID from foodshopreviewTable where foodShopID = ?) order by " +
                    "reviewRating desc;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, foodShopID);
            rs = stat.executeQuery();
            while (rs.next()) {
                reviewModels.add(new ReviewModel(rs.getInt("reviewID"), rs.getString("reviewUser"), rs.getString("reviewContent"),
                        rs.getDouble("reviewRating")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return reviewModels;
    }

    public void addItemInWishList(int userID, int shopID, int itemID) {
        verifyConnection();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "insert into usershopwishlisttable(userID, shopID, itemID) values(?, ?, ?);";
        try {
            stat = conn.prepareStatement(sql);
            stat.setInt(1, userID);
            stat.setInt(2, shopID);
            stat.setInt(3, itemID);
            stat.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<SearchItemModel> getSearchItems(String query) {
        verifyConnection();
        ArrayList<SearchItemModel> itemModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "select shop.shopName, item.itemID, item.itemName, item.itemImage, item.itemPrice, item.itemCategory from " +
                "itemTable as item join shopTable as shop, shopitemtable as shopitem where shopitem.itemID=item.itemID and " +
                " shopItem.shopID=shop.shopID and item.itemName regexp ? group by item.itemID;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, query);
            rs = stat.executeQuery();
            while (rs.next())
                itemModels.add(new SearchItemModel(rs.getInt("item.itemID"), R.drawable.shop, rs.getString("item.itemName"),
                        rs.getString("item.itemCategory"), rs.getDouble("item.itemPrice"), rs.getString("shop.shopName")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return  itemModels;
    }

    public ArrayList<MallModel> getSearchShops(String query) {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.latitude, loc.longitude FROM " +
                "shoptable as s inner join locationtable as loc, shoploctable as shop, shopitemtable as shopitem, itemtable as item " +
                "where s.shopID=shop.shopID and shopitem.shopID=s.shopID and shopitem.itemID=item.itemID and lower(item.itemName)" +
                " regexp ? group by s.shopID;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, query);
            rs = stat.executeQuery();
            while (rs.next())
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }

    public ArrayList<MallModel> getSearchShops1(String query) {
        verifyConnection();
        ArrayList<MallModel> mallModels = new ArrayList<>();
        PreparedStatement stat = null;
        ResultSet rs = null;
        String sql = "SELECT s.shopID, s.shopCategory, s.shopName, s.shopRating, s.shopArea, loc.latitude, loc.longitude FROM " +
                "shoptable as s inner join locationtable as loc, shoploctable as shop where s.shopID=shop.shopID and lower(s.shopName) " +
                "regexp ? group by s.shopID;";
        try {
            stat = conn.prepareStatement(sql);
            stat.setString(1, query);
            rs = stat.executeQuery();
            while (rs.next())
                mallModels.add(new MallModel(R.drawable.ic_vividseats, rs.getString("s.shopName"), rs.getInt("s.shopID"),
                        rs.getString("s.shopArea"), rs.getDouble("loc.latitude"), rs.getDouble("loc.longitude"),
                        rs.getString("s.shopCategory"), rs.getDouble("s.shopRating")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mallModels;
    }
}