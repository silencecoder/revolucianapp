package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 1/6/16.
 */
public class ShopPageDealsModel {

    int id;
    String desc;

    public ShopPageDealsModel(int id, String desc) {
        this.id = id;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
