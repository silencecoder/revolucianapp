package com.example.hitman.revolucianapp.activity.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

public class CreateDeckActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView deckTitle, deckDesc, deckBtn;
    private Intent intent;

    private DatabaseAdapter databaseAdapter;
    private PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_deck);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        deckTitle = (TextView) findViewById(R.id.create_deck_title);
        deckDesc = (TextView) findViewById(R.id.create_deck_description);
        deckBtn = (TextView) findViewById(R.id.create_deck_btn);

        databaseAdapter = new DatabaseAdapter(getApplicationContext());
        prefManager = new PrefManager(getApplicationContext());

        deckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseAdapter.insertDeck(deckTitle.getText().toString(), deckDesc.getText().toString(),
                        databaseAdapter.getUserName(prefManager.getUserEmail()));
                intent = new Intent();
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });
    }
}
