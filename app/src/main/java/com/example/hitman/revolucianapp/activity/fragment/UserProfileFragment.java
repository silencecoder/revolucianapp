package com.example.hitman.revolucianapp.activity.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.LayoutManager.RoundedImageView;
import com.example.hitman.revolucianapp.activity.adapter.UserProfileDeckAdapter;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    public UserProfileFragment() {
        // Required empty public constructor
    }

    private static final float BITMAP_SCALE = 0.2f;
    private static final float BLUR_RADIUS = 9.5f;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserProfileFragment.
     */
    private RoundedImageView imageView;
    private LinearLayout llImageView;
    private RecyclerView recyclerView;
    private ArrayList<UserProfileDeckModel> decks;
    private UserProfileDeckAdapter userProfileDeckAdapter;
    // TODO: Rename and change types and number of parameters
    public static UserProfileFragment newInstance(String param1, String param2) {
        UserProfileFragment fragment = new UserProfileFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_profile, container, false);
        imageView = (RoundedImageView) v.findViewById(R.id.user_profile_image);
        llImageView = (LinearLayout) v.findViewById(R.id.user_profile_ll_image);
        recyclerView = (RecyclerView) v.findViewById(R.id.user_profile_recycler_view);

        imageView.setBackgroundResource(R.drawable.profile);
        llImageView.setBackground(new BitmapDrawable(getResources(),
                blur(getContext(), BitmapFactory.decodeResource(getResources(), R.drawable.profile))));

        decks = new ArrayList<>();
        addDecks();

        userProfileDeckAdapter = new UserProfileDeckAdapter(getContext(), decks);
        RecyclerView.LayoutManager mLayoutManager = new com.example.hitman.revolucianapp.activity.LayoutManager.LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(10));
        recyclerView.setAdapter(userProfileDeckAdapter);

        return v;
    }

    private void addDecks() {
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
//        decks.add(new UserProfileDeckModel(R.drawable.ic_vividseats, "Today", "Himanshu Garg", "It contains movies decks"));
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }

    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }
}
