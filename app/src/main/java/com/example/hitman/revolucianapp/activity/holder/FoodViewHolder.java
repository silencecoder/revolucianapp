package com.example.hitman.revolucianapp.activity.holder;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;

/**
 * Created by hitman on 12/7/16.
 */
public class FoodViewHolder extends RecyclerView.ViewHolder {

    public TextView cardFoodShopName, cardFoodShopTimings, cardFoodShopLocation, cardFoodShopCost;
    public ImageView cardFoodShopCall;

    public FoodViewHolder(View v) {
        super(v);
        cardFoodShopName = (TextView) v.findViewById(R.id.card_restaurant_name);
        cardFoodShopTimings = (TextView) v.findViewById(R.id.card_restaurant_timing);
        cardFoodShopLocation = (TextView) v.findViewById(R.id.card_restaurant_address);
        cardFoodShopCost = (TextView) v.findViewById(R.id.card_restaurant_cost);
        cardFoodShopCall = (ImageView) v.findViewById(R.id.card_restaurant_call);
    }

    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }
}