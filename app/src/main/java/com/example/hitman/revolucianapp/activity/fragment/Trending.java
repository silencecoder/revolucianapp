package com.example.hitman.revolucianapp.activity.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.adapter.DesignDemoRecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Trending.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Trending#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Trending extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//    private static final String ARG_PARAM1 = "param1";
//    private static final String ARG_PARAM2 = "param2";
//
//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Trending.
     */

    private static final String TAB_POSITION = "tab_position";
    // TODO: Rename and change types and number of parameters
    public static Trending newInstance(String param1, String param2) {
        Trending fragment = new Trending();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    public Trending() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Bundle args = getArguments();
        int tabPosition = 1;
        View v =  inflater.inflate(R.layout.fragment_trending, container, false);

        RecyclerView recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        ArrayList<String> items = new ArrayList<String>();
        addItems(items, tabPosition);
        setRecyclerView(recyclerView, items);

        return v;
    }

    private void setRecyclerView(RecyclerView recyclerView, ArrayList<String> items) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new DesignDemoRecyclerAdapter(items));
    }

    private void addItems(ArrayList<String> items, int tabPosition) {
        for (int i = 0; i < 20; i++) {
            items.add("Tab #" + tabPosition + " item #" + i);
        }
    }
}
