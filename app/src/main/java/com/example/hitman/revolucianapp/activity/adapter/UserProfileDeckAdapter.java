package com.example.hitman.revolucianapp.activity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.model.MoviesModel;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;

import java.util.List;

/**
 * Created by hitman on 8/6/16.
 */
public class UserProfileDeckAdapter extends InterceptorView.Adapter<UserProfileDeckAdapter.MyViewHolder> {

    private List<UserProfileDeckModel> decks;
    public static Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView deckItemImage;
        public TextView deckItemTitle, deckItemUser, deckItemDesc, deckItemNumberCards;

        public MyViewHolder(View view) {
            super(view);
            deckItemImage = (ImageView) view.findViewById(R.id.user_profile_deck_item_image);
            deckItemTitle = (TextView) view.findViewById(R.id.user_profile_deck_item_title);
            deckItemUser = (TextView) view.findViewById(R.id.user_profile_deck_item_username);
            deckItemDesc = (TextView) view.findViewById(R.id.user_profile_deck_item_description);
            deckItemNumberCards = (TextView) view.findViewById(R.id.user_profile_deck_item_number_cards);
        }
    }

    public UserProfileDeckAdapter(Context context, List<UserProfileDeckModel> decks) {
        mContext = context;
        this.decks = decks;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_profile_deck_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
       UserProfileDeckModel deck = decks.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.deckItemImage.setBackgroundResource(deck.getImage());
        holder.deckItemTitle.setText(deck.getTitle());
        holder.deckItemUser.setText("by "+deck.getUser());
        holder.deckItemNumberCards.setText(" | "+deck.getNumberOfCards()+" decks");
        holder.deckItemDesc.setText(deck.getDescription());
    }

    @Override
    public int getItemCount() {
        return decks.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private UserProfileDeckAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final UserProfileDeckAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
