package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 1/6/16.
 */
public class ShopPageInventoryItemModel {
    int id1, id2;
    int image1,image2;
    String itemName1, itemName2;
    String brandName1, brandName2;
    double price1, price2;
    boolean wishlist1, wishlist2;

    public ShopPageInventoryItemModel(int id1, int id2, int image1, int image2, String itemName1, String itemName2,
                                      String brandName1, String brandName2, double price1, double price2, boolean wishlist1, boolean wishlist2) {
        this.id1 = id1;
        this.id2 = id2;
        this.image2 = image2;
        this.image1 = image1;
        this.itemName1 = itemName1;
        this.itemName2 = itemName2;
        this.brandName1 = brandName1;
        this.brandName2 = brandName2;
        this.price1 = price1;
        this.price2 = price2;
        this.wishlist1 = wishlist1;
        this.wishlist2 = wishlist2;
    }

    public int getId1() {
        return id1;
    }

    public void setId1(int id1) {
        this.id1 = id1;
    }

    public int getId2() {
        return id2;
    }

    public void setId2(int id2) {
        this.id2 = id2;
    }

    public int getImage1() {
        return image1;
    }

    public void setImage1(int image1) {
        this.image1 = image1;
    }

    public int getImage2() {
        return image2;
    }

    public void setImage2(int image2) {
        this.image2 = image2;
    }

    public String getItemName1() {
        return itemName1;
    }

    public void setItemName1(String itemName1) {
        this.itemName1 = itemName1;
    }

    public String getItemName2() {
        return itemName2;
    }

    public void setItemName2(String itemName2) {
        this.itemName2 = itemName2;
    }

    public String getBrandName1() {
        return brandName1;
    }

    public void setBrandName1(String brandName1) {
        this.brandName1 = brandName1;
    }

    public String getBrandName2() {
        return brandName2;
    }

    public void setBrandName2(String brandName2) {
        this.brandName2 = brandName2;
    }

    public double getPrice1() {
        return price1;
    }

    public void setPrice1(double price1) {
        this.price1 = price1;
    }

    public double getPrice2() {
        return price2;
    }

    public void setPrice2(double price2) {
        this.price2 = price2;
    }

    public boolean isWishlist1() {
        return wishlist1;
    }

    public void setWishlist1(boolean wishlist1) {
        this.wishlist1 = wishlist1;
    }

    public boolean isWishlist2() {
        return wishlist2;
    }

    public void setWishlist2(boolean wishlist2) {
        this.wishlist2 = wishlist2;
    }
}
