package com.example.hitman.revolucianapp.activity.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.adapter.ShopPageDealsAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.ShopPageDealsModel;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ShopPageDealsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ShopPageDealsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShopPageDealsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private RecyclerView dealsRecyclerView;

    private ShopPageDealsAdapter shopPageDealsAdapter;
    private ServerDB serverDB;
    private OfferTask offerTask;
    private ArrayList<ShopPageDealsModel> deals;
    private int shopID;

//    // TODO: Rename and change types of parameters
//    private String mParam1;
//    private String mParam2;
//
//    private OnFragmentInteractionListener mListener;

    public ShopPageDealsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShopPageDealsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShopPageDealsFragment newInstance(String param1, String param2) {
        ShopPageDealsFragment fragment = new ShopPageDealsFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_shop_page_deals, container, false);
        dealsRecyclerView = (RecyclerView) v.findViewById(R.id.shop_page_deals_recycler_view);

        shopID = getActivity().getIntent().getIntExtra("shopID", 0);
        Log.d("ShopID", ""+shopID);
        deals = new ArrayList<>();
        shopPageDealsAdapter = new ShopPageDealsAdapter(getContext(), deals);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        dealsRecyclerView.setLayoutManager(layoutManager);
//        dealsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        dealsRecyclerView.setNestedScrollingEnabled(false);
        dealsRecyclerView.setHasFixedSize(true);
        dealsRecyclerView.addItemDecoration(new DividerItemDecoration(10));
        dealsRecyclerView.setAdapter(shopPageDealsAdapter);

        offerTask = new OfferTask(shopID);
        offerTask.execute((Void) null);

        return v;
    }

    private class OfferTask extends AsyncTask<Void, Void, Boolean> {

        int shopID;

        OfferTask(int shopID) {
            this.shopID = shopID;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getContext());
            } catch (InterruptedException e) {
                return false;
            }
            deals = serverDB.getShopOffers(shopID);

            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            offerTask = null;
            if(success) {
                Log.d("offers", deals.size()+"");
                shopPageDealsAdapter = new ShopPageDealsAdapter(getContext(), deals);
                dealsRecyclerView.setAdapter(shopPageDealsAdapter);
//                shopPageInventoryAdapter.notifyDataSetChanged();
                dealsRecyclerView.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(getView(), "No Internet Connection", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }
    }
//
//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
