package com.example.hitman.revolucianapp.activity.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;
import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.model.UserModel;
import com.example.hitman.revolucianapp.activity.utils.CustomRequest;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class SignUpActivity extends AppCompatActivity {

    private ServerDB serverDB;
    private DatabaseAdapter databaseAdapter;
    private UserSignUpTask mAuthTask;
    private EditText mPhoneView;
    private EditText mPasswordView;
    private EditText mEmailView;
    private EditText mNameView;
    private View mProgressView;
    private View mSignUpFormView;
    private Button mSignUpButton;
    private PrefManager prefManager;
    private TextView otpText;
    private EditText otpEdit;
    private Button otpButton;
    private ProgressDialog pDialog;
    private AlertDialog alertDialog;
    private CountDownTimer countDownTimer;
    private TextView otpSecondsRem;
    private Timer timer;
    private TimerTask timerTask;
    final Handler handler = new Handler();


    private int verificationID;

    private final String CONST_URL = "http://apifv.foneverify.com/U2opia_Verify/v1.0/flow/sms";
    private final String CONST_URL_VERIFY = "http://apifv.foneverify.com/U2opia_Verify/v1.0/flow/update";
    private String TAG = SignUpActivity.class.getSimpleName();
    // This tag will be used to cancel the request
    private String tag_json_obj = "string_req";
    RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        prefManager = new PrefManager(this);
        queue = Volley.newRequestQueue(this);

        mNameView = (EditText) findViewById(R.id.name);
        mPhoneView = (EditText) findViewById(R.id.phone);
        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mSignUpButton = (Button) findViewById(R.id.email_sign_up_button);
        mSignUpFormView = findViewById(R.id.signup_form);
        mProgressView = findViewById(R.id.signup_progress);
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        databaseAdapter = new DatabaseAdapter(getApplicationContext());
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPhoneView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String name = mNameView.getText().toString();
        String email = mEmailView.getText().toString();
        String phone = mPhoneView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if(TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if(!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if(TextUtils.isEmpty(name)) {
            mNameView.setError(getString(R.string.error_field_required));
            focusView = mNameView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(phone)) {
            mPhoneView.setError(getString(R.string.error_field_required));
            focusView = mPhoneView;
            cancel = true;
        } else if (!isPhoneValid(phone)) {
            mPhoneView.setError(getString(R.string.error_invalid_phone));
            focusView = mPhoneView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            verifyUser();
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPhoneValid(String phone) {
        //TODO: Replace this with your own logic
        return phone.length() == 10;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignUpFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class UserSignUpTask extends AsyncTask<Void, Void, Boolean> {

       UserModel userModel;
        int id = 0;

        UserSignUpTask(String userName, String userPhone, String userEmail, String userPassword) {
            userModel = new UserModel(userName, userEmail, userPhone, userPassword);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
                serverDB =  ServerDB.getInstance(getBaseContext());
            } catch (InterruptedException e) {
                return false;
            }
            serverDB.signUpUser(userModel);
            id = serverDB.getUserID(userModel.getEmail());
//            for (String credential : DUMMY_CREDENTIALS) {
//                String[] pieces = credential.split(":");
//                if (pieces[0].equals(mPhone)) {
//                    // Account exists, return true if the password matches.
//                    return pieces[1].equals(mPassword);
//                }
//            }
            return true;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
                databaseAdapter.signupUser(userModel);
                prefManager.setUserEmail(userModel.getEmail());
                prefManager.setUserSigninType(getString(R.string.signin_revolucian));
                prefManager.setUserLoggedIn(true);
                prefManager.setUserId(id);
                startActivity(new Intent(SignUpActivity.this, NavigationDrawerActivity.class));
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPhoneView.setError(getString(R.string.error_invalid_phone));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void verifyUser() {
        TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String countryCode = tm.getSimCountryIso();
        Log.e("CountryCode",countryCode);
        makeJsonObjReq(countryCode, mPhoneView.getText().toString());
        customOTPDialog();
    }

    private void customOTPDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.custom_otp_dialog, null);
        otpText = (TextView) v.findViewById(R.id.otp_txt);
        otpEdit = (EditText) v.findViewById(R.id.otp_code);
        otpButton = (Button) v.findViewById(R.id.otp_continue);
        otpSecondsRem = (TextView) v.findViewById(R.id.otp_seconds_rem);
//        otpButton.setBackgroundResource(R.color.black);
//        otpButton.setEnabled(false);
//        otpButton.setClickable(false);
        builder.setTitle("Verify your number")
                .setView(v);

        otpText.setText("Sent One time password to "+ mPhoneView.getText().toString() + " This password is valid for 90 seconds only");

        otpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(otpEdit.getText().toString().length() == 0)
                    return;;
                countDownTimer.cancel();
                stopTimerTask();
                verifyOTP();
                alertDialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();

        countDownTimer = new CountDownTimer(90000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
//                Log.e("seconds", millisUntilFinished+"");
                otpSecondsRem.setText(millisUntilFinished/1000 + " seconds ");
            }

            @Override
            public void onFinish() {
                otpSecondsRem.setText("Time over");
            }
        };

//        startTimer();
        countDownTimer.start();
    }

    private void timeoutRequest() {
        Map<String, String> params = new HashMap<String, String>();

        Log.d("Timeout", "Request");
        CustomRequest customRequest = new CustomRequest(getTimeOutURl(), params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG+" timeout", response.toString());
//                        msgResponse.setText(response.toString());
                        hideProgressDialog();
                        parseJsonTimeoutOtp(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        queue.add(customRequest);
    }

    private void intializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        timeoutRequest();
                    }

                });
            }
        };
    }

    private void startTimer() {
        timer = new Timer();
        intializeTimerTask();
        timer.schedule(timerTask,30000, 3000);
    }

    private void stopTimerTask() {
        if(timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void verifyOTP() {
        showProgressDialog();
        Map<String, String> params = new HashMap<String, String>();

        CustomRequest customRequest = new CustomRequest(getOtpUrl(), params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG+" verify", response.toString());
//                        msgResponse.setText(response.toString());
                        hideProgressDialog();
                        parseJsonVerifyOTP(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });

        queue.add(customRequest);
    }

    private void makeJsonObjReq(final String countryCode, final String mobileNumber) {
        showProgressDialog();
        Map<String, String> params = new HashMap<String, String>();
        params.put("customerId", getString(R.string.otp_customer_id));
        params.put("isoCountryCode", countryCode.toUpperCase());
        params.put("msisdn", mobileNumber);
        params.put("appKey", getString(R.string.otp_app_key));

        CustomRequest customRequest = new CustomRequest(Method.POST, CONST_URL, params,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG+" send", response.toString());
//                        msgResponse.setText(response.toString());
                        hideProgressDialog();
                        parseJsonOTP(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });

        queue.add(customRequest);
//        AppController.getInstance().addToRequestQueue(jsonObjReq,
//                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void parseJsonTimeoutOtp(JSONObject object) {
        try {
            int code = object.getInt("responseCode");
            if(code == 707) {
                Log.d("Sms delivery", "SMS successfully delivered");
                otpButton.setEnabled(true);
                otpButton.setClickable(true);
                otpButton.setBackgroundResource(R.color.colorPrimary);
                stopTimerTask();
            } else if(code == 708) {
                Log.d("Sms delivery", "Successfully delivery pending");
            } else {
                Log.d("Error1", "Error "+code);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseJsonOTP(JSONObject object) {
        try {
            int code = object.getInt("responseCode");
            if(code == 200) {
                verificationID = object.getInt("verificationId");
            } else if(code == 703) {
                Log.d("Number already verified", "Already verified");
                Toast.makeText(getApplicationContext(), "Number already verified", Toast.LENGTH_LONG).show();
            } else if(code == 501) {
                Log.d("Error", object.getString("errorMessage"));
            } else {
                Log.d("Error1", object.getString("errorMessage"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void parseJsonVerifyOTP(JSONObject object) {
        try {
            int code = object.getInt("responseCode");
            Log.d("OTP Submit", code+"");
            if(code == 200) {
                showProgress(true);
                mAuthTask = new UserSignUpTask(mNameView.getText().toString(),mPhoneView.getText().toString(),
                            mEmailView.getText().toString(), mPasswordView.getText().toString());
                mAuthTask.execute((Void) null);
            } else if(code == 501) {
                Log.d("Error", object.getString("errorMessage"));
            } else if(code == 504) {
                Toast.makeText(getApplicationContext(), "Invalid mobile Number", Toast.LENGTH_SHORT).show();
            } else if(code == 506) {
                Toast.makeText(getApplicationContext(), "OTP has already sent to "+mPhoneView.getText().toString(), Toast.LENGTH_LONG).show();
            } else if(code == 700) {
                Log.d("Verification", "Verification failed");
                Toast.makeText(getApplicationContext(), "Verification failed", Toast.LENGTH_LONG).show();
            } else if(code == 702) {
                Log.d("Verification", "Wrong OTP provided");
                Toast.makeText(getApplicationContext(), "Wrong OTP provided", Toast.LENGTH_LONG).show();
            } else {
                Log.d("Error1", object.getString("errorMessage"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private String getTimeOutURl() {
        String url = CONST_URL_VERIFY +
                "?customerId="+getString(R.string.otp_customer_id)+
                "&verificationId="+verificationID+
                "&appKey="+getString(R.string.otp_app_key);
        Log.d("TimeoutUrl", url);
        return url;
    }

    private String getOtpUrl() {
        return CONST_URL_VERIFY +
                "?customerId="+getString(R.string.otp_customer_id)+
                "&verificationId="+verificationID+
                "&appKey="+getString(R.string.otp_app_key) +
                "&code="+otpEdit.getText().toString();
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }
}
