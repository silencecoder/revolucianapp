package com.example.hitman.revolucianapp.activity.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.DividerItemDecoration;
import com.example.hitman.revolucianapp.activity.adapter.UserProfileDeckAdapter;
import com.example.hitman.revolucianapp.activity.databases.DatabaseAdapter;
import com.example.hitman.revolucianapp.activity.model.UserProfileDeckModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.ArrayList;

public class UserProfileActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    private Toolbar toolbar;
    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private RecyclerView recyclerView;
    private Intent intent;
    private FloatingActionButton fab;
    private LinearLayout navHeaderLL;
    private ImageView navHeaderImage;
    private TextView navHeaderName, navHeaderDesc;
    private CoordinatorLayout coordinatorLayout;

    private static final float BITMAP_SCALE = 0.2f;
    private static final float BLUR_RADIUS = 9.5f;

    private ArrayList<UserProfileDeckModel> decks;
    private UserProfileDeckAdapter userProfileDeckAdapter;;
    private PrefManager prefManager;
    private DatabaseAdapter databaseAdapter;

    private final int REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toolbar.setNavigationIcon(R.drawable.ic_action_list);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        recyclerView = (RecyclerView) findViewById(R.id.user_profile_recycler_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefManager = new PrefManager(getApplicationContext());
        databaseAdapter = new DatabaseAdapter(getApplicationContext());

        navigationView.setNavigationItemSelectedListener(this);

        decks = new ArrayList<>();
        decks = databaseAdapter.getAllDecks();

        userProfileDeckAdapter = new UserProfileDeckAdapter(getApplicationContext(), decks);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(10));
        recyclerView.setAdapter(userProfileDeckAdapter);

        addNavigationHeader();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(UserProfileActivity.this, CreateDeckActivity.class);
                startActivityForResult(intent, REQUEST_CODE);
            }
        });

        recyclerView.addOnItemTouchListener(new UserProfileDeckAdapter.RecyclerTouchListener(getApplicationContext(),
                recyclerView, new UserProfileDeckAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("images", images);
//                bundle.putInt("position", position);
//
//                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
//                SlideShowDialogFragment newFragment = SlideShowDialogFragment.newInstance();
//                newFragment.setArguments(bundle);
//                newFragment.show(ft, "slideshow");
//                Intent intent = new Intent(getActivity(), ShopPageActivity.class);
//                intent.putExtra("shopID", malls.get(position).getId());
//                startActivity(intent);
                intent = new Intent(UserProfileActivity.this, CardActivity.class);
                intent.putExtra("deckID", decks.get(position).getId());
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
//            fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.replace(R.id.containerView, new TabFragment());
////            fragmentTransaction.addToBackStack(null);
//            fragmentTransaction.commit();
            intent = new Intent(UserProfileActivity.this, NavigationDrawerActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_offers) {

        } else if (id == R.id.nav_notification) {

        } else if (id == R.id.nav_follow) {
            intent = new Intent(UserProfileActivity.this, UserProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_wishlist) {
            intent = new Intent(UserProfileActivity.this, WishListActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_to_do) {
            intent = new Intent(this, UserProfileActivity.class);
            startActivity(intent);
        } else if(id == R.id.nav_logout) {
            prefManager.setUserLoggedIn(false);
            intent = new Intent(UserProfileActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null && requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Toast.makeText(getApplicationContext(), "Deck created successfully", Toast.LENGTH_SHORT).show();
            decks = databaseAdapter.getAllDecks();
            userProfileDeckAdapter = new UserProfileDeckAdapter(getApplicationContext(), decks);
            recyclerView.setAdapter(userProfileDeckAdapter);
        }
    }

    private void addNavigationHeader() {
        View v = LayoutInflater.from(this).inflate(R.layout.nav_header_navigation_drawer, null);
        navHeaderLL = (LinearLayout) v.findViewById(R.id.nav_header_ll);
        navHeaderImage = (ImageView) v.findViewById(R.id.nav_header_image);
        navHeaderName = (TextView) v.findViewById(R.id.nav_header_name);
        navHeaderDesc = (TextView) v.findViewById(R.id.nav_header_desc);

        navHeaderImage.setImageResource(R.drawable.profile);
        navHeaderDesc.setText(prefManager.getUserEmail());
        navHeaderName.setText(databaseAdapter.getUserName(prefManager.getUserEmail()));
        navHeaderLL.setBackground(new BitmapDrawable(getResources(),
                blur(getApplicationContext(), BitmapFactory.decodeResource(getResources(), R.drawable.profile))));
        navigationView.addHeaderView(v);
    }

    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height, false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }
}
