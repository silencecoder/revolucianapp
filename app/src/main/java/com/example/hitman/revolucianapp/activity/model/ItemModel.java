package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 19/6/16.
 */
public class ItemModel {
    int id;
    int image;
    String name;
    String category;
    int quantity;
    double price;

    public ItemModel(int id, int image, String name, String category, int quantity, double price) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.category = category;
        this.quantity = quantity;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
