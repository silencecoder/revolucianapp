package com.example.hitman.revolucianapp.activity.adapter;

/**
 * Created by hitman on 1/6/16.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hitman.revolucianapp.R;
import com.example.hitman.revolucianapp.activity.LayoutManager.InterceptorView;
import com.example.hitman.revolucianapp.activity.activity.ShopPageActivity;
import com.example.hitman.revolucianapp.activity.databases.ServerDB;
import com.example.hitman.revolucianapp.activity.interfaces.AddItemWishList;
import com.example.hitman.revolucianapp.activity.model.Image;
import com.example.hitman.revolucianapp.activity.model.ShopPageInventoryItemModel;
import com.example.hitman.revolucianapp.activity.utils.PrefManager;

import java.util.List;

public class ShopPageInventoryAdapter extends InterceptorView.Adapter<ShopPageInventoryAdapter.MyViewHolder> {

    private List<ShopPageInventoryItemModel> items;
    public Context mContext;
    private ServerDB serverDB;
    private ShopPageActivity shopPageActivity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView shopPageInventoryItemImage1, shopPageInventoryItemImage2;
        public TextView shopPageInventoryItemName1, shopPageInventoryItemName2;
        public TextView shopPageInventoryItemBrandName1, shopPageInventoryItemBrandName2;
        public TextView shopPageInventoryItemPrice1, shopPageInventoryItemPrice2;
        public ImageView shopPageInventoryItemWishlist1, shopPageInventoryItemWishlist2;

        public MyViewHolder(View view) {
            super(view);
            shopPageInventoryItemImage1 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_image1);
            shopPageInventoryItemName1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_name1);
            shopPageInventoryItemBrandName1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_brand_name1);
            shopPageInventoryItemPrice1 = (TextView) view.findViewById(R.id.shop_page_inventory_item_price1);
            shopPageInventoryItemWishlist1 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_wishlist1);

            shopPageInventoryItemImage2 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_image2);
            shopPageInventoryItemName2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_name2);
            shopPageInventoryItemBrandName2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_brand_name2);
            shopPageInventoryItemPrice2 = (TextView) view.findViewById(R.id.shop_page_inventory_item_price2);
            shopPageInventoryItemWishlist2 = (ImageView) view.findViewById(R.id.shop_page_inventory_item_wishlist2);
        }
    }

    public ShopPageInventoryAdapter(Context context, List<ShopPageInventoryItemModel> items) {
        mContext = context;
        this.items = items;
        shopPageActivity = new ShopPageActivity();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shop_page_inventory_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final ShopPageInventoryItemModel item = items.get(position);

//        Glide.with(mContext).load(image.getName())
//                .thumbnail(0.5f)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(holder.image);
        holder.shopPageInventoryItemImage1.setBackgroundResource(item.getImage1());
        holder.shopPageInventoryItemName1.setText(item.getItemName1());
        holder.shopPageInventoryItemBrandName1.setText(item.getBrandName1());
        holder.shopPageInventoryItemPrice1.setText("Rs"+item.getPrice1());

        holder.shopPageInventoryItemImage2.setBackgroundResource(item.getImage2());
        holder.shopPageInventoryItemName2.setText(item.getItemName2());
        holder.shopPageInventoryItemBrandName2.setText(item.getBrandName2());
        holder.shopPageInventoryItemPrice2.setText("Rs"+item.getPrice2());

//        holder.shopPageInventoryItemWishlist1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shopPageActivity.addShopItemWishList(item.getId1(), );
//            }
//        });
//
//        holder.shopPageInventoryItemWishlist2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                shopPageActivity.addShopItemWishList(item.getId2());
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ShopPageInventoryAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ShopPageInventoryAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {

//                    Intent intent = new Intent(mContext, ShopPageActivity.class);

                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}


