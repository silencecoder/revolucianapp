package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 31/5/16.
 */
public class FoodShopModel {
    private int id;
    private int image;
    private String shopName;
    private String shopLocation;
    private String shopTimings;
    private String shopCost;
    private String shopNumber;
    private double shopRating;
    private double shopDistance;
    private double shopLatitude;
    private double shopLongitude;

    public FoodShopModel() {

    }

    public FoodShopModel(int id, String shopName, String shopLocation, String shopTimings, String shopCost, String shopNumber) {
        this.id = id;
        this.shopName = shopName;
        this.shopLocation = shopLocation;
        this.shopTimings = shopTimings;
        this.shopCost = shopCost;
        this.shopNumber = shopNumber;
    }

    public FoodShopModel(int image, int id, String shopName, String shopLocation, double shopLatitude, double shopLongitude, double shopRating) {
        this.id = id;
        this.image = image;
        this.shopName = shopName;
        this.shopLocation = shopLocation;
        this.shopRating = shopRating;
        this.shopLatitude = shopLatitude;
        this.shopLongitude = shopLongitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLocation() {
        return shopLocation;
    }

    public void setShopLocation(String shopLocation) {
        this.shopLocation = shopLocation;
    }

    public String getShopTimings() {
        return shopTimings;
    }

    public void setShopTimings(String shopTimings) {
        this.shopTimings = shopTimings;
    }

    public String getShopCost() {
        return shopCost;
    }

    public void setShopCost(String shopCost) {
        this.shopCost = shopCost;
    }

    public String getShopNumber() {
        return shopNumber;
    }

    public void setShopNumber(String shopNumber) {
        this.shopNumber = shopNumber;
    }

    public double getShopRating() {
        return shopRating;
    }

    public void setShopRating(double shopRating) {
        this.shopRating = shopRating;
    }

    public double getShopDistance() {
        return shopDistance;
    }

    public void setShopDistance(double shopDistance) {
        this.shopDistance = shopDistance;
    }

    public double getShopLatitude() {
        return shopLatitude;
    }

    public void setShopLatitude(double shopLatitude) {
        this.shopLatitude = shopLatitude;
    }

    public double getShopLongitude() {
        return shopLongitude;
    }

    public void setShopLongitude(double shopLongitude) {
        this.shopLongitude = shopLongitude;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
