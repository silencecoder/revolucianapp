package com.example.hitman.revolucianapp.activity.model;

/**
 * Created by hitman on 13/7/16.
 */
public class SearchItemModel {
    private int id;
    private int image;
    private String name;
    private String category;
    private double price;
    private String shopName;

    public SearchItemModel(int id, int image, String name, String category, double price, String shopName) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.category = category;
        this.price = price;
        this.shopName = shopName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }
}
